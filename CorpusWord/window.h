#ifndef WINDOW_H
#define WINDOW_H
#include "Words.h"
#include "patterns.h"
#include <QtSql>
using namespace std;
//================================ COMPOSANTS DE LA FENETRE
class Window : public QWidget
{  Q_OBJECT

public:
  Window(QWidget  * parent = 0) ;

  Words                 *word;
  Patterns              *pattern;

  int                   FormsNumber ;   // ceci doit être rentré ou récupéré dans la base par FormsAnalyseButton
  QElapsedTimer         Time ;
  static inline double  minutes(qint64 time) {return (double) time / (double) (qint64) 60000 ; }

protected slots:
  //corpus
  void              showCorpus() ;
  void              showWords();
  void              analyseForms();
  void              generatePattern();
  void              thresholdEntered();
 void               showPreProcessing();
 //void               updateAWN();
 void               build_AWN_corpus();
 void               FinalResult();



protected:

  // Partie Corpus
  void                 createUrlTable() ;
  QComboBox          * CorpusBox ;
  QComboBox          * Languagebox;
  QPushButton        * LoadCorpusButton ;
  QPushButton        * CreateWordNetCorpusButton ;
  QPushButton        * InsertWordButton ;
  QComboBox          * sqlGetCorpusBox() ;
  QComboBox          * sqlGetLanguageBox();
  QStandardItemModel * CorpusModel ;
  QTableView         * CorpusView ;
  int                  IdCorpus ;      // donné par le slot de LoadCorpusButton
  QLabel             * ConstDate ;
  QLabel             * NumberFiles ;
  QLabel             * CorpusSize ;
  QLabel             * CorpusEncoding ;
  QLabel             * CorpusFormats ;
  QLabel             * FormsNb ;
  QCheckBox           * ar_r_diactrics;
  QCheckBox           * ar_r_numbers;
  QCheckBox           * ar_r_f_letters;
  QCheckBox           * ar_r_shadda_madad;
  QCheckBox           * ar_r_alef;
  QCheckBox           * ar_r_yeh;
  QCheckBox           * ar_r_heh;
  QLabel              * ar_title;
  QLabel              * ar_RemoveDiactrics;
  QLabel              * ar_RemoveNumbers;
  QLabel              * ar_RemoveForeignletters;
  QLabel              * ar_RemoveShaddaAndMadda;
  QLabel              * ar_RemoveAlef;
  QLabel              * ar_RemoveYeh;
  QLabel              * ar_RemoveHeh;
  QLabel              * ar_stem;
  QComboBox           * stem;

  QString               language;
  bool                  ch_r_diactrics;
  bool                  ch_r_numbers;
  bool                  ch_r_f_letters;
  bool                  ch_r_shadda_madad;
  bool                  ch_r_alef;
  bool                  ch_r_yeh;
  bool                  ch_r_heh;
  QVector<QString> language_param;


  QString             sqlGetFormats() ;
  QString             sqlGetEncoding() ;
  void                corpusBoxes(QGridLayout *) ;
  void                patternBoxes(QGridLayout *) ;
  void                preprocessingBoxes(QGridLayout *) ;
  void                wordBoxes(QGridLayout *) ;

  //Partie Formes - marques
  QPushButton       * FormsAnalyseButton ;
  int                 MaxMarkers ;          // nombre maximal de marques quelle que soit la langue
  int                 MaxOccurrences ;      // nombre maximal d'occurrences ; pris de la base
  QLineEdit         * AcceptedBox ;
  QSpinBox          * ThresholdBox ;
  QSpinBox          * StopListBox ;
  QSpinBox          * JokerLengthBox ;
  QPushButton       * PatternGenerationButton ;
  QLineEdit         * AcceptedPatternBox ;
  QSpinBox          * PatternThresholdBox ;

  void                createNgramsTable() ;

  // général
  QLabel             * createLabel() ;

  QPushButton       * TreeConstitutionButton11 ;
  QPushButton     * TreeConstitutionButton12 ;
  void                createWordsTable();
  void                createMarquesTable();
  void                createPatternsTable();

  QTableWidget      * WordsTable;
  QTableWidget      * MarquesTable;
  QTableWidget      * PatternsTable;

  void getMarques(int,int);
  void getWords(int);
  void getPatterns();
  void MsgBox(QString);

  void removeArabicSection();
  void appearArabicSection();
  void checkPreProcessing();
  bool check_file_in_AWN(QString);
  bool searchAWN(QString , QString);

  QVector<QString> awn_list;
 /* void updateAWN();
  void updateAWN1();*/
  bool Exist_in_form(QString);


  bool Synset_Exist_In_Result(vector<QString>, vector<vector<int>>);
  QString  form_of_id(int);
  bool Intersection(vector<QString>,vector<int> );


} ;

#endif // WINDOWNGRAMS_H
