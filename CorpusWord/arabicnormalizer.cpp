#include "arabicnormalizer.h"
#include <iostream>
#include <sstream>
#include <cstdio>
#include <QtSql>
#include <QRegExp>
#include <string>
using namespace std;
/********************************************ARABICNORMALIZER**************************************/
ArabicNormalizer::~ArabicNormalizer()
{
}
QString ArabicNormalizer::normalize(QString s, QString remove_diactritics, QString remove_shadda, QString normalize_alef, QString normalize_yeh, QString normalize_heh)
{
    //ALEF_MADDA:ALEF_HAMZA_ABOVE:ALEF_HAMZA_BELOW: replaced by ALEF
    if (normalize_alef=="true")
        s.replace(QRegExp("[\u0622\u0623\u0625]"),"\u0627");
    //DOTLESS_YEH: replaced by YEH
    if (normalize_yeh=="true")
        s.replace(QRegExp("\u0649"),"\u064a");
    //TEH_MARBUTA: replaced by HEH
    if (normalize_heh=="true")
        s.replace(QRegExp("\u0629"),"\u0647");
    //SHADDA:TATWEEL: removed
    if (remove_shadda=="true")
        s.remove(QRegExp("[\u0651,\u0640]"));
    //KASRATAN:DAMMATAN:FATHATAN:FATHA:DAMMA:KASRA:SUKUN: removed
    if (remove_diactritics=="true")
        s.remove(QRegExp("[\u064b,\u064c,\u064d,\u064e,\u064f,\u0650,\u0652]"));
    return s;
}
QString ArabicNormalizer::isArabicWord(QString s)
{
    s.remove(QRegExp("[^\u0621-\u063a,\u0640-\u0655,\\d+]"));
    return s;
}
QString ArabicNormalizer::RemoveDigitWord(QString s)
{
    s.remove(QRegExp("\\d+"));
    return s;
}
QString ArabicNormalizer::normalize(QString s, QString remove_non_arabic, QString remove_digits, QString remove_diactritics, QString remove_shadda, QString normalize_alef, QString normalize_yeh, QString normalize_heh)
{
    if (remove_non_arabic=="true" && remove_digits=="true")
        s=ArabicNormalizer::isArabicWord(ArabicNormalizer::RemoveDigitWord(s));
    else if (remove_non_arabic=="true")
        s=ArabicNormalizer::isArabicWord(s);
    else if (remove_digits=="true")
        s=ArabicNormalizer::RemoveDigitWord(s);
    if (s.length()>0)
        return ArabicNormalizer::normalize(s,remove_diactritics,remove_shadda,normalize_alef,normalize_yeh,normalize_heh);
    return s;
}
/*******************************KhojaStemmer*************************************************/
//vector<vector<string> > KhojaStemmer::staticFiles;
vector<vector<wstring> > KhojaStemmer::staticFiles;
bool KhojaStemmer::rootFound = false;
bool KhojaStemmer::patternFound = false;
bool KhojaStemmer::stopwordFound = false;
bool KhojaStemmer::strangeWordFound = false;
bool KhojaStemmer::rootNotFound = false;
bool KhojaStemmer::fromSuffixes = false;
bool KhojaStemmer::addVectorFromFile ( QString path, QString fileName )
{
    vector<wstring> tmp;
    QTextStream flux;
    QFile infile;
    QDir::setCurrent(path);
    infile.setFileName(fileName) ;
    if (not infile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "ERREUR: Impossible d'ouvrir le fichier " << fileName << " en lecture." << endl ;
        return false;
    }
      flux.setDevice(& infile) ;
      //addTokens(flux) ;
      QString str ;
        while (not flux.atEnd())
        {
            flux >> str;
            //tmp.push_back(str.toUtf8().constData());
            tmp.push_back(str.toStdWString());
        }
      infile.close() ;
      KhojaStemmer::staticFiles.push_back(tmp);
      return true;
}
void KhojaStemmer::readInStaticFiles ( QString pathToStemmerFiles)
{
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "definite_article.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "duplicate.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "first_waw.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "first_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_alif.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_hamza.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_maksoura.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "last_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "mid_waw.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "mid_yah.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles, "prefixes.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles ,"punctuation.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles, "quad_roots.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "stopwords.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles ,"suffixes.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "tri_patt.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "tri_roots.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "diacritics.txt" ))
    if ( KhojaStemmer::addVectorFromFile ( pathToStemmerFiles , "strange.txt" ))
        cout << "";
}

// check that the word is a strange word
bool KhojaStemmer::checkStrangeWords(wstring currentWord)
{
    KhojaStemmer::strangeWordFound=find(KhojaStemmer::staticFiles.at(18).begin(),KhojaStemmer::staticFiles.at(18).end(),currentWord)!= KhojaStemmer::staticFiles.at(18).end();
    return KhojaStemmer::strangeWordFound;
}
// check that the word is a stopword
bool KhojaStemmer::checkStopwords(wstring currentWord)
{
    KhojaStemmer::stopwordFound=find(KhojaStemmer::staticFiles.at(13).begin(),KhojaStemmer::staticFiles.at(13).end(),currentWord)!= KhojaStemmer::staticFiles.at(13).end();
    return KhojaStemmer::stopwordFound;
}
// handle duplicate letters in the word
wstring KhojaStemmer::duplicate ( wstring word )
{
     // check if a letter was duplicated
    if (find(KhojaStemmer::staticFiles.at(1).begin(),KhojaStemmer::staticFiles.at(1).end(),word)!= KhojaStemmer::staticFiles.at(1).end())
    {
        // if so, then return the deleted duplicate letter
        word = word + word.substr(1);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
    }
    return word;
}
// check if the last letter of the word is a weak letter
wstring KhojaStemmer::lastWeak(wstring word)
{
    wstring stemWord(L"");
    // check if the last letter was an alif
    if (find(KhojaStemmer::staticFiles.at(4).begin(),KhojaStemmer::staticFiles.at(4).end(),word)!= KhojaStemmer::staticFiles.at(4).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0627");
        word=stemWord;
        stemWord.clear();
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        return word;
    }
    // check if the last letter was an hamza
    if (find(KhojaStemmer::staticFiles.at(5).begin(),KhojaStemmer::staticFiles.at(5).end(),word)!= KhojaStemmer::staticFiles.at(5).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0623");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was an maksoura
    if (find(KhojaStemmer::staticFiles.at(6).begin(),KhojaStemmer::staticFiles.at(6).end(),word)!= KhojaStemmer::staticFiles.at(6).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u0649");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was an yah
    if (find(KhojaStemmer::staticFiles.at(7).begin(),KhojaStemmer::staticFiles.at(7).end(),word)!= KhojaStemmer::staticFiles.at(7).end())
    {
        stemWord.append(word);
        stemWord.append(L"\u064a");
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// check if the first letter is a weak letter
wstring KhojaStemmer::firstWeak(wstring word)
{
    wstring stemWord(L"");
    // check if the firs letter was a waw
    if (find(KhojaStemmer::staticFiles.at(2).begin(),KhojaStemmer::staticFiles.at(2).end(),word)!= KhojaStemmer::staticFiles.at(2).end())
    {
        stemWord.append(L"\u0648");
        stemWord.append(word);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the last letter was a yah
    if (find(KhojaStemmer::staticFiles.at(3).begin(),KhojaStemmer::staticFiles.at(3).end(),word)!= KhojaStemmer::staticFiles.at(3).end())
    {
        stemWord.append(L"\u064a");
        stemWord.append(word);
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// check if the middle letter of the root is weak
wstring KhojaStemmer::middleWeak(wstring word)
{
    wstring stemWord(L"");
     // check if the middle letter is a waw
    if (find(KhojaStemmer::staticFiles.at(8).begin(),KhojaStemmer::staticFiles.at(8).end(),word)!= KhojaStemmer::staticFiles.at(8).end())
    {
        // return the waw to the word
        stemWord.append(word.substr(0,1));
        stemWord.append(L"\u0648");
        stemWord.append(word.substr(1));
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    // check if the middle letter is a yah
    if (find(KhojaStemmer::staticFiles.at(9).begin(),KhojaStemmer::staticFiles.at(9).end(),word)!= KhojaStemmer::staticFiles.at(9).end())
    {
        //return the yah to the word
        stemWord.append(word.substr(0,1));
        stemWord.append(L"\u064a");
        stemWord.append(word.substr(1));
        // root was found, so set variable
        KhojaStemmer::rootFound=true;
        word=stemWord;
        stemWord.clear();
        return word;
    }
    return word;
}
// if the word consists of two letters
wstring KhojaStemmer::isTwoLetters ( wstring word )
{
    // if the word consists of two letters, then this could be either
    // - because it is a root consisting of two letters (though I can't think of any!)
    // - because a letter was deleted as it is duplicated or a weak middle or last letter.
    word = KhojaStemmer::duplicate ( word );
    // check if the last letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::lastWeak ( word );
    // check if the first letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::firstWeak ( word );
    // check if the middle letter was weak
    if ( !KhojaStemmer::rootFound )
        word = KhojaStemmer::middleWeak ( word );
    return word;
}
// if the word consists of three letters
wstring KhojaStemmer::isThreeLetters ( wstring word )
{
    wstring modifiedWord( word );
    wstring root = L"";
    // if the first letter is a 'ا', 'ؤ'  or 'ئ'
   // then change it to a 'أ'
    if ( word.length() > 0 )
    {
        if ( word[0] == L'\u0627' || word[0] == L'\u0624' || word[0] == L'\u0626' )
        {
            modifiedWord.clear();
            modifiedWord.append( L"\u0623" );
            modifiedWord.append( word.substr(1) );
            root = modifiedWord;
        }
        // if the last letter is a weak letter or a hamza
        // then remove it and check for last weak letters
        if ( word[2] == L'\u0648' || word[2] == L'\u064a' || word[2] == L'\u0627' ||
             word[2] == L'\u0649' || word[2] == L'\u0621' || word[2] == L'\u0626' )
        {
            root = word.substr( 0, 2 );
            root = KhojaStemmer::lastWeak( root );
            if ( KhojaStemmer::rootFound )
            {
                return root;
            }
        }
        // if the second letter is a weak letter or a hamza
        // then remove it
        if ( word[1] == L'\u0648' || word[1] == L'\u064a' || word[1] == L'\u0627' || word[1] == L'\u0626' )
        {
            root = word.substr( 0, 1 );
            root = root + word.substr( 2 );
            root = KhojaStemmer::middleWeak( root );
            if ( KhojaStemmer::rootFound )
                return root;
        }
        // if the second letter has a hamza, and it's not on a alif
        // then it must be returned to the alif
        if ( word[1] == L'\u0624' || word[1] == L'\u0626' )
        {
            if ( word[2] == L'\u0645' || word[2] == L'\u0632' || word[2] == L'\u0631' )
            {
                root = word.substr( 0, 1 );
                root = root + L"\u0627";
                root = root+ word.substr( 2 );
            }
            else
            {
                root = word.substr( 0, 1 );
                root = root + L"\u0623";
                root = root + word.substr( 2 );
            }
        }
        // if the last letter is a shadda, remove it and
        // duplicate the last letter
        if ( word[2] == L'\u0651')
        {
            root = word.substr( 0, 1 );
            //????ADELLE??//
            root = root + word.substr(1, 1);
        }
    }
    // if word is a root, then rootFound is true
    if ( root.length() == 0 )
    {
        if (find(KhojaStemmer::staticFiles.at(16).begin(),KhojaStemmer::staticFiles.at(16).end(),word)!= KhojaStemmer::staticFiles.at(16).end())
        {
            KhojaStemmer::rootFound = true;
            return word;
        }
    }
    // check for the root that we just derived
    else if (find(KhojaStemmer::staticFiles.at(16).begin(),KhojaStemmer::staticFiles.at(16).end(),root)!= KhojaStemmer::staticFiles.at(16).end())
    {
        KhojaStemmer::rootFound = true;
        return root;
    }
    return word;
}
// if the word has four letters
void KhojaStemmer::isFourLetters(wstring word)
{
    if (find(KhojaStemmer::staticFiles.at(12).begin(),KhojaStemmer::staticFiles.at(12).end(),word)!= KhojaStemmer::staticFiles.at(12).end())
        KhojaStemmer::rootFound=true;
}
// check if the word matches any of the patterns
wstring KhojaStemmer::checkPatterns(wstring word)
{
    wstring root(L"");
    // if the first letter is a hamza, change it to an alif
    if ( word.length() > 0 )
        if ( word[0] == L'\u0623' || word[0] == L'\u0625' || word[0] == L'\u0622' )
        {
            root.append(L"\u0627" );
            root.append ( word.substr(1 ) );
            word = root;
        }
    // try and find a pattern that matches the word
    vector<wstring> patterns=KhojaStemmer::staticFiles.at(15);
    int numberSameLetters = 0;
    wstring pattern = L"";
    wstring modifiedWord = L"";
    // for every pattern
    for( int i = 0; i < patterns.size(); i++ )
    {
        pattern = patterns[i];
        root.clear();
        // if the length of the words are the same
        if ( pattern.length() == word.length() )
        {
            numberSameLetters = 0;
            // find out how many letters are the same at the same index
            // so long as they're not a fa, ain, or lam
            for ( int j = 0; j < word.length(); j++ )
                if ( pattern[j] == word[j]  && pattern[j] != L'\u0641' && pattern[j] != L'\u0639' && pattern[j] != L'\u0644')
                    numberSameLetters ++;
            // test to see if the word matches the pattern ЧнксЧ
            if ( word.length() == 6 && word[3] == word[5] && numberSameLetters == 2 )
            {
                root.append ( word.substr(1,1) );
                root.append ( word.substr(2,1) );
                root.append ( word.substr(3,1 ) );
                modifiedWord = root;
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
                if ( KhojaStemmer::rootFound )
                    return modifiedWord;
                else
                    root.clear();
            }
            // if the word matches the pattern, get the root
            if ( word.length() - 3 <= numberSameLetters )
            {
                // derive the root from the word by matching it with the pattern
                for ( int j = 0; j < word.length(); j++ )
                    if (pattern[j] == L'\u0641' || pattern[j] == L'\u0639' || pattern[j] == L'\u0644')
                        root.append ( word.substr( j,1 ) );
                modifiedWord = root;
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
                if ( KhojaStemmer::rootFound )
                {
                    word = modifiedWord;
                    return word;
                }
            }
        }
    }
    return word;
}
// METHOD CHECKFORSUFFIXES
wstring KhojaStemmer::checkForSuffixes ( wstring word )
{
    wstring suffix = L"";
    wstring modifiedWord = word;
    vector<wstring> suffixes =KhojaStemmer::staticFiles.at( 14 );
    KhojaStemmer::fromSuffixes= true;
    // for every suffix in the list
    for ( int i = 0; i < suffixes.size(); i++ )
    {
        suffix = suffixes[i];
        // if the suffix was found
        //if( suffix.regionMatches ( 0, modifiedWord, modifiedWord.length() - suffix.length(), suffix.length() ) )
        if (modifiedWord.length()>suffix.length() && modifiedWord.compare(modifiedWord.length()-suffix.length(),suffix.length(),suffix)== 0)
        {
            modifiedWord = modifiedWord.substr( 0, modifiedWord.length() - suffix.length() );
            // check to see if the word is a stopword
            if ( KhojaStemmer::checkStopwords ( modifiedWord ) )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
            {
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            }
            else if ( modifiedWord.length() == 3 )
            {
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            }
            else if ( modifiedWord.length() == 4 )
            {
                KhojaStemmer::isFourLetters ( modifiedWord );
            }
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length( ) > 2 )
            {
                modifiedWord = KhojaStemmer::checkPatterns( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound )
            {
                KhojaStemmer::fromSuffixes = false;
                return modifiedWord;
            }
        }
    }
    KhojaStemmer::fromSuffixes = false;
    return word;
}

// check and remove any prefixes
wstring KhojaStemmer::checkForPrefixes ( wstring word )
{
    wstring prefix = L"";
    wstring modifiedWord = word;
    vector<wstring> prefixes = KhojaStemmer::staticFiles.at( 10 );
    // for every prefix in the list
    for ( int i = 0; i < prefixes.size(); i++ )
    {
        prefix = prefixes[i];
        // if the prefix was found
        //if ( prefix.regionMatches ( 0, modifiedWord, 0, prefix.length() ) )
        if (prefix==modifiedWord.substr(0,prefix.length()))
        {
            modifiedWord = modifiedWord.substr( prefix.length() );
            // check to see if the word is a stopword
            if ( KhojaStemmer::checkStopwords( modifiedWord ) )
                return modifiedWord;
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            else if ( modifiedWord.length() == 4 )
                KhojaStemmer::isFourLetters ( modifiedWord );
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
                modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
            // if the root STILL hasn't been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound && !KhojaStemmer::fromSuffixes)
            {
                // check for suffixes
                modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
                return modifiedWord;
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                return modifiedWord;
            }
        }
    }
    return word;
}
// check and remove the definite article
wstring KhojaStemmer::checkDefiniteArticle ( wstring word )
{
    // looking through the vector of definite articles
    // search through each definite article, and try and
    // find a match
    wstring definiteArticle = L"";
    wstring modifiedWord = L"";
    vector<wstring> definiteArticles = KhojaStemmer::staticFiles.at( 0 );

    // for every definite article in the list
    for ( int i = 0; i < definiteArticles.size(); i++ )
    {
        definiteArticle = definiteArticles[i];
        // if the definite article was found
        //if ( definiteArticle.regionMatches ( 0, word, 0, definiteArticle.length() ) )
        if (word.length() > definiteArticle.length() && definiteArticle==word.substr(0,definiteArticle.length()))
        {
            // remove the definite article
            modifiedWord = word.substr(definiteArticle.length(), word.length() - definiteArticle.length());
            // check to see if the word is a stopword
            if (KhojaStemmer::checkStopwords ( modifiedWord ) )
                return modifiedWord;
            // check to see if the word is a root of three or four letters
            // if the word has only two letters, test to see if one was removed
            if ( modifiedWord.length() == 2 )
                modifiedWord = KhojaStemmer::isTwoLetters ( modifiedWord );
            else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
                modifiedWord = KhojaStemmer::isThreeLetters ( modifiedWord );
            else if ( modifiedWord.length() == 4 )
                KhojaStemmer::isFourLetters ( modifiedWord );
            // if the root hasn't been found, check for patterns
            if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
                modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
            // if the root STILL hasnt' been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                // check for suffixes
                modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
            }
            // if the root STILL hasn't been found
            if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                // check for prefixes
                modifiedWord = KhojaStemmer::checkForPrefixes ( modifiedWord );
            }
            if ( KhojaStemmer::stopwordFound )
                return modifiedWord;
            // if the root was found, return the modified word
            if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
            {
                return modifiedWord;
            }
        }
    }
    if ( modifiedWord.length() > 3 )
        return modifiedWord;
    return word;
}
// check and remove the special prefix (waw)
wstring KhojaStemmer::checkPrefixWaw ( wstring word )
{
    wstring modifiedWord = L"";
   // if ( word.length() > 3 && word.at( 0 ) == '\u0648' )
    if ( word.length() > 3 && word[0] == L'\u0648' )
    {
        modifiedWord = word.substr(1 );
        // check to see if the word is a stopword
        if ( KhojaStemmer::checkStopwords ( modifiedWord ) )
            return modifiedWord;
        // check to see if the word is a root of three or four letters
        // if the word has only two letters, test to see if one was removed
        if ( modifiedWord.length() == 2 )
            modifiedWord = KhojaStemmer::isTwoLetters( modifiedWord );
        else if ( modifiedWord.length() == 3 && !KhojaStemmer::rootFound )
            modifiedWord = KhojaStemmer::isThreeLetters( modifiedWord );
        else if ( modifiedWord.length() == 4 )
            KhojaStemmer::isFourLetters ( modifiedWord );
        // if the root hasn't been found, check for patterns
        if ( !KhojaStemmer::rootFound && modifiedWord.length() > 2 )
            modifiedWord = KhojaStemmer::checkPatterns ( modifiedWord );
        // if the root STILL hasnt' been found
        if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            // check for suffixes
            modifiedWord = KhojaStemmer::checkForSuffixes ( modifiedWord );
        }
        // iIf the root STILL hasn't been found
        if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            // check for prefixes
            modifiedWord = KhojaStemmer::checkForPrefixes ( modifiedWord );
        }
        if ( KhojaStemmer::stopwordFound )
            return modifiedWord;
        if ( KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
        {
            return modifiedWord;
        }
    }
    return word;
}
wstring KhojaStemmer::stemWord ( wstring word )
{
    // check if the word consists of two letters
    // and find it's root
    if ( word.length() == 2 )
        word = KhojaStemmer::isTwoLetters ( word );
    // if the word consists of three letters
    if( word.length() == 3 && !KhojaStemmer::rootFound )
        // check if it's a root
        word = KhojaStemmer::isThreeLetters ( word );
    // if the word consists of four letters
    if( word.length() == 4 )
        // check if it's a root
        KhojaStemmer::isFourLetters ( word );
    // if the root hasn't yet been found
    if( !KhojaStemmer::rootFound )
    {
        // check if the word is a pattern
        word = KhojaStemmer::checkPatterns ( word );
    }
    // if the root still hasn't been found
    if ( !KhojaStemmer::rootFound )
    {
        // check for a definite article, and remove it
        word = KhojaStemmer::checkDefiniteArticle ( word );
    }
    // if the root still hasn't been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for the prefix waw
        word = KhojaStemmer::checkPrefixWaw ( word );
    }
    // if the root STILL hasnt' been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for suffixes
        word = KhojaStemmer::checkForSuffixes ( word );
    }
    // if the root STILL hasn't been found
    if ( !KhojaStemmer::rootFound && !KhojaStemmer::stopwordFound )
    {
        // check for prefixes
        word = KhojaStemmer::checkForPrefixes ( word );
    }
    return word;
}
