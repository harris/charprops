#include <QtWidgets>
#include "../Common/Types.h"
#include "../Common/BDD.h"
#include<iostream>
#include <vector>

// l'élément de l'arbre préfixe
struct node_segment
{ id m_id ;                     // identifiant de la marque ou du joker
  int sortie ;                   // id = 0, pas de sortie ; sinon identifiant de la sortie
  val occurrences ;             // combien de sorties à cet endroit
  node_segment * sons ;
  node_segment * bros ;
} ;


typedef node_segment *arbre_segments;

class SegmentsDB : public QWidget
{  
  BDD                   Bdd ;
  int                   CountWords;

public:
  arbre_segments        Trie;
  QVector<int>          tmp_vector;

  int                   CountSegments;
  public:
    SegmentsDB() ;
    ~SegmentsDB();
   


  public:
   // void addTokens(arbre_word,int );
    inline void addWord(QString) ;

   arbre_segments create_node (id=0 , id=0 , arbre_segments=NULL , arbre_segments=NULL );
   void insert(QVector<int> , arbre_segments , int=0 );
   arbre_segments create_branch(QVector<int> , arbre_segments , int );
   arbre_segments assoc(id , arbre_segments );
   arbre_segments assoc_search(id , int, arbre_segments , std::vector<id>);
   bool search(QVector<int> , int, arbre_segments , std::vector<id> ,int =0);
   bool search_in_vector(std::vector<id>, int );
} ;



