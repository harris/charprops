#include "window.h"


Window::Window(QWidget *parent)
 : QWidget(parent), MaxMarkers(800)
{
      //================================ FENETRAGE ====================================================
      QGridLayout * mainlayout = new QGridLayout;
      mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));

      //================================ PARTIE CORPUS ================================================

      corpusBoxes(mainlayout) ;

      //================================ Pre-Processing ===============================================

      preprocessingBoxes(mainlayout) ;

      //================================ PARTIE Analyse Mots ==========================================

      wordBoxes(mainlayout) ;

      //================================ PARTIE Formes / Patterns =====================================

      patternBoxes(mainlayout) ;

      //==============================================================================================
      setLayout(mainlayout);
      setWindowTitle("Corpus Analysis");
}

void Window::MsgBox(QString msg)
{
    QMessageBox msgBox;
    msgBox.setText(msg);
    msgBox.exec();
}

