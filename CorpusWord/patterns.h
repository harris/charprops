#ifndef WORDS_H
#define WORDS_H
#include <QtWidgets>
#include "../Common/Types.h"
#include "../Common/BDD.h"
#include "../Common/Common.h"
#include "arabicnormalizer.h"
#include "WordsDB.h"
#include<iostream>
#include <vector>


// l'élément de l'arbre préfixe des patrons
struct node_pattern
{ id m_id ;                     // identifiant de la marque ou du joker
  int sortie ;                   // id = 0, pas de sortie ; sinon identifiant de la sortie
  val occurrences ;             // combien de sorties à cet endroit
  node_pattern * sons ;
  node_pattern * bros ;
} ;

typedef struct jokerelement
{ int  key ;
  int  segmentIndex;
} JokerElement;


typedef node_pattern * arbre_pattern ;
class Patterns
{
  int SeuilMarque ;              // fréquence minimale pour définir une marque
  int CountMarques ;             // nombre total de marques

  QMap<QString, id> Marques ;      // liste d'association des marques
  vector<id> id_Marques ;
  int size_Marques ;
  QMap<QString,id> Normales ;      // liste d'association des Normales
  vector<id> id_Normales ;
  int size_Normales ;
  QVector<JokerElement> tmp_pa;


  //QVector<QVector<int>> Segments;


 QMap<arbre_pattern, QVector<JokerElement> > JokerElements;
 QVector< QVector<int> > all_segments;

  id  JokerId ;                   // l'identifiant du joker (nombre de marques + 1)
  int JokerLength ;
  int SeuilPattern ;             // fréquence minimale pour conserver un patron
  int CountPatterns ;            // nombre total de patrons
  int index;

public:
    WordsDB *word_db;

  public:
    Patterns(int, int, int,int, int, QElapsedTimer,QString, QVector<QString>) ;
    ~Patterns() {}

  public:

    void addTokens(arbre_pattern,QString, QString, QVector<QString>,int) ;
    //arbre_pattern getTrie() { return Trie; }
    int getPlaceHolderLength() { return JokerLength ; }
    int getSeuilPattern() { return SeuilPattern ; }
    int getSeuilMarque() { return SeuilMarque ; }
    int getCountPatterns() { return CountPatterns ; }
    int getCountMarques() { return CountMarques ; }

    void getMatrix(int, QElapsedTimer time) ;

    // Création

    void sqlWriteData(int) ;
    void sqlWriteMatrix(int);

    arbre_pattern create_node (id =0, id=0, arbre_pattern =NULL, arbre_pattern =NULL);
    void insert(QVector<int> , int, arbre_pattern , int = 0 );
    arbre_pattern create_branch(QVector<int> , arbre_pattern , int );
    arbre_pattern assoc(id, arbre_pattern);

    int lastPunct(QStringRef,QString );
    int firstPunct(QStringRef ,QString);
    int stopsegment(QStringRef);
    QString removePunctuation(QString,QString);
    bool analysWord(QString);
    QVector<int> readSegment(QTextStream & , QString, QVector<QString>);
    val AnalyseWord(QString );
    void PrintSegment(QVector< QVector<int> > );

    void sqlWriteBatch(arbre_pattern);
    void sqlWritePatterns(arbre_pattern , QVector<arbre_pattern>, QVector<int> , QSqlQuery, QSqlQuery);
    int sqlWritePattern(QString, QString, int, QSqlQuery, bool = false);

    void sqlWriteMatriceBatch();
    void sqlWriteMatrice(int, int, int, bool,bool,QSqlQuery);

    QString Convert_vector_to_String(QVector<int> );
    QString ReadWord(int);
    bool valid_pattern(QVector<int>);
    bool valid_segment(QVector<int>);
    bool valid_segment_str(QVector<QString>);
    bool valid_normal_with_pattern(JokerElement , QVector<int>);
    bool type_of_word_in_table_form(int, QString, QString, int,int);
    bool checkJokerLength(QVector<int>, int);
    int exist_pattern(QString );
    int id_of_word(QString);
    bool search_vector(vector<id>, int );
    bool partition_serach(vector<id> ,int, int , int );

} ;

#endif // WORDS_H
