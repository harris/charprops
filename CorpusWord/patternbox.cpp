#include  "window.h"

void Window::patternBoxes(QGridLayout * mainlayout)
{
    QGroupBox * formbigbox = new QGroupBox(tr("Pattern and form analysis")) ;
    mainlayout->addWidget(formbigbox, 0, 3) ;
    QVBoxLayout * formbiglayout = new QVBoxLayout ;
    formbigbox->setAlignment(Qt::AlignTop) ;
    formbigbox->setLayout(formbiglayout) ;

    // la boite de sélection des marques
    QGroupBox * markerselectionbox = new QGroupBox(tr("Marker selection")) ;
    formbiglayout->addWidget(markerselectionbox, 2) ;
    QGridLayout * markerselectionlayout = new QGridLayout ;
    markerselectionbox->setAlignment(Qt::AlignTop);
    markerselectionbox->setLayout(markerselectionlayout);

    FormsAnalyseButton = new QPushButton("Analyse corpus forms") ;
    //FormsAnalyseButton->setEnabled(false) ;
    connect(FormsAnalyseButton, SIGNAL(clicked()), this, SLOT(analyseForms())) ;
    markerselectionlayout->addWidget(FormsAnalyseButton, 0, 0, 1, -1) ;

    markerselectionlayout->addWidget(new QLabel(tr("Number of forms")), 1, 0);
    //----------------------------------------------------Number of Forms
    FormsNb = createLabel() ;
    markerselectionlayout->addWidget(FormsNb, 1, 1) ;

    markerselectionlayout->addWidget(new QLabel(tr("Number of markers")), 2, 0);
    //----------------------------------------------------Number of Markers TextBox
    AcceptedBox = new QLineEdit ;
    AcceptedBox->setValidator(new QIntValidator(0, MaxMarkers)) ;
    AcceptedBox->setReadOnly(true) ;
    markerselectionlayout->addWidget(AcceptedBox, 2, 1) ;

      //----------------------------------------------------Marker threshold TextBox
    markerselectionlayout->addWidget(new QLabel(tr("Marker threshold")), 3, 0) ;

    ThresholdBox = new QSpinBox ;
    ThresholdBox->setRange(1, 1000000) ;
    ThresholdBox->setValue(2) ;
    connect(ThresholdBox, SIGNAL(valueChanged()), this, SLOT(thresholdEntered())) ;
    markerselectionlayout->addWidget(ThresholdBox, 3, 1) ;

    markerselectionlayout->addWidget(new QLabel(tr("Marques")), 4, 0);
    createMarquesTable();
    markerselectionlayout->addWidget(MarquesTable, 4,0) ;

    PatternGenerationButton = new QPushButton("Generate pattern") ;
    //PatternGenerationButton->setEnabled(false) ;
    connect(PatternGenerationButton, SIGNAL(clicked()), this, SLOT(generatePattern())) ;
    markerselectionlayout->addWidget(PatternGenerationButton, 5, 0, 1, -1) ;

    // la boite des patterns
    QGroupBox * patternbox = new QGroupBox(tr("Pattern selection")) ;
    formbiglayout->addWidget(patternbox, 0) ;
    QGridLayout * patternslayout = new QGridLayout ;
    patternbox->setAlignment(Qt::AlignTop) ;
    patternbox->setLayout(patternslayout) ;

    patternslayout->addWidget(new QLabel(tr("Joker Length")), 0, 0);
    JokerLengthBox = new QSpinBox ;
    JokerLengthBox->setRange(1, 1000000) ;
    JokerLengthBox->setValue(10) ;
    connect(JokerLengthBox, SIGNAL(editingFinished()), this, SLOT(jokerLengthEntered())) ;
    patternslayout->addWidget(JokerLengthBox, 0, 1);


    patternslayout->addWidget(new QLabel(tr("Pattern threshold")), 1, 0);
    PatternThresholdBox = new QSpinBox ;
    PatternThresholdBox->setRange(1, 1000000) ;
    PatternThresholdBox->setValue(1) ;
    patternslayout->addWidget(PatternThresholdBox, 1, 1);

    patternslayout->addWidget(new QLabel(tr("Number of patterns")), 2, 0);
    AcceptedPatternBox = new QLineEdit ;
    AcceptedPatternBox->setValidator(new QIntValidator(0, MaxMarkers)) ;
    AcceptedPatternBox->setReadOnly(true) ;
    connect(AcceptedPatternBox, SIGNAL(editingFinished()), this, SLOT(acceptedPatternNumberEntered())) ;
    patternslayout->addWidget(AcceptedPatternBox, 2, 1) ;

     createPatternsTable();
     patternslayout->addWidget(PatternsTable, 4,0) ;

     TreeConstitutionButton12 = new QPushButton("Compare Results with WordNet") ;
     TreeConstitutionButton12->setEnabled(true) ;
     patternslayout->addWidget(TreeConstitutionButton12, 5, 0) ;
     //connect(TreeConstitutionButton12, SIGNAL(clicked()), this, SLOT(updateAWN()));
     //connect(TreeConstitutionButton12, SIGNAL(clicked()), this, SLOT(build_AWN()));
     connect(TreeConstitutionButton12, SIGNAL(clicked()), this, SLOT(FinalResult()));
}

void Window::createMarquesTable()
{
  MarquesTable = new QTableWidget(0, 2) ;
  MarquesTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
  MarquesTable->setHorizontalHeaderLabels(QStringList() << tr("Words") << tr("Frequency")) ;
  MarquesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
  MarquesTable->verticalHeader()->hide() ;
  MarquesTable->setShowGrid(false) ;
  MarquesTable->setContextMenuPolicy(Qt::CustomContextMenu);
}

void Window::createPatternsTable()
{
  PatternsTable = new QTableWidget(0, 1) ;
  PatternsTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
  PatternsTable->setHorizontalHeaderLabels(QStringList() << tr("Patterns")) ;
  PatternsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
  PatternsTable->verticalHeader()->hide() ;
  PatternsTable->setShowGrid(false) ;
  PatternsTable->setContextMenuPolicy(Qt::CustomContextMenu);
}

void Window::getMarques(int corpus,int maxmarkers)
{
    MarquesTable->clear();
    int counter=0;
    QSqlQuery req ;
    //req.exec(QString("select * from \"WORD_form\" where fk_corpus = %1 and occs >=%2").arg(corpus).arg(maxmarkers)) ;
    req.exec(QString("select * from \"WORD_form\" where  occs >=%1").arg(maxmarkers)) ;

    while (req.next())
    {
            QTableWidgetItem * Marque = new QTableWidgetItem(req.value(0).toString()) ;

            QTableWidgetItem * frequency = new QTableWidgetItem((req.value(1)).toString()) ;

            int row = MarquesTable->rowCount();
            MarquesTable->insertRow(row);
            MarquesTable->setItem(row, 0, Marque);
            MarquesTable->setItem(row, 1, frequency);
            counter++;
    }
    AcceptedBox->setText(QString::number(counter));
}

void Window::getPatterns()
{
    int counter=0;
    QSqlQuery req ;
    req.exec(QString("select * from \"WORD_pattern\"")) ;
    while (req.next())
    {
            QTableWidgetItem * pat = new QTableWidgetItem(req.value(1).toString()) ;
            int row = PatternsTable->rowCount();
            PatternsTable->insertRow(row);
            PatternsTable->setItem(row, 0, pat);
            counter++;
    }
    AcceptedPatternBox->setText(QString::number(counter));
}


void Window::analyseForms()
{
  MaxMarkers=ThresholdBox->text().toInt();
  FormsAnalyseButton->setEnabled(true) ;
  getMarques(IdCorpus,MaxMarkers);
  PatternGenerationButton->setEnabled(true) ;
  MsgBox("Fill in the Joker Length and Pattern Shreshold and Click on the Generate Patterns");
}

void Window::generatePattern()
{
  checkPreProcessing();
  PatternGenerationButton->setEnabled(true) ;
  pattern = new Patterns(IdCorpus, MaxMarkers, JokerLengthBox->text().toInt() ,PatternThresholdBox->text().toInt(), 2,  Time,language,language_param);
  getPatterns();
}

void Window::thresholdEntered()
{
    MaxMarkers=ThresholdBox->text().toInt();
    FormsAnalyseButton->setEnabled(true) ;
}
void Window:: FinalResult()
{
    int number_of_result_synsets=0;
    int number_of_true_in_result_synsets=0;
    int number_of_target_synsets=0;
    vector<vector<int>> result ;
    vector<vector<QString>> wordNet ;

    //----------------------------------------------------------Result --------------------------------------
    QSqlQuery req_result ;
    vector<int> temp_result ;
    QString row_result="";

    QString sql_Result_Corpus="select synset, id_word from \"WORD_synsets\" order by synset";
    req_result.exec(QString(sql_Result_Corpus)) ;
    while (req_result.next())
    {
      if(row_result!=req_result.value(0).toString())
      {
          if(temp_result.size()>0)
          {
                result.push_back(temp_result);
                temp_result.clear();
          }
          row_result=req_result.value(0).toString();
          temp_result.push_back(req_result.value(1).toInt());
          number_of_result_synsets++;
      }
      else
      {
          temp_result.push_back(req_result.value(1).toInt());
          number_of_result_synsets++;
      }
    }
    //----------------------------------------------------------End Result ----------------------------------
        qDebug() <<"#number_of_result_synsets= "+QString::number(number_of_result_synsets)+"\n";
    //----------------------------------------------------------Arabic WordNet-------------------------------
    QSqlQuery req ;
    vector<QString> temp ;
    QString row="";

    QString sql_Result_AWN="select synsetid,normalize from \"awn_word\"  where \"ExistInCorpus\"=1 order by synsetid";
    req.exec(QString(sql_Result_AWN)) ;
    while (req.next())
    {
      if(row!=req.value(0).toString())
      {
          if(temp.size()>0)
          {
              //if(Synset_Exist_In_Result(temp,result))
              //{
              //    number_of_true_in_result_synsets++;
               //   qDebug() <<"#number_of_true_in_result_synsets= "+QString::number(number_of_true_in_result_synsets)+"\n";
              //}
              wordNet.push_back(temp);

              temp.clear();
              number_of_target_synsets++;
          }

          row=req.value(0).toString();
          temp.push_back(req.value(1).toString());
      }
      else
          temp.push_back(req.value(1).toString());
    }
    //----------------------------------------------------------End Arabic WordNet----------------------------------
qDebug() <<"Number of target="+QString::number(wordNet.size());
    qDebug() <<"Last Step";;
    vector<int> vec;
    for(int i=0;i<result.size();i++)
    {
        vec=result[i];
        for(int j=0;j<wordNet.size();j++)
        {
            if(Intersection(wordNet[j],vec))
                number_of_true_in_result_synsets++;
        }
    }
    MsgBox("number_of_result_synsets="+QString::number(number_of_result_synsets)+"\n number_of_true_in_result_synsets="+QString::number(number_of_true_in_result_synsets)+"\n number_of_target_synsets="+QString::number(number_of_target_synsets));
}

bool Window::Synset_Exist_In_Result(vector<QString> synset, vector<vector<int>> result)
{
    vector<int> temp;
    for(int i=0;i<result.size();i++)
    {
      temp=result[i];
      if(Intersection(synset,temp))
          return true;
    }
    return false;
}
QString Window::form_of_id(int id)
{
    QSqlQuery req ;
    QString sql="select id,form from \"WORD_form\" where id=%1";
    req.exec(QString(sql).arg(id)) ;
    if (req.next())
           return req.value(1).toString();
    else
           return "";
}
bool Window::Intersection(vector<QString> syn,vector<int> res)
{
     int i,j,k=0,flag=0;

     for(i=0;i<syn.size();i++)
     {
              flag=0;
              for(j=0;j<res.size();j++)
              {
                   qDebug() <<syn[i]<< "             "<< form_of_id(res[j]);
                   if(syn[i]==form_of_id(res[j]))
                   {
                        flag=1;
                        break;
                   }
              }

              if(flag==1)
              {
                    k++;
              }
              if(k>=2)
                   return true;
     }

     if(k<2)
        return false;
}

/*
void Window:: updateAWN()
{
    QSqlQuery req ;
    QSqlQuery req1 ;
    QSqlQuery req2 ;

    req.exec(QString("select normalize from \"awn_word\" where authorshipid > 152000")) ;
    while (req.next())
    {
        QString word_=req.value(0).toString();

        if(Exist_in_form(word_))
            req1.exec(QString("update \"awn_word\" set \"ExistInCorpus\"=1 where value_='%1' and authorshipid>152000").arg(word_)) ;
    }

    req2.exec(QString("SELECT synsetid, string_agg(normalize, ', '), string_agg(file_, ', ')  FROM \"awn_word\" where authorshipid>152000 and \"ExistInCorpus\"=1 GROUP BY synsetid")) ;

    QTextStream out ;
    QFile outfile ;
    outfile.setFileName("synsets.txt");
    out.setCodec("UTF-8");
    QDir::setCurrent("../resultats/");
    if (not outfile.open(QIODevice::ReadWrite | QIODevice::Text))
        qDebug() << "ERREUR: Impossible de creer le fichier " << outfile.fileName() << " en ecriture lecture." << qPrintable(outfile.errorString()) << endl ;

     out.setDevice(& outfile);
     qDebug() << "Ajouter les synsets dans le fichier principal synsets.txt" ;

     while (req2.next())
        {
            QString synset=req2.value(1).toString();
            QString files=req2.value(2).toString();
            out << synset<<"    " <<files<< "\n";
        }
}
*/
bool Window::Exist_in_form(QString str)
{
    QSqlQuery req ;

    req.exec(QString("select * from \"WORD_form\" where form='%1'").arg(str)) ;

    if (req.next()) return true ;
    return false ;
}

