#include  "window.h"

void Window::preprocessingBoxes(QGridLayout * mainlayout)
{
  QGroupBox * preprocessingbox = new QGroupBox(tr("Pre-Processing")) ;
  mainlayout->addWidget(preprocessingbox, 0, 1) ;
  QGridLayout * preprocessinglayout= new QGridLayout ;
  preprocessingbox->setAlignment(Qt::AlignTop);
  preprocessingbox->setLayout(preprocessinglayout);

  preprocessinglayout->addWidget(new QLabel("Languages"), 0, 0) ;
  Languagebox = sqlGetLanguageBox() ;
  connect(Languagebox, SIGNAL(currentIndexChanged(int)), this, SLOT(showPreProcessing())) ;
  preprocessinglayout->addWidget(Languagebox, 0, 1) ;

  //Arabic Section
  preprocessinglayout->setAlignment(Qt::AlignTop);
  ar_title= new QLabel("For Arabic only");
  preprocessinglayout->addWidget(ar_title, 1, 0) ;
  ar_RemoveDiactrics= new QLabel("Remove Diactrics");
  preprocessinglayout->addWidget(ar_RemoveDiactrics, 2, 0) ;
  ar_r_diactrics= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_diactrics, 2, 1) ;
  ar_RemoveNumbers= new QLabel("Remove Numbers");
  preprocessinglayout->addWidget(ar_RemoveNumbers, 3, 0) ;
  ar_r_numbers= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_numbers, 3, 1) ;
  ar_RemoveForeignletters= new QLabel("Remove Foreign letters");
  preprocessinglayout->addWidget(ar_RemoveForeignletters, 4, 0) ;
  ar_r_f_letters= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_f_letters, 4, 1) ;
  ar_RemoveShaddaAndMadda= new QLabel("Remove Shadda and Madda");
  preprocessinglayout->addWidget(ar_RemoveShaddaAndMadda, 5, 0) ;
  ar_r_shadda_madad= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_shadda_madad, 5, 1) ;
  ar_RemoveAlef= new QLabel("Remove Alef");
  preprocessinglayout->addWidget(ar_RemoveAlef, 6, 0) ;
  ar_r_alef= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_alef, 6, 1) ;
  ar_RemoveYeh= new QLabel("Remove Yeh");
  preprocessinglayout->addWidget(ar_RemoveYeh, 7, 0) ;
  ar_r_yeh= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_yeh, 7, 1) ;
  ar_RemoveHeh= new QLabel("Remove Heh");
  preprocessinglayout->addWidget(ar_RemoveHeh, 8, 0) ;
  ar_r_heh= new QCheckBox();
  preprocessinglayout->addWidget(ar_r_heh, 8, 1) ;
  ar_stem= new QLabel("Stemming");
  preprocessinglayout->addWidget(ar_stem, 9, 0) ;
  stem = new QComboBox();
  stem->addItem("");
  stem->addItem("KHOJA STEMMER");
  preprocessinglayout->addWidget(stem, 9, 1) ;
  //End Arabic section

  removeArabicSection();
}


QComboBox * Window::sqlGetLanguageBox()
{ QSqlQueryModel * languagesqlmodel = new QSqlQueryModel();
  QComboBox * combobox = new QComboBox();
  languagesqlmodel->setQuery("SELECT name FROM language") ;
  combobox->setModel(languagesqlmodel) ;
  return combobox;
}

void Window::showPreProcessing()
{
    if(Languagebox->currentText()=="Arabic")
      appearArabicSection();
    else
      removeArabicSection();
}

void Window:: removeArabicSection()
{
    ar_title->hide();
    ar_RemoveDiactrics->hide();
    ar_r_diactrics->hide();
    ar_RemoveNumbers->hide();
    ar_r_numbers->hide();
    ar_RemoveForeignletters->hide();
    ar_r_f_letters->hide();
    ar_RemoveShaddaAndMadda->hide();
    ar_r_shadda_madad->hide();
    ar_RemoveAlef->hide();
    ar_r_alef->hide();
    ar_RemoveYeh->hide();
    ar_r_yeh->hide();
    ar_RemoveHeh->hide();
    ar_r_heh->hide();
    ar_stem->hide();
    stem->hide();
}

void Window:: appearArabicSection()
{
    ar_title->show();
    ar_RemoveDiactrics->show();
    ar_r_diactrics->show();
    ar_RemoveNumbers->show();
    ar_r_numbers->show();
    ar_RemoveForeignletters->show();
    ar_r_f_letters->show();
    ar_RemoveShaddaAndMadda->show();
    ar_r_shadda_madad->show();
    ar_RemoveAlef->show();
    ar_r_alef->show();
    ar_RemoveYeh->show();
    ar_r_yeh->show();
    ar_RemoveHeh->show();
    ar_r_heh->show();
    ar_stem->show();
    stem->show();
}

void Window:: checkPreProcessing()
{
    if(Languagebox->currentText()=="Arabic")
    {
        language="Arabic";
        if(ar_r_diactrics->isChecked())
        {
            ch_r_diactrics=true;
            language_param.append("true");
        }else
        {
            ch_r_diactrics=false;
            language_param.append("false");
        }

        if(ar_r_numbers->isChecked())
        {
            ch_r_numbers=true;
             language_param.append("true");
        }else
        {
            ch_r_numbers=false;
             language_param.append("false");
        }
        if(ar_r_f_letters->isChecked())
        {
            ch_r_f_letters=true;
             language_param.append("true");
        }else
        {
            ch_r_f_letters=false;
             language_param.append("false");
        }
        if(ar_r_shadda_madad->isChecked())
        {
            ch_r_shadda_madad=true;
             language_param.append("true");
        }else
        {
            ch_r_shadda_madad=false;
             language_param.append("false");
        }
        if(ar_r_alef->isChecked())
        {
            ch_r_alef=true;
             language_param.append("true");
        }else
        {
            ch_r_alef=false;
             language_param.append("false");
        }
        if(ar_r_yeh->isChecked())
        {
            ch_r_yeh=true;
             language_param.append("true");
        }else
        {
            ch_r_yeh=false;
             language_param.append("false");
        }
        if(ar_r_heh->isChecked())
        {
            ch_r_heh=true;
             language_param.append("true");
        }else
        {
            ch_r_heh=false;
             language_param.append("false");
        }
        if(stem->currentText()=="KHOJA STEMMER")
            language_param.append("KHOJASTEMMER");
        else
            language_param.append("");
    }
}
