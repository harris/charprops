#include "WordsDB.h"

// Constructeur de l'arbre des Words

WordsDB::WordsDB(int corpus)
{
  Trie = create_node((QChar) '/', true) ;

      addTokens(Trie,corpus) ;
}

WordsDB::~WordsDB()
{}

arbre_word WordsDB::create_node (QChar c, bool end, arbre_word son, arbre_word bro, val id_db)
{ arbre_word p = new node_word ;
  p->letter = c ;
  p->sons = son ;
  p->bros = bro ;
  if (end)
  { p->occurrences = 1 ;
    p->ID_DB=id_db;
    CountWords++ ;
  }
  else p->occurrences = 0 ;
  return p ;
}


/********************************************************************** Ajouter Words dans l'arbre **************************************/

void WordsDB::addTokens(arbre_word Trie,int corpus)
{
    QString str ;
    QSqlQuery req ;
     req.exec(QString("select form,id,occs from \"WORD_form\" where fk_corpus = %1").arg(corpus)) ;
     while (req.next())
    {
      str=req.value(0).toString();

      //qDebug() << str << req.value(1).toInt();
      insert(&str, Trie,0,req.value(1).toInt()) ;
   }

  /*   Trie=Trie->sons;
     qDebug() <<Trie->letter<< Trie->ID_DB;
     Trie=Trie->sons;
     qDebug() <<Trie->letter<< Trie->ID_DB;
     Trie=Trie->sons;
     qDebug() <<Trie->letter<< Trie->ID_DB;
     Trie=Trie->sons;
    qDebug() <<Trie->letter<< Trie->ID_DB;*/
}

// ===================================================================== Fabrication de l'arbre ==========================================

void WordsDB::insert(QStringRef word, arbre_word tree, int position, val id_db)
{
  arbre_word branch ;
  if (position >= word.size()) return ;
  branch = assoc(word.at(position), tree->sons) ;
  if (branch and (position == word.size() - 1)) 
  { branch->occurrences++ ;
    return ;
  }
  if (branch)
  { insert(word, branch, position + 1, id_db) ;
    return ;
  }
  branch = create_branch(word, tree, position,id_db) ;
  branch->bros = tree->sons ;
  tree->sons = branch ;
  return ;
}

arbre_word WordsDB::create_branch(QStringRef string, arbre_word tree, int position,val id_db)
{ if (position >= string.size()) return NULL ;
  if (position == string.size() - 1)
    return create_node(string.at(position), true, create_branch(string, tree, position + 1,id_db),NULL,id_db) ;
  return create_node(string.at(position), false, create_branch(string, tree, position + 1,id_db),NULL,id_db) ;
}

arbre_word WordsDB::assoc(QChar c, arbre_word tree)
{ if (not tree) return NULL ;
  if (tree->letter == c) return tree ;
  return assoc(c, tree->bros) ;
}

bool WordsDB::search(QStringRef word, arbre_word tree,int position)
{
  arbre_word temp_tree=tree;
  arbre_word branch ;
  if (position >= word.size()) return true;
  branch = assoc(word.at(position), temp_tree->sons) ;

  if (branch and (position == word.size() - 1))
  {
    return true;
  }
  if (branch)
  {
    return search(word, branch,position + 1) ;
  }
  return false;

}

val WordsDB::return_id(QStringRef word,arbre_word tree, int position)
{ arbre_word temp_tree=tree;
  arbre_word branch ;
  branch = assoc(word.at(position), temp_tree->sons) ;
  if (branch and (position == word.size() - 1))
  {
    return branch->ID_DB;
  }

  if (branch)
  {
    return return_id(word, branch, position + 1) ;
  }

    return -1;
}

//==============================================ponctuation=======================================================================

QStringRef WordsDB::removePunctuation(QStringRef string)
{ int punct = lastPunct(string) ;
  if (punct < string.size()) string = string.left(punct + 1) ;
  punct = firstPunct(string) ;
  if (punct > 0) string = string.mid(punct) ;
  return string ;
}

// Renvoyer la position de la ponctuation qui ferme le mot

int WordsDB::lastPunct(QStringRef string)
{ int position ;
  for (position = string.size() - 1 ; position >= 0 ; position--)
    if (not string.at(position).isPunct()) return position ;
  return position ;
}

// Renvoie la position du premier caractère qui n'est pas une ponctuation

int WordsDB::firstPunct(QStringRef string)
{ int position ;
  for (position = 0 ; position < string.size() ; position++)
    if (not string.at(position).isPunct()) return position ;
  return position ;
}

bool WordsDB::exist_word(QString w)
{
    bool exist=false;
    QSqlQuery req ;
    QSqlQuery update;
    QString sql="select id from \"WORD_form\" where form='%1'";
    req.exec(QString(sql).arg(w)) ;
    if (req.next())
    {
           exist=true;
           update.exec(QString("update \"WORD_form\" set occs=occs+1 where id=%1").arg(req.value(0).toString()));
    }
    else
        exist=false;

    return exist;
}


