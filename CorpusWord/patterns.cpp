#include "patterns.h"
#include <vector>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <locale>

using namespace std;

// Constructeur de l'arbre des patterns
Patterns::Patterns(int corpus, int seuilmarque, int jokerlength, int seuilpatern,int params, QElapsedTimer time, QString language, QVector<QString> language_param)
  : SeuilMarque(seuilmarque), CountMarques(0), CountPatterns(1), JokerLength(jokerlength), SeuilPattern(seuilpatern)
{
  JokerLength=jokerlength;
  SeuilPattern=seuilpatern;
  index=0;

  arbre_pattern Trie = create_node(-1,-1,NULL,NULL) ;


  QSqlQuery req ;
   req.exec(QString("select form,id,occs from \"WORD_form\" where fk_corpus = %1 order by id").arg(corpus)) ;
   while (req.next())
   {
       if(req.value(2).toInt()>=seuilmarque)
       {
           id_Marques.push_back(req.value(1).toInt());
           Marques.insert(req.value(0).toString(), req.value(1).toInt()) ;
       }
       else
       {
           Normales.insert(req.value(0).toString(), req.value(1).toInt()) ;
       }
   }

  JokerId = 0;

  req.exec(QString("select depot_url_copy from \"document\" where fk_id_corpus = %1").arg(corpus)) ;

  qDebug() << "Début de la construction de l'arbre préfixe (+ remplissage table pattern) : " << readabletime(time.elapsed()) ;
  int i=0;

  while (req.next())
  {
            QString file=QUrl(req.value(0).toString()).toLocalFile();
            addTokens(Trie,file , language,language_param,i) ;
            i++;
  }
QVector<int> identifiants;


qDebug() << "Début de l'insertion dans Trie";
qDebug() <<"#All= "+QString::number(all_segments.size());

  for(int k=0;k<all_segments.size();k++)
  {
      identifiants=all_segments[k];

      {
          insert(identifiants,k, Trie) ;
      }
      qDebug() <<"#Sgm : "+QString::number(k);
  }
qDebug() << "Fin de l'insertion dans Trie";


qDebug() << "Début de l'insertion dans pattern et matrix";
 sqlWriteBatch(Trie);
qDebug() << "Fin de l'insertion dans pattern et matrix";
}

void Patterns::addTokens(arbre_pattern Trie,QString file, QString language, QVector<QString> language_param,int fnum)
{
    QList<QList<QString> > dataSet;
    QList<QString> list;
        QString lastError = "";
        QFile inFile(file);
        if (inFile.open(QIODevice::ReadOnly)){
            QTextStream fread(&inFile);
            fread.setCodec("UTF-8");
            QString line;
            while(!fread.atEnd()){
                line = fread.readLine();
                QList<QString> record = line.split(QRegExp("\\s"));
                dataSet.append(record);
            }
        }else{
            lastError = "Could not open "+file+" for reading";
        }

//qDebug() <<file;
QVector<int> identifiants;
QVector<QString> identifiants_str;
QString string ;
QString string1 ;
int id;
for(int t=0;t<dataSet.length();t++)
{
   //=======================Read Segment==============================================================================
   list =dataSet[t];
    for(int t1=0;t1<list.length();t1++)
    {
            string=list[t1];
            string1=string;
           if(language=="Arabic")
           {
                  string=ArabicNormalizer::normalize(string,language_param[0],language_param[1],language_param[2],language_param[3],language_param[4],language_param[5],language_param[6]);

                   if(language_param[7]=="KHOJASTEMMER")
                   {
                       KhojaStemmer::readInStaticFiles("../KhojaStemmerFiles/");
                       QString::fromStdWString(KhojaStemmer::stemWord(string.toStdWString()));
                   }
          }
           if(stopsegment(&string1)>0)
           {

              string=removePunctuation(string, language);
              id=AnalyseWord(string);
              identifiants.append(id);
              identifiants_str.append(string);
              //=============================================Insert Into Trie===
              if(valid_segment_str(identifiants_str))
              {
                    all_segments.append(identifiants);
              }
              identifiants.clear();
              identifiants_str.clear();
              //=============================================End Insert Into Trie===
           }
           else
           {
              string=removePunctuation(string, language);
              id=AnalyseWord(string);
              identifiants.append(id);
              identifiants_str.append(string);
           }
    }
    //=======================End Read Segment===============================================================================
}
dataSet.clear();
}
//==========================================================test string : if it's Marque or not====================
val Patterns::AnalyseWord(QString string)
{
   if (Marques.contains(string))
    {
        CountMarques++;
        return Marques.value(string);
    }
    else
    {
        if(string!="")
        {
            if(Normales.contains(string))
                return Normales.value(string);
            else
                return -1 ;
        }
    }
}
//=====================================================================================================================
int Patterns::stopsegment(QStringRef string)
{   if(string.size()==1)
        if (string.at(0).isPunct()) return 1;

    int position=string.size() - 1 ;
      if (string.at(position).isPunct()) return position ;
    return 0 ;
}

void Patterns::PrintSegment(QVector< QVector<int> > segms)
{
    QFile file("../resultats/segments.txt");
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
    QTextStream flux(&file);
    flux.setCodec("UTF-8");
    QVector<int> vec;
    for(int i=0;i<segms.size();i++)
    {
        vec=segms[i];

        for(int j=0;j<vec.length();j++)
            flux << ReadWord(vec[j])<<" ";
        flux << endl;
    }

    file.close();
}

bool Patterns::valid_segment(QVector<int> identifiant)
{
    int all_marque=1;
    int all_normal=1;
    for(int i=0; i<identifiant.size(); i++)
    {
        int ref=identifiant[i];

        if(Normales.values().contains(ref))
            all_normal=0;

        if(Marques.values().contains(ref))
            all_marque=0;
    }

    if(!all_marque && !all_normal)
            return true;
   return false;
}

bool Patterns::valid_segment_str(QVector<QString> identifiant)
{
    int all_marque=1;
    int all_normal=1;
    for(int i=0; i<identifiant.size(); i++)
    {
        QString str=identifiant[i];

        if(Normales.contains(str))
            all_normal=0;

        if(Marques.contains(str))
            all_marque=0;
    }

    if(!all_marque && !all_normal)
            return true;
   return false;
}

//===================================================================== Fabrication de l'arbre ==========================================
arbre_pattern Patterns::create_node (id marque, id sortie, arbre_pattern son, arbre_pattern bro)
{ arbre_pattern p = new node_pattern ;
   if(!search_vector(id_Marques,marque))
    p->m_id = JokerId ;
        else
    p->m_id = marque ;

  p->sons = son ;
  p->bros = bro ;
  p->occurrences=1;
  if (sortie)
    p->sortie = CountPatterns++ ;
  else
   p->sortie  = 0;
  return p ;
}
void Patterns::insert(QVector<int> identifiant, int line,arbre_pattern tree, int position)
{
    arbre_pattern branch ;
    QVector<JokerElement> tmp;
    JokerElement tmp_joker;
    if (position >= identifiant.length()) return ;

    branch = assoc(identifiant[position], tree->sons) ;

    if (branch && (position == identifiant.length() - 1))
    {
        if(branch->m_id==JokerId)
        {
            tmp_joker.key=identifiant[position];
            tmp_joker.segmentIndex=line;
                    tmp=JokerElements.value(branch);
                    tmp.append(tmp_joker);
                    JokerElements.insert(branch,tmp);
        }

      branch->occurrences++ ;
      return ;
    }
    if (branch )
    {
        if(branch->m_id==JokerId)
        {
            tmp_joker.key=identifiant[position];
            tmp_joker.segmentIndex=line;
            tmp=JokerElements.value(branch);
            tmp.append(tmp_joker);
            JokerElements.insert(branch,tmp);
        }
      branch->occurrences++ ;
      insert(identifiant, line, branch, position + 1) ;

      return ;
    }
    branch = create_branch(identifiant, tree, position) ;

    arbre_pattern temp=branch;
    int nbr;

    if(position)
        nbr=position;
    else
        nbr=0;
    while (temp)
    {
        if(temp->m_id==JokerId)
        {
            tmp_joker.key=identifiant[nbr];
            tmp_joker.segmentIndex=line;
                    tmp=JokerElements.value(temp);
                    tmp.append(tmp_joker);
                    JokerElements.insert(temp,tmp);
        }
          nbr=nbr+1;
          temp=temp->sons;
    }
    branch->bros = tree->sons ;
    tree->sons = branch ;

    return ;
}

arbre_pattern Patterns::create_branch(QVector<int> identifiant, arbre_pattern tree, int position)
{    int sortie=0;
         if(!search_vector(id_Marques,identifiant[position]))
         sortie=1;
     else
     {
         for(int i=0;i<position;i++)
         {
             if(!search_vector(id_Marques,identifiant[i]))
             {sortie=1;
             }
         }
     }
   if (position > identifiant.length()-1) return NULL ;

   if (position == identifiant.length() - 1)
         return create_node(identifiant[position], sortie);

   return create_node(identifiant[position],sortie, create_branch(identifiant, tree, position + 1)) ;
}

arbre_pattern Patterns::assoc(id m, arbre_pattern tree)
{

   if (not tree) return NULL ;
   id ma;

     if(!search_vector(id_Marques,m))
        ma=JokerId;
    else
        ma=m;

  if (tree->m_id == ma)
  {
      tree->occurrences++;
      return tree ;
  }
  return assoc(m, tree->bros) ;
}

//==============================================ponctuation=======================================================================

QString Patterns::removePunctuation(QString string, QString Language)
{ int punct = lastPunct(&string, Language) ;
  if (punct < string.size()) string = string.left(punct + 1) ;
  punct = firstPunct(&string, Language) ;
  if (punct > 0) string = string.mid(punct) ;
  return string ;
}

// Renvoyer la position de la ponctuation qui ferme le mot

int Patterns::lastPunct(QStringRef string, QString Language)
{int position ;
    for (position = string.size() - 1 ; position >= 0 ; position--)
      if (not string.at(position).isPunct()) return position ;
    return position ;
}

// Renvoie la position du premier caractère qui n'est pas une ponctuation

int Patterns::firstPunct(QStringRef string, QString Language)
{ int position ;
  for (position = 0 ; position < string.size() ; position++)
    if (not string.at(position).isPunct()) return position ;
  return position ;
}
//=========================================================================écriture pattern dans la base de données ==================================================================

void Patterns::sqlWriteBatch(arbre_pattern Trie)
{ QSqlQuery req ;

  QVector<int> ar;
  QVector<arbre_pattern> ar_ref;
  req.prepare("INSERT INTO \"WORD_pattern\" (pattern, stoplist,occs) VALUES (:pattern, :stoplist, :occs) ;");
  QSqlQuery req_matrix;
  req_matrix.prepare("INSERT INTO \"WORD_matrix\" (fk_form, fk_pattern, val, stoplist) VALUES (:form,:pattern, :val, :stoplist) ;");
  sqlWritePatterns(Trie->sons, ar_ref, ar,req,req_matrix) ;
  sqlWriteMatrice(0,0,1,1,true,req_matrix);

}

void Patterns::sqlWritePatterns(arbre_pattern tree, QVector<arbre_pattern> identifiant_ref,QVector<int> identifiant,  QSqlQuery req,QSqlQuery req_matrix)
{   int id_pattern;
    QVector<JokerElement> tmp;
    if (not tree) return ;
    identifiant.append(tree->m_id);
    identifiant_ref.append(tree);
    bool flag=false;


    if(JokerElements.contains(tree))
    {
       tmp= JokerElements.value(tree);

       for(int i=0;i<tmp.size();i++)
       {
           tmp_pa.append(tmp[i]);
       }
    }
    QString pattern;
    QString P;
    if (tree->sortie && checkJokerLength(identifiant,JokerLength) && valid_pattern(identifiant))
    {
       if(tree->occurrences>=SeuilPattern)
        {
                pattern=Convert_vector_to_String(identifiant);

               //for(int m=0;m<identifiant.size();m++)
               //   P=P+" "+ReadWord(identifiant[m]);

               //qDebug() << P;
               //P="";

               id_pattern=sqlWritePattern(pattern, "0",tree->occurrences,req) ;
               for(int j=0;j<tmp_pa.size();j++)
                  {
                        if(tmp_pa[j].key !=JokerId)
                        {
                            if(valid_normal_with_pattern(tmp_pa[j], identifiant))
                            {
                               sqlWriteMatrice(tmp_pa[j].key,id_pattern,1,1,flag,req_matrix);
                                //qDebug() << ReadWord(tmp_pa[j].key);
                                //qDebug() << tmp_pa[j].segmentIndex;
                            }
                        }
                  }
        }
    }
    sqlWritePatterns(tree->sons , identifiant_ref, identifiant, req,req_matrix) ;
    identifiant.removeLast();
    identifiant_ref.removeLast();
    sqlWritePatterns(tree->bros , identifiant_ref, identifiant, req,req_matrix) ;
}

int Patterns::sqlWritePattern(QString pattern, QString stoplist,int occurrence,QSqlQuery req,bool flag)
{ int id_pattern;
  static QVariantList patterns ;
  static QVariantList stoplists ;
  static QVariantList occurrences ;

    req.bindValue(":pattern", pattern) ;
    req.bindValue(":stoplist", stoplist) ;
    req.bindValue(":occs", occurrence) ;
    req.exec() ;

    patterns.clear() ;
    stoplists.clear() ;
    occurrences.clear();
    id_pattern=req.lastInsertId().toInt();
    return id_pattern;
}

//=========================================================================écriture Matrice dans la base de données ==================================================================

void Patterns::sqlWriteMatrice(int form, int pattern, int val, bool stoplist,bool flag, QSqlQuery req_matrix)
{
    static QVariantList forms ;
    static QVariantList patterns ;
    static QVariantList vals ;
    static QVariantList stoplists ;
    static int counter;

    if (not flag)
    {
        forms << form;
        patterns << pattern;
        vals << val;
        stoplists << stoplist;
        counter++;
    }
    if(counter==5000 or flag)
    {
        req_matrix.bindValue(":form", forms) ;
        req_matrix.bindValue(":pattern", patterns) ;
        req_matrix.bindValue(":val", vals) ;
        req_matrix.bindValue(":stoplist", stoplists) ;
        req_matrix.execBatch();

        forms.clear();
        patterns.clear();
        vals.clear();
        stoplists.clear();
        counter=0;

    }
}
//=========================================================================Convert vector to string======================================
QString Patterns::Convert_vector_to_String(QVector<int> identifiant)
{   QString pattern="{";
    for(int i=0; i<identifiant.size(); i++)
    {
        pattern += QString::number(identifiant[i]);
        if(i<identifiant.size()-1)
        pattern += "," ;
    }
    pattern+="}";
    return pattern;
}

QString Patterns::ReadWord(int id)
{
    QSqlQuery req ;
    if(id==JokerId)
        return "*";
    req.exec(QString("select * from \"WORD_form\" where id=%1").arg(id)) ;

    if (req.next()) return req.value(0).toString() ;
    return "NOT FOUND" ;
}


bool Patterns::valid_pattern(QVector<int> identifiant)
{
    for(int i=0; i<identifiant.size(); i++)
    {
        if(identifiant[i]==JokerId)
            return true;
    }
   return false;
}

bool Patterns::valid_normal_with_pattern(JokerElement n, QVector<int> p)
{
    QVector<int> segment;
    int exist_in_segment=false;
    segment=all_segments[n.segmentIndex];
       if(segment.contains(n.key))
       {
           for(int j=0;j<p.size();j++)
           {
               if(p[j]!=JokerId)
               {
                   if(segment.contains(p[j]) && p[j]==segment[j])
                        exist_in_segment=true;
                   else
                   {
                       exist_in_segment=false;
                       break;
                   }
               }
           }
       }

   return exist_in_segment;
}
bool Patterns::type_of_word_in_table_form(int id, QString word, QString type_of_word, int seuil_marque,int flag)
{
    bool exist=false;
    QSqlQuery req ;

    QString sql="select id from \"WORD_form\" where  ";
    if(flag==0)
        sql= sql + "  id=%1 ";
    if(flag==1)
        sql=sql + "  form='%1' ";

    if(type_of_word=="marque")
    {
        sql=sql + " and occs>=%2";
    }

    if(type_of_word=="normale")
    {
        sql=sql + " and occs<%2";
    }

    if(flag==0)
        req.exec(QString(sql).arg(id).arg(seuil_marque)) ;
    if(flag==1)
        req.exec(QString(sql).arg(word).arg(seuil_marque)) ;

    if (req.next())
           exist=true;
    else
        exist=false;

    return exist;
}

bool Patterns:: checkJokerLength(QVector<int> id, int JLength)
{
    int lJockerLenght=0;
    if(id.count(JokerId)>JLength)
    {
        for(int i=0;i< id.size();i++)
        {
            if (id[i]==JokerId)
            {
                lJockerLenght++;
                if(lJockerLenght>JLength)
                    return false;
            }
            else
                lJockerLenght=0;

        }
    }
    return true;

}

int Patterns::exist_pattern(QString P)
{
    QSqlQuery req ;
    QString sql="select id,pattern from \"WORD_pattern\" where pattern='%1'";
    req.exec(QString(sql).arg(P)) ;
    if (req.next())
           return req.value(0).toInt();
    else
           return 0;
}

int Patterns::id_of_word(QString P)
{
    QSqlQuery req ;
    QString sql="select id,form from \"WORD_form\" where form='%1'";
    req.exec(QString(sql).arg(P)) ;
    if (req.next())
           return req.value(0).toInt();
    else
           return 0;
}
bool Patterns::search_vector(vector<id> vec, int id)
{
    int iPremier=0;
    int iDernier=vec.size()-1;
    bool iTrouve=false;
    int iMilieu;

    while((iPremier <= iDernier)&&(iTrouve==false))
    {
        iMilieu=(iPremier+iDernier)/2;

        if(vec[iMilieu]==id) {iTrouve =true;break;}
        else
        {
                if(vec[iMilieu]>id) iDernier = iMilieu -1;
            else
                iPremier = iMilieu +1;
         }
    }
    return iTrouve;
}

