#include  "window.h"

// fonction qui crée les boites pour la sélection du  corpus
// devrait être partagée entre les applications qui en ont besoin
// pour l'instant nécessite une recopie de ce fichier + dupliquer la fonction createLabel
// createLabel devrait être une fonction héritée d'une superclasse Window dont chaque sous-classe serait différente
// et corpusbox devrait aussi être une classe
// Bdd et les autres globales sont à reconstituer
// Globales : Bdd Corpus
// CorpusBox, LoadCorpusButton, ConstDate, NumberFiles, CorpusSize, CorpusEncoding, CorpusFormats, CorpusView, CorpusModel

void Window::corpusBoxes(QGridLayout * mainlayout)
{ // la grande boite corpus
  QGroupBox * corpusbigbox = new QGroupBox(tr("Corpus selection")) ;
  mainlayout->addWidget(corpusbigbox, 0, 0) ;
  QGridLayout * corpusbiglayout= new QGridLayout ;
  corpusbigbox->setAlignment(Qt::AlignTop);
  corpusbigbox->setLayout(corpusbiglayout);
  
  // la petite boite corpus
  QGroupBox * corpusbox = new QGroupBox(tr("")) ;
  corpusbiglayout->addWidget(corpusbox, 0, 0) ;
  QGridLayout * corpuslayout = new QGridLayout ;
  corpusbox->setAlignment(Qt::AlignTop) ;
  corpusbox->setLayout(corpuslayout) ;
  
  corpuslayout->addWidget(new QLabel("Corpus"), 0, 0) ;
  // bdd.ouvrirConnexion() ; // inutile la première fois
  CorpusBox = sqlGetCorpusBox() ;
  //Bdd.fermerConnexion() ;
  corpuslayout->addWidget(CorpusBox, 0, 1) ;
  LoadCorpusButton = new QPushButton("Load") ;     // utile si redevient clickable (si un reset est possible)
  corpuslayout->addWidget(LoadCorpusButton, 0, 2) ;
  connect(LoadCorpusButton, SIGNAL(clicked()), this, SLOT(showCorpus())) ;

  CreateWordNetCorpusButton = new QPushButton("Create WordNetCorpus") ;
  corpuslayout->addWidget(CreateWordNetCorpusButton, 1, 2) ;
  connect(CreateWordNetCorpusButton, SIGNAL(clicked()), this, SLOT(build_AWN_corpus())) ;


  // la grande boite des documents
  QGroupBox * docbox = new QGroupBox(tr("contains")) ;
  corpusbiglayout->addWidget(docbox,2, 0) ;
  QVBoxLayout * doclayout = new QVBoxLayout ;
  docbox->setAlignment(Qt::AlignTop) ;
  docbox->setLayout(doclayout) ;
  
  // la boite des urls
  createUrlTable() ;
  doclayout->addWidget(CorpusView, 0) ;

  // la boite des descripteurs
  QGroupBox * descriptionbox = new QGroupBox(tr("")) ;
  doclayout->addWidget(descriptionbox, 1) ;
  QGridLayout * descriptionlayout= new QGridLayout ;
  descriptionbox->setLayout(descriptionlayout) ;
  descriptionbox->setAlignment(Qt::AlignTop);
  
  descriptionlayout->addWidget(new QLabel(tr("Creation date")), 0, 0) ;
  ConstDate = createLabel() ;
  descriptionlayout->addWidget(ConstDate, 0, 1) ;
  descriptionlayout->addWidget(new QLabel(tr("Number of files")), 1, 0) ;
  NumberFiles = createLabel() ;
  descriptionlayout->addWidget(NumberFiles, 1, 1) ;
  CorpusSize = createLabel() ;
  descriptionlayout->addWidget(new QLabel(tr("Corpus size")), 2, 0) ;
  descriptionlayout->addWidget(CorpusSize, 2, 1) ;
  descriptionlayout->addWidget(new QLabel(tr("Charsets")), 3, 0) ;
  CorpusEncoding = createLabel() ;
  descriptionlayout->addWidget(CorpusEncoding, 4, 0) ;
  descriptionlayout->addWidget(new QLabel(tr("Formats")), 3, 1) ;
  CorpusFormats = createLabel() ;
  descriptionlayout->addWidget(CorpusFormats, 4, 1) ; 
}

QComboBox * Window::sqlGetCorpusBox()
{ QSqlQueryModel * corpussqlmodel = new QSqlQueryModel();
  QComboBox * combobox = new QComboBox();
  corpussqlmodel->setQuery("SELECT nom_corpus FROM corpus") ;
  combobox->setModel(corpussqlmodel) ;
  return combobox;
}

void Window::createUrlTable()
{ CorpusView = new QTableView ;
  CorpusModel = new QStandardItemModel();
  CorpusView->verticalHeader()->hide();
  CorpusView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  CorpusView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
  CorpusView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
  CorpusModel->setHorizontalHeaderLabels(QStringList() << tr("Url"));
}


void Window::showCorpus()
{ LoadCorpusButton->setEnabled(false) ;
  
  CorpusModel->setRowCount(0) ;
  QSqlQuery req ;
  req.exec(QString("select depot_url_copy, fk_id_corpus from document where fk_id_corpus = (select id from corpus where nom_corpus = '%1')").arg(CorpusBox->currentText())) ;
  while (req.next())
  { QStandardItem * item = new QStandardItem() ;
    item->setText(req.value(0).toString()) ;
    CorpusModel->appendRow(item) ;
  }
  CorpusView->setModel(CorpusModel) ;
  if (not req.first()) return ;
  NumberFiles->setNum(req.numRowsAffected()) ;
  IdCorpus = req.value(1).toInt() ;
  req.clear() ;
  req.exec(QString("select SUM(size) from document where fk_id_corpus = %1").arg(IdCorpus)) ;
  if (req.next()) CorpusSize->setText(QString::number(((req.value(0).toInt()) + 1023) / 1024) + " Ko") ;
  CorpusFormats->setText(sqlGetFormats()) ;
  CorpusEncoding->setText(sqlGetEncoding()) ;
  TreeConstitutionButton11->setEnabled(true) ;
}

QString  Window::sqlGetFormats()
{ QString listformats ;
  QSqlQuery req ;
  req.exec(QString("(select distinct extension from format, document where fk_id_corpus = %1 and format.id = document.fk_id_format)").arg(IdCorpus));
  while (req.next()) listformats.append(req.value(0).toString() +", ") ;
  return listformats ;
}

QString Window::sqlGetEncoding()
{ QString listencoding ;
  QSqlQuery req ;
  req.prepare("(select distinct designation_codage from codage, document where fk_id_corpus=:idcorpus and   codage.id = document.fk_id_codage)");
  req.bindValue(":idcorpus", IdCorpus) ;
  req.exec() ;
  while (req.next()) listencoding.append(req.value(0).toString() +", ") ;
  return listencoding ;
}

QLabel * Window::createLabel()
{ QLabel * label = new QLabel ;
  label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred) ;
  label->setWordWrap(true);
  return label ;
}
void Window::build_AWN_corpus()
{

     QSqlQuery req ;
      QSqlQuery req1 ;

     req.exec(QString("select depot_url_copy, id from \"document\" where fk_id_corpus ='%1'").arg(IdCorpus)) ;
     while (req.next())
     {
         QString file=QUrl(req.value(0).toString()).toLocalFile();

         //qDebug() << file;
         if(!check_file_in_AWN(file))
         {

             req1.exec(QString("delete from \"document\" where id=%1").arg(req.value(1).toInt())) ;
         }
       //infile.close() ;
     }


}

bool Window::check_file_in_AWN(QString file)
{
    QList<QList<QString> > dataSet;
    QList<QString> list;
    QString word;
     QSqlQuery req ;
    bool result =false;
        QString lastError = "";
        QFile inFile(file);
        if (inFile.open(QIODevice::ReadOnly)){
            QTextStream fread(&inFile);
            fread.setCodec("UTF-8");
            QString line;
            while(!fread.atEnd()){
                line = fread.readLine();
                QList<QString> record = line.split(QRegExp("\\s"));
                dataSet.append(record);
            }
        }else{
            lastError = "Could not open "+file+" for reading";
        }

        for(int t=0;t<dataSet.length();t++)
        {
            list =dataSet[t];
             for(int t1=0;t1<list.length();t1++)
             {
                word =list[t1];
                            // qDebug() << word;
                if(searchAWN(word,file))
                {
                     return true;
                }
             }
        }
        return result;
}


bool Window::searchAWN(QString P, QString file)
{
    QSqlQuery req ;
    QSqlQuery req1 ;
    QString sql="select * from \"awn_word\" where normalize='%1' ";
    req.exec(QString(sql).arg(P)) ;
    if (req.next())
    {
        req1.exec(QString("update \"awn_word\" set \"ExistInCorpus\"=1 where normalize='%1'").arg(P)) ;
        return true;
    }
    else
           return false;
}


