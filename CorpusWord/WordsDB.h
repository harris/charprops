#include <QtWidgets>
#include "../Common/Types.h"
#include "../Common/BDD.h"

// l'élément de l'arbre préfixe
struct node_word
{ QChar letter ;
  val occurrences ;
  bool composite ; // le mot contient une ponctuation
  val ID_DB;
  /* autre information utile ici */
  node_word * sons ;
  node_word * bros ;
} ;

typedef node_word * arbre_word ;

class WordsDB : public QWidget
{  
  BDD                   Bdd ;
  int                   CountWords;
public:
  arbre_word                 Trie;
  public:
    WordsDB(int) ;
    ~WordsDB();
   
    // Recherche
    bool research(QStringRef, arbre_word, int = 0) ;

  private:
    void addTokens(arbre_word,int );
    inline void addWord(QString) ;

   arbre_word create_node(QChar,bool = false, arbre_word = NULL, arbre_word = NULL, val=0) ;
   void  insert(QStringRef, arbre_word, int = 0, val=0);
   arbre_word create_branch(QStringRef , arbre_word ,int ,val=0);
   arbre_word assoc(QChar, arbre_word);


   int lastPunct(QStringRef) ;
   int firstPunct(QStringRef) ;
   QStringRef removePunctuation(QStringRef) ;
   bool exist_word(QString );
   void printTrie(int) ;

public:
   bool search(QStringRef,arbre_word, int=0);
   val return_id(QStringRef,arbre_word, int=0);
} ;



