#include "Words.h"

// Constructeur de l'arbre des Words

Words::Words(int corpus, QElapsedTimer time,QString language, QVector<QString> language_param)
{ //arbre Trie;
  arbre Trie = create_node((QChar) '/', true) ;
  QSqlQuery req ;
  req.exec(QString("select depot_url_copy from document where fk_id_corpus ='%1'").arg(corpus)) ;
  QTextStream flux ;
  QFile infile ;

  qDebug() << "Début de la construction de l'arbre préfixe" ;
  while (req.next())
  {
      QString file=QUrl(req.value(0).toString()).toLocalFile();
      addTokens(Trie,file , language,language_param) ;

    infile.close() ;
  }
  qDebug() << "Début de l'insertion dans la table WORD_form" ;
  sqlWriteBatch(corpus, Trie);
  qDebug() << "Fin de l'insertion dans la table WORD_form" ;
  //qDebug() << "Fin de la construction de l'arbre préfixe";
 // qDebug() << "Début de l'insertion dans la table WORD_form" ;
  //sqlWriteBatch(corpus, Trie);
  //qDebug() << "Fin de l'insertion dans la table WORD_form" ;
  //qDebug() << "Début de l'insertion dans la table WORD_noeud" ;
  //saveTrie(corpus);
  //qDebug() << "Fin de l'insertion dans la table WORD_noeud" ;
}

Words::~Words()
{}

arbre Words::create_node (QChar c, bool end, arbre son, arbre bro)
{ arbre p = new node ;
  p->letter = c ;
  p->sons = son ;
  p->bros = bro ;
  if (end)
  { p->occurrences = 1 ;
    CountWords++ ;
  }
  else p->occurrences = 0 ;
  return p ;
}

/********************************************************************** Ajouter Words dans l'arbre **************************************/

void Words::addTokens(arbre Trie,QString file, QString language, QVector<QString> language_param)
{
    QList<QList<QString> > dataSet;
    QList<QString> list;
        QString lastError = "";
        QFile inFile(file);
        if (inFile.open(QIODevice::ReadOnly)){
            QTextStream fread(&inFile);
            fread.setCodec("UTF-8");
            QString line;
            while(!fread.atEnd()){
                line = fread.readLine();
                QList<QString> record = line.split(QRegExp("\\s"));
                dataSet.append(record);
            }
        }else{
            lastError = "Could not open "+file+" for reading";
        }



  QString str ;
  for(int t=0;t<dataSet.length();t++)
    {
      list =dataSet[t];
      for(int t1=0;t1<list.length();t1++)
      {
      str=list[t1];

      if(language=="Arabic")
      {
            str=ArabicNormalizer::normalize(str,language_param[0],language_param[1],language_param[2],language_param[3],language_param[4],language_param[5],language_param[6]);

            if(language_param[7]=="KHOJASTEMMER")
            {
                KhojaStemmer::readInStaticFiles("../KhojaStemmerFiles/");
                QString::fromStdWString(KhojaStemmer::stemWord(str.toStdWString()));
            }
      }
    insert(removePunctuation(& str), Trie) ;
  }
  }

}

// ===================================================================== Fabrication de l'arbre ==========================================

//Cette fonction couvre 4 cas :
//Case 1:  le mot est fini (1er  if)
//Case 2:  le mot exactement est dans l'arbre, occurrences++ de la derniere lettre (2er  if)
//Case 3:  Une partie du mot est dans l'arbre (3ème if)
//Case 4:  autres cas (la suite) : la lettre n'est pas dans l'arbre, exemple
//         "abcd", "abef" and "abgh"
//         /  ----> root
//         |
//         a
//         |
//         b
//         |
//         c <=== e <=== g
//         |      |      |
//         d      f      h

void Words::insert(QStringRef word, arbre tree, int position)
{ arbre branch ;
  if (position >= word.size()) return ;
  branch = assoc(word.at(position), tree->sons) ;
  if (branch and (position == word.size() - 1)) 
  { branch->occurrences++ ;
    return ;
  }
  if (branch)
  { insert(word, branch, position + 1) ;
    return ;
  }
  branch = create_branch(word, tree, position) ;
  branch->bros = tree->sons ;
  tree->sons = branch ;
  return ;
}

arbre Words::create_branch(QStringRef string, arbre tree, int position)
{ if (position >= string.size()) return NULL ;
  if (position == string.size() - 1)
    return create_node(string.at(position), true, create_branch(string, tree, position + 1)) ;
  return create_node(string.at(position), false, create_branch(string, tree, position + 1)) ;
}

arbre Words::assoc(QChar c, arbre tree)
{ if (not tree) return NULL ;
  if (tree->letter == c) return tree ;
  return assoc(c, tree->bros) ;
}

//==============================================ponctuation=======================================================================

// Dans cette méthode on provoque un deep-copy par mot
// Il vaudrait mieux soit renvoyer un QStringRef
// Soit modifier directement la QString avec argument QString & string
/*

QString Words::removePunctuation(QString string)
{ QString newstring = string ;
  int punct = lastPunct(string) ;
  if (punct < string.size()) newstring = string.left(punct + 1) ;
  punct = firstPunct(newstring) ;
  if (punct > 0) newstring = newstring.mid(punct) ;
  return newstring;
}
*/

QStringRef Words::removePunctuation(QStringRef string)
{ int punct = lastPunct(string) ;
  if (punct < string.size()) string = string.left(punct + 1) ;
  punct = firstPunct(string) ;
  if (punct > 0) string = string.mid(punct) ;
  return string ;
}

// Renvoyer la position de la ponctuation qui ferme le mot

int Words::lastPunct(QStringRef string)
{ int position ;
  for (position = string.size() - 1 ; position >= 0 ; position--)
    if (not string.at(position).isPunct()) return position ;
  return position ;
}

// Renvoie la position du premier caractère qui n'est pas une ponctuation

int Words::firstPunct(QStringRef string)
{ int position ;
  for (position = 0 ; position < string.size() ; position++)
    if (not string.at(position).isPunct()) return position ;
  return position ;
}


//=========================================================================écriture Word dans la base de données ==================================================================

void Words::sqlWriteBatch(int corpusid,arbre Trie)
{ QSqlQuery req ;
  req.prepare("INSERT INTO \"WORD_form\" (form, occs, composite, fk_corpus) VALUES (:word, :occs,  :composite, :corpus) ;");
  sqlWriteWords(Trie->sons, "", corpusid, req) ;
  sqlWriteWord("", 0, corpusid, req, false, true) ;
}

// composite est là pour gérer le cas des ponctuations en milieu de mot
// pour un post traitement éventuel
// apriori ce ne sera pas le cas de l'arabe (anglais ?) - en fait, si -, mais sûrement pour le français.
// les fils d'un composite sont composites
// mais pas ses frères

/* Pour le français :
 * 
 * Une lettre + ' ou (lors/quoi)qu + ' => découper.
 * Aujourd'hui => ne pas découper.
 * 
 * Comment faire ? Chaque langue a ses règles. Une base de règles ?
 * et que faire de passa-t-il, vient-il, venez-vous ?
 * 
 */

void Words::sqlWriteWords(arbre tree, QString word, int corpus, QSqlQuery req, bool composite)
{ if (not tree) return ;
  word.append(tree->letter) ;
  if (tree->occurrences)
  {


      sqlWriteWord(word, tree->occurrences, corpus, req, tree->letter.isPunct()) ;
  }
  sqlWriteWords(tree->sons, word, corpus, req, tree->letter.isPunct()) ;
  sqlWriteWords(tree->bros, word.left(word.size() - 1), corpus, req, composite) ;
}

void Words::sqlWriteWord(QString word, val occs, int corpus, QSqlQuery req, bool composite, bool flag)
{ static QVariantList words ;
  static QVariantList alloccs ;
  static QVariantList allcorpus ;
  static QVariantList composites ;
  static int compteur = 0 ;


  //if (not flag && !exist_word(word))
  if (not flag)
   { words << word ;
     alloccs << occs ;
     allcorpus << corpus ;
     composites << composite ;
     compteur++ ;
   }

   if (compteur == 5000 or flag)
   { req.bindValue(":word", words) ;
     req.bindValue(":occs", alloccs) ;
     req.bindValue(":corpus", allcorpus) ;
     req.bindValue(":composite", composites) ;
     req.execBatch() ;

     words.clear() ;
     alloccs.clear() ;
     allcorpus.clear() ;
     composites.clear() ;
     compteur = 0 ;
     return ;
   }
}

//=======================================================================écriture noeud dans la base de données========================

void Words::saveTrie(int corpusid, arbre Trie)
{ QSqlQuery req ;
  req.prepare("INSERT INTO \"WORD_noeud\" (letter, occurrences, sons, bros, fk_corpus) VALUES (:letter, :occs, :sons, :bros, :corpus) ;");
  sqlWriteNodes(Trie->sons, corpusid, req) ;
}

int Words::sqlWriteNodes(arbre node, int corpusid, QSqlQuery req)
{ if (not node) return 0;
  if (not node->sons and not node->bros)
    return sqlWriteNode(node->letter, node->occurrences, 0, 0, corpusid, req) ;
  if (not node->sons and node->bros)
    return sqlWriteNode(node->letter, node->occurrences, 0, sqlWriteNodes(node->bros, corpusid, req), corpusid, req) ;
  if (node->sons and not node->bros)
    return sqlWriteNode(node->letter, node->occurrences, sqlWriteNodes(node->sons, corpusid, req), 0, corpusid, req) ;
  return sqlWriteNode(node->letter, node->occurrences, sqlWriteNodes(node->sons, corpusid, req), sqlWriteNodes(node->bros, corpusid, req), corpusid, req);
}

int Words::sqlWriteNode(QString lettre, val occs, int sons, int bros, int corpus, QSqlQuery req)
{ req.bindValue(":letter", lettre) ;
  req.bindValue(":occs", occs) ;
  req.bindValue(":sons", sons) ;
  req.bindValue(":bros", bros) ;
  req.bindValue(":corpus", corpus) ;
  req.exec() ;
  return req.lastInsertId().toInt();
}

bool Words::exist_word(QString w)
{
    bool exist=false;
    QSqlQuery req ;
    QSqlQuery update;
    QString sql="select id from \"WORD_form\" where form='%1'";
    req.exec(QString(sql).arg(w)) ;
    if (req.next())
    {
           exist=true;
           update.exec(QString("update \"WORD_form\" set occs=occs+1 where id=%1").arg(req.value(0).toString()));
    }
    else
        exist=false;

    return exist;
}
/* à supprimer ?
 * 
void Words::printTrie(int corpus)
{
  QString mot="";
  QString temp="";
  
  
  QSqlQuery req ;
  req.prepare("select * from  WORD_noeud where fk_corpus=:idcorpus order by id desc");
  req.bindValue(":idcorpus", corpus) ;
  req.exec() ;
  
  while (req.next())
  {
    if(req.value(2).toInt()==0)
    {
      mot=mot+req.value(1).toString();
      
      // if(req.value(3).toInt()!=0 && req.value(4).toInt()!=0)
      //     temp=mot;
    }
    else
    {
      mot=mot+req.value(1).toString();
    }
    if(req.value(3).toInt()==0)
    {
      qDebug() <<  mot;
      qDebug() <<  temp;
      mot="";
      
    }
  }
}
*/

