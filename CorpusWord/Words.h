#ifndef NGRAMS_H
#define NGRAMS_H

#include <QtWidgets>
#include "../Common/Types.h"
#include "../Common/BDD.h"
#include "arabicnormalizer.h"



// l'élément de l'arbre préfixe
struct node
{ QChar letter ;
  val occurrences ;
  bool composite ; // le mot contient une ponctuation
  /* autre information utile ici */
  node * sons ;
  node * bros ;
} ;

typedef node * arbre ;


class Words : public QWidget
{  


  BDD                   Bdd ;
  int                   CountWords;


  public:

    Words(int, QElapsedTimer, QString, QVector<QString>) ;
    ~Words();
   




    // Recherche
    bool research(QStringRef, arbre, int = 0) ;
          
    void calculate_mle(arbre, val = 0) ;
    void calculate_proba(arbre, val = 0, short = 0) ;
    void sqlWriteData(int) ;
    void sqlWriteMatrix();

    // Exportation dans un fichier
    void printArbre(arbre, QTextStream &, int = 0) ;

  private:
    

    void addTokens(arbre,QString, QString, QVector<QString> );
    inline void addWord(QString) ;

   arbre create_node(QChar,bool = false, arbre = NULL, arbre = NULL) ;
   void  insert(QStringRef, arbre, int = 0);
   arbre create_branch(QStringRef , arbre ,int );
   arbre assoc(QChar, arbre);
   void sqlWriteBatch(int,arbre);
   void sqlWriteWords(arbre, QString, int, QSqlQuery, bool = false) ;
   void sqlWriteWord(QString, val, int, QSqlQuery, bool=false, bool = false);

   void saveTrie(int,arbre);
   int  sqlWriteNodes(arbre,int,QSqlQuery);
   int sqlWriteNode(QString, val, int, int,int, QSqlQuery);


    void computeAndWriteMatrix(arbre, arbre, int, QSqlQuery) ;
    void identifyAndWriteVoisins(arbre, arbre, int, id, QSqlQuery) ;
    void sqlWriteCell(id, id, val, QSqlQuery, bool = false) ;
     
    // fonctions devenues inutiles (sont dans impression.cpp, non linké)
    void getVoisins(arbre, arbre, int, QTextStream &) ;
    void identifyVoisins(arbre, arbre, int, id, QTextStream &) ;
    void printCell(id, id, val, QTextStream &) ;
        
    void printNode(arbre, QTextStream &, QString = "") ;
    void spacesIndent(int, QTextStream &) ;
    void printString(QString, QTextStream &);
    
    void printLetter(QChar, QTextStream &, bool = false) ;
    bool isPrintableLetter(QChar) ;

    void sqlWriteTrie(arbre) ;
    void sqlWriteBranch(arbre) ;


    int lastPunct(QStringRef) ;
    int firstPunct(QStringRef) ;
    QStringRef removePunctuation(QStringRef) ;
    bool exist_word(QString );
    void printTrie(int) ;
    bool building_AWN_corpus(QString,QVector<QString>);
    
     
} ;

#endif // NGRAMS_H



