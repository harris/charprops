/////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2009-2014 Alan Wright. All rights reserved.
// Distributable under the terms of either the Apache License (Version 2.0)
// or the GNU Lesser General Public License.
/////////////////////////////////////////////////////////////////////////////

#ifndef ARABICNORMALIZER_H
#define ARABICNORMALIZER_H

#include <stdlib.h>
#include <QtSql>
#include <vector>
using namespace std;
class ArabicNormalizer
{
/*public:
    static const wchar_t FATHATAN = (wchar_t)0x064b;
    static const wchar_t DAMMATAN = (wchar_t)0x064c;
    static const wchar_t KASRATAN = (wchar_t)0x064d;
    static const wchar_t FATHA = (wchar_t)0x064e;
    static const wchar_t DAMMA = (wchar_t)0x064f;
    static const wchar_t KASRA = (wchar_t)0x0650;
    static const wchar_t SUKUN = (wchar_t)0x0652;

    static const wchar_t TATWEEL = (wchar_t)0x0640;
    static const wchar_t SHADDA = (wchar_t)0x0651;

    static const wchar_t YEH = (wchar_t)0x064a;
    static const wchar_t DOTLESS_YEH = (wchar_t)0x0649;

    static const wchar_t ALEF = (wchar_t)0x0627;
    static const wchar_t ALEF_MADDA = (wchar_t)0x0622;
    static const wchar_t ALEF_HAMZA_ABOVE = (wchar_t)0x0623;
    static const wchar_t ALEF_HAMZA_BELOW = (wchar_t)0x0625;

    static const wchar_t TEH_MARBUTA = (wchar_t)0x0629;
    static const wchar_t HEH = (wchar_t)0x0647;
    static const wchar_t ARABIC_VOWEL_DIACRITICS[11];
    static const wchar_t ARABIC_CONSONNANT_LETTER[37];*/


public:

    virtual ~ArabicNormalizer();
public:
    /// Normalize an input buffer of Arabic text
    /// @param s input buffer
    /// @param all the bools param if set to true take this normalization into consideration
    /// @return input buffer after normalization
    static QString normalize(QString s, QString remove_diactritics, QString remove_shadda, QString normalize_alef, QString normalize_yeh, QString normalize_heh);
    ///check if the word passed is arabic
    /// @param s the word to check
    /// @return s if the word is arabic, otherwise return null
    static QString isArabicWord(QString s);
    ///clean the word passed from the digits
    /// @param s the word to clean
    /// @return the word cleaned of digit
    static QString RemoveDigitWord(QString s);
    /// Normalize an input buffer of Arabic text
    /// @param s input buffer
    /// @param all the bools param if set to true take this normalization into consideration
    /// @return input buffer after normalization
    static QString normalize(QString s, QString remove_non_arabic, QString remove_digits, QString remove_diactritics, QString remove_shadda, QString normalize_alef, QString normalize_yeh, QString normalize_heh);
};

class KhojaStemmer
{
    //before stemming a word
    // format the word by removing any punctuation, diacritics and non-letter charracters
public:
    //static vector<vector<string> > staticFiles;
    static vector<vector<wstring> > staticFiles;
    static bool rootFound;
    static bool patternFound;
    static bool stopwordFound;
    static bool strangeWordFound;
    static bool rootNotFound;
    static bool fromSuffixes;
public:
    static bool addVectorFromFile ( QString path, QString fileName );// read in the contents of a file, put it into a vector, and add that vector to the vector composed of vectors containing the static files
    static void readInStaticFiles ( QString pathToStemmerFiles); // read in the static files
    static wstring stemWord(wstring word );//stem the word
    static wstring checkForPrefixes(wstring word );// check and remove any prefixes
    static wstring checkForSuffixes(wstring word );    // check for the suffixes
    static wstring checkPrefixWaw(wstring word );    // check and remove the special prefix (waw)
    static wstring checkDefiniteArticle(wstring word );  // check and remove the definite article
    static wstring isTwoLetters(wstring word );    // if the word consists of two letters
    static wstring isThreeLetters(wstring word );// if the word consists of three letters
    static void isFourLetters (wstring word );// if the word has four letters
    static wstring checkPatterns(wstring word );  // check if the word matches any of the patterns
    static wstring duplicate(wstring word );    // handle duplicate letters in the word
    static wstring lastWeak(wstring word );// check if the last letter of the word is a weak letter
    static wstring firstWeak(wstring word );// check if the first letter is a weak letter
    static wstring middleWeak(wstring word );// check if the middle letter of the root is weak
    static bool checkStopwords (wstring currentWord );// check that the word is a stopword
    static bool checkStrangeWords (wstring currentWord ); // check that the word is a strange word
};


#endif // ARABICNORMALIZER_H
