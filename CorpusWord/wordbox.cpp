#include  "window.h"

void Window::wordBoxes(QGridLayout * mainlayout)
{
    QGroupBox * wordbigbox = new QGroupBox(tr("Word analysis")) ;
    mainlayout->addWidget(wordbigbox, 0, 2) ;
    QVBoxLayout * wordbiglayout = new QVBoxLayout ;
    wordbigbox->setAlignment(Qt::AlignTop) ;
    wordbigbox->setLayout(wordbiglayout) ;

    QGroupBox * wordbox = new QGroupBox(tr("")) ;
    wordbiglayout->addWidget(wordbox, 0, 0) ;
    QGridLayout * wordslayout = new QGridLayout ;
    wordbox->setAlignment(Qt::AlignTop) ;
    wordbox->setLayout(wordslayout) ;

    TreeConstitutionButton11 = new QPushButton("Insert Words") ;
    TreeConstitutionButton11->setEnabled(false) ;
    wordslayout->addWidget(TreeConstitutionButton11, 0, 0, 1, -1) ;
    connect(TreeConstitutionButton11, SIGNAL(clicked()), this, SLOT(showWords()));

    createWordsTable() ;
    wordbiglayout->addWidget(WordsTable, 3) ;
}


void Window::createWordsTable()
{ WordsTable = new QTableWidget(0, 2) ;
  WordsTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
  WordsTable->setHorizontalHeaderLabels(QStringList() << tr("Words") << tr("Frequency")) ;
  WordsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
  WordsTable->verticalHeader()->hide() ;
  WordsTable->setShowGrid(false) ;
  WordsTable->setContextMenuPolicy(Qt::CustomContextMenu);
}

void Window::getWords(int corpus)
{
    QSqlQuery req ;
    req.exec(QString("select * from \"WORD_form\" where  fk_corpus=%1").arg(corpus)) ;
    //req.exec(QString("select * from \"WORD_form\")) ;

    while (req.next())
    {
            QTableWidgetItem * Word = new QTableWidgetItem(req.value(0).toString()) ;
            QTableWidgetItem * frequency = new QTableWidgetItem((req.value(1)).toString()) ;
            int row = MarquesTable->rowCount();
            WordsTable->insertRow(row);
            WordsTable->setItem(row, 0, Word);
            WordsTable->setItem(row, 1, frequency);
            FormsNumber++;
    }
    FormsNb->setText(QString::number(FormsNumber));
}

void Window::showWords()
{
  checkPreProcessing();
  word = new Words(IdCorpus, Time,language,language_param);
  //getWords(IdCorpus);
  delete word;
  FormsAnalyseButton->setEnabled(true) ;
  MsgBox("Fill in the Marker Shreshold and Click on the Analyse Corpus Forms");
}
