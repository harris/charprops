#include "SegmentsDB.h"
#include <vector>

// Constructeur de l'arbre des Words

SegmentsDB::SegmentsDB()
{
  Trie =create_node(-1,-1,NULL,NULL);
}

SegmentsDB::~SegmentsDB()
{}

arbre_segments SegmentsDB::create_node (id marque, id sortie, arbre_segments son, arbre_segments bro)
{  arbre_segments p = new node_segment ;

  p->m_id=marque;
  p->sons = son ;
  p->bros = bro ;
  p->occurrences=1;

  if (sortie)
  {
    p->sortie = 1 ;
    CountSegments++;
  }
  else
    p->sortie  = 0;

  return p ;
}

void SegmentsDB::insert(QVector<int> identifiant, arbre_segments tree, int position)
{
    arbre_segments branch ;
    QVector<int> tmp;
    if (position >= identifiant.size()) return ;

    branch = assoc(identifiant[position], tree->sons) ;

    if (branch && (position == identifiant.size() - 1))
    {
      branch->occurrences++ ;
      return ;
    }
    if (branch )
    {
      branch->occurrences++ ;
      insert(identifiant, branch, position + 1) ;

      return ;
    }
    branch = create_branch(identifiant, tree, position) ;

    branch->bros = tree->sons ;
    tree->sons = branch ;

    return ;
}

arbre_segments SegmentsDB::create_branch(QVector<int> identifiant, arbre_segments tree, int position)
{  int sortie=0;

   if (position > identifiant.size()-1) return NULL ;

   if (position == identifiant.size() - 1)
   {
         sortie=1;
         return create_node(identifiant[position], sortie);
   }

   return create_node(identifiant[position],sortie, create_branch(identifiant, tree, position + 1)) ;
}

arbre_segments SegmentsDB::assoc(id m, arbre_segments tree)
{

   if (not tree) return NULL ;

  if (tree->m_id == m)
  {
      tree->occurrences++;
      return tree ;
  }
  return assoc(m, tree->bros) ;
}

arbre_segments SegmentsDB::assoc_search(id m, int element, arbre_segments tree, std::vector<id> id_Marques)
{
  if (not tree) return NULL ;

  if (tree->m_id == m)
       return tree;

  if(m==0)
  {
      if(tree->m_id == element && !search_in_vector(id_Marques,element))
        return tree;
  }

  return assoc_search(m, element, tree->bros,id_Marques) ;
}

bool SegmentsDB::search(QVector<int> pattern, int element, arbre_segments tree, std::vector<id> id_Marques,int position)
{
  if (not tree) return false;

  arbre_segments temp_tree=tree;
  arbre_segments branch ;

  branch = assoc_search(pattern[position], element,temp_tree->sons,id_Marques) ;

  if (branch && (position == pattern.size() - 1))
    return true;

  if (branch)
    return search(pattern, element, branch,id_Marques, position + 1) ;

  return false;
}

bool SegmentsDB::search_in_vector(std::vector<id> vec, int id)
{
    int iPremier=0;
    int iDernier=vec.size();
    bool iTrouve=false;
    int iMilieu;

    while((iPremier <= iDernier)&&(iTrouve==false))
    {
        iMilieu=(iPremier+iDernier)/2;

        if(vec[iMilieu]==id) {iTrouve =true;break;}
        else
        {
                if(vec[iMilieu]>id) iDernier = iMilieu -1;
            else
                iPremier = iMilieu +1;
         }
    }
    return iTrouve;
}




