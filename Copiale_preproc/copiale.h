#ifndef COPIALE_H
#define COPIALE_H

#include <QtWidgets>

#define WORD2CODE false
#define CODE2WORD true

// Structure pour conversion

struct Conversion
{ QString word ;
  QChar letter ;
  struct Conversion * next ;
} ;

typedef struct Conversion * table ;


class Table
{ public:
  Table() ;
  ~Table() {}
 
  table Value ;
  ushort Count ;
  ushort CharIndex ;
  long int Length ;

  void generateCodes() ;
  void printCodes(table, QTextStream &) ;
  void printPages(QDirIterator &, QString, bool = false) ;
  void printTable(table, QTextStream &) ;
  
private:

  void getAllWords(QDirIterator &) ;
  void getWords(QFile &) ;
  void insertWordList(QStringList) ;
  void insertWord(QString) ;
  bool memberWord(QString, table) ;
  void pushWord(QString) ;
  void generateCode(table) ;
  void printPage(QFile &, QFile &, bool) ;
  void translatePage(QFile &, QFile &) ;
  void translateWords(QStringList, QTextStream &) ;
  void tranlateWord(QString, QTextStream &) ;
  QChar findWord(QString, table) ;
  void reversePage(QFile &, QFile &) ;
  void translateChars(QString, QTextStream &) ;
  QString findChar(QChar c, table T) ;  
  void printReport(QTextStream &) ;
  
} ;






#endif // COPIALE_H
