#include "copiale.h"

Table::Table()
  : Value(NULL), Count(0), CharIndex(0), Length(0)
{ // création de la table
  QDirIterator dir("../Data/", QStringList() << "Page*.in", QDir::Files | QDir::NoSymLinks) ;
  getAllWords(dir) ;
  generateCodes() ;
  
  // traduction
  QDirIterator dir2("../Data/", QStringList() << "Page*.in", QDir::Files | QDir::NoSymLinks) ;
  printPages(dir2, QString(".out")) ;

  // reporting
  QFile Out("report.out") ;
  if (not Out.open(QIODevice::WriteOnly | QIODevice::Text)) return ;
  QTextStream Flux(&Out) ;
  printReport(Flux) ;
  Out.close() ;
  Out.setFileName("table.cnv") ;
  if (not Out.open(QIODevice::WriteOnly | QIODevice::Text)) return ;
  printTable(Value, Flux) ;
  Out.close() ;
  
  // vérification effectuée, code devenu inutile
/*
  QDirIterator dir3("../Data/", QStringList() << "Page*.out", QDir::Files | QDir::NoSymLinks) ;
  printPages(dir3, QString(".rev"), true) ;
*/  
}

// ============================================================ CRÉATION DE LA TABLE DE TRADUCTION =============================================

// Récupérer les mots de tous les fichiers dans une liste
// pour les insérer dans une table
void Table::getAllWords(QDirIterator & page)
{ while (page.hasNext())
  { QFile file(page.next()) ;
    if (not file.open(QIODevice::ReadOnly | QIODevice::Text))
    { qDebug() << "Could not open the page" ;
      return ; }
    getWords(file) ;
    file.close() ; }
}

// Récupérer les mots d'un fichier
// Les insérer dans la table ligne par ligne
void Table::getWords(QFile & page)
{ QTextStream flux(& page) ;
  while (not flux.atEnd()) insertWordList((flux.readLine()).split(" ")) ;
}

// Récupérer les mots d'une ligne
void Table::insertWordList(QStringList list)
{ QStringListIterator word(list) ;
  while (word.hasNext())
  { if (not (word.next()).isEmpty()) insertWord(word.peekPrevious()) ;
    Length++ ; }
}

// Insérer un mot dans la table
// S'il n'y figure pas déjà
void Table::insertWord(QString word)
{ if (not memberWord(word, Value)) pushWord(word) ;
}

// Vérifier qu'un mot est dans la table
bool Table::memberWord(QString word, table T)
{ if (not T) return false ;
  if (word.compare(T->word) == 0) return true ;
  return memberWord(word, T->next) ;
}

// Créer un mot
void Table::pushWord(QString word)
{ table p = new Conversion ;
  p->word = word ;
  p->next = Value ;
  Value = p ;
  Count++ ;
}

// Générer des QChars pour chaque mot
// Le test devrait prendre en compte le nombre de caractères imprimables
void Table::generateCodes()
{ if (CharIndex > (ushort) 65535) 
  { qDebug() << " Too much Symbols : " << CharIndex ;
    return ; }
  generateCode(Value) ;
}

// Générer un QChar pour un mot ; cette méthode n'est pas très au point
void Table::generateCode(table T)
{ if (not T) return ;
  QChar c(++CharIndex);
  while (not c.isLetterOrNumber()) c = (QChar) CharIndex++  ;
  T->letter = c ;
  generateCode(T->next) ;
}

// =================================================================== CONVERSION DU CORPUS ===========================================================

// La traduction s'effectue en imprimant dans des fichiers
// Entrée Page*.in Sortie Page*.out
// La détection des namefilters ne permet pas de traiter "Page*."
// La traduction inverse s'effectue en imprimant dans des fichiers
// Entrée Page*.out Sortie Page*.rin

void Table::printPages(QDirIterator & page, QString extension, bool reverse)
{ while (page.hasNext())
  { QFile infile(page.next()) ;
    if (not infile.open(QIODevice::ReadOnly | QIODevice::Text))
    { qDebug() << "Could not open the page" ;
      return ; }
    QFile outfile((page.fileInfo()).baseName().append(extension).prepend(page.path())) ;
    if (not outfile.open(QIODevice::WriteOnly | QIODevice::Text)) return ;
    printPage(infile, outfile, reverse) ;
    infile.close() ;
    outfile.close() ;
  }
}

void Table::printPage(QFile & infile, QFile & outfile, bool reverse)
{ if (reverse) reversePage(infile, outfile) ;
  else translatePage(infile, outfile) ;
}


// =================================================================== TRADUIRE LE CORPUS EN UNICODE ====================================================

// imprimer un fichier
void Table::translatePage(QFile & infile, QFile & outfile)
{ QTextStream in(& infile) ;
  QTextStream out(& outfile) ;
  out.setCodec("UTF-8") ;
  while (not in.atEnd()) translateWords((in.readLine()).split(" "), out) ;
}  
  
  // imprimer une ligne de caractères
void Table::translateWords(QStringList list, QTextStream & out)
{ QStringListIterator word(list) ;
  while (word.hasNext())
    if (not (word.next()).isEmpty()) out << findWord(word.peekPrevious(), Value) ;
  out << endl ;
}

// ramener le caractère d'un mot ou 0
QChar Table::findWord(QString word, table T)
{ if (not T) return (QChar) 0 ;
  if (word.compare(T->word) == 0) return T->letter ;
  return findWord(word, T->next) ;
}

// ========================================================================== REVERSE VERIFICATION ==========================================================

/* ici, il s'agit de partir du résultat page*.out pour reconstituer le fichier d'origine.
 * puis d'utiliser un diff pour voir s'il y a une erreur d'encodage.
 * 
 */

// imprimer un fichier
void Table::reversePage(QFile & infile, QFile & outfile)
{ QTextStream in(& infile) ;
  QTextStream out(& outfile) ;
  out.setCodec("UTF-8") ;
  while (not in.atEnd()) translateChars((in.readLine()), out) ;
}

// imprimer une ligne de mots
void Table::translateChars(QString string, QTextStream & out)
{ int position = 0 ;
  while (position < string.size() - 1)
    out << findChar(string.at(position++), Value) << " " ;
  out << findChar(string.at(position), Value) << endl ;
}

// ramener le mot d'un caractère ou ""
QString Table::findChar(QChar c, table T)
{ if (not T) return QString("") ;
  if (T->letter == c) return T->word ;
  return findChar(c, T->next) ;
}

// =================================================================================== REPORTING ==============================================================
// imprimer tous les éléments importants
void Table::printReport(QTextStream & flux)
{ flux << QString("Longueur du corpus en caractères (sans les RC) : ") << Length << endl ;
  flux << QString("Nombre de caractères différents  : ") << Count << endl ;
  flux << QString("Index du dernier caractère : ") << CharIndex << endl ;
  flux << QString("Caractères utilisés : ") ;
  printCodes(Value, flux) ;
  flux << endl ;
}

// imprime la liste des QChars utilisés
void Table::printCodes(table T, QTextStream & flux)
{ if (not T) return ;
  flux << T->letter ;
  printCodes(T->next, flux) ;
}

// imprime la table des codages
void Table::printTable(table T, QTextStream & flux)
{ if (not T) return ;
  flux << T->letter << " " << T->word << endl ;
  printTable(T->next, flux) ;
}

