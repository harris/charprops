#ifndef SUKHOTIN_H
#define SUKHOTIN_H

#include <QtWidgets>

#include "../Common/Types.h"
#include "../Common/BDD.h"

// la valeur de la table : nom, occs
typedef QPair<QString, val> un_ngramme ;
// la table : identifiant / nom, occs
typedef QMap<id, un_ngramme> table_ngramme ;

// int = le rang
typedef QPair<QString, int> une_voyelle ;
// pour les voyelles
typedef QMap<id, une_voyelle> table_voyelle ;

// pour trier
typedef QPair<id, un_ngramme> consonneid ;
typedef QPair<id, une_voyelle> voyelleid ;

// pour colonnes et diagonale
typedef QMap<id, val> line ;


class Sukhotin : public QWidget
{ Q_OBJECT
  
public:
  Sukhotin();
  ~Sukhotin() {}

private slots:
  void classer();
  void on_vm_textChanged();
  
private:
  BDD Bdd ;
  table_voyelle Voyelles ;
  table_ngramme Table ;
  table_ngramme Consonnes ;
  QTextStream * Stdout ;
  
  QPushButton * classifierBouton;
  QPushButton * quitterBouton;
  
  QLineEdit * NomBDD ;
  
  //QGroupBox *groupChargement;
  QGroupBox * ParamsBox ;
  QSpinBox * Max_Ngram ;
  QSpinBox * Ngram_fixe ;
  QSpinBox * Thresholded ;
  
  void sukhotin() ;
  bool putVowel() ;
  void newVowel(id, int) ;
  
  // ramènent des cellules
  line sqlDiagCells() ;
  line sqlColCells(id) ;
  QString reqNgramme() ;
  QString reqMatriceDiag() ;
  QString reqMatriceCol(id) ;
  
  // ramène les ngrammes et occurrences
  void sqlIdsTable() ;
  void suppDiagonale() ;
  
  void printResults(QTextStream &) ;
  static  bool sortByRank(voyelleid, voyelleid) ;
  static  bool sortByValue(consonneid, consonneid) ;
  QString translate(id) ;
} ;

#endif // SUKHOTIN_H
