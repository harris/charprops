#include "Sukhotin.h"

/*
 * On part de la matrice des occurrences ; on met la diagonale à zéro, et on additionne les valeurs pour chaque ligne
 * Chaque caractère / ngraphe est supposé être une consonne
 * L'élement qui a la somme la plus grande et > 0 est une voyelle
 * On remplace la colonne de la voyelle (sauf la diagonale) : val => -val pour chaque ligne consonne
 * On recommence les calculs jusqu'à ce qu'il n'y ait plus de sommes > 0
 * 
 * Pour tous les ngrammes :
 *  Pour le ngramme I, additionner toutes les cellules de ligne I sauf celle de colonne J
 *  stocker la somme
 *  conserver le ngramme à la somme la plus élevée si elle est supérieure à 0
 * 
 * Stocker l'identifiant K1 de ce ngramme.
 * 
 * Pour tous les ngrammes sauf K :
 *  Pour le ngramme I, additionner toutes les cellules de ligne I sauf celle de colonne J et inverser la valeur de la colonne K
 *  stocker la somme
 *  conserver le ngramme à la somme la plus élevée si elle est supérieure à 0
 * 
 * Stocker l'identifiant K2 de ce ngramme
 * 
 * Etc.
 * 
 * Généralisation :
 * 
 * 1. Initialiser la liste Voyelles (liste vide)
 * Initialiser la liste Consonnes = tout
 * Initialiser les valeurs au nombre global d'occurrences
 * Soustraire la diagonale
 * 
 * 2. Boucler tant qu'on trouve des voyelles
 * 
 *  1/ Prendre le max > 0
 *  2/ Mettre à jour Voyelles et Consonnes
 *  2/ Soustraire la colonne de la voyelle trouvée * -2
 * 
 * 3. Retourner Voyelles dans l'ordre ; les autres ngrammes sont des consonnes
 * 
 * 
 * Prévoir la possibilité de limiter aux unigrammes (par sélection de l'utilisateur) :
 * La diagonale n'a pas de sens : (ch, ch), mais (ch, c) à exclure aussi 
 * ou alors soustraire la diagonale à tous les ngrammes plus petits :
 * 
 * (cha, c) - (cha, cha)
 * (cha, ch) - (cha, cha)
 * 
 * Une telle fonction pourrait être utile dans d'autres cas, pour obtenir une fréquence sur 100%
 * sans répétition
 * 
 * Nombre d'occurrences sans duplication : soustraire systématiquement le nombre d'occurrence des ngrammes suivants au ngramme précédent.
 * 
 */

/*
 * Il faudrait un algorithmes.h avec la classe Sukhotin, et d'autres pour chaque algorithme
 * 
 */


Sukhotin::Sukhotin()
  : QWidget()
{ setFixedSize(500, 300);
  QVBoxLayout * layoutPrincipal = new QVBoxLayout;
  setLayout(layoutPrincipal);
  
  QFormLayout * chargerLayout = new QFormLayout ;
  layoutPrincipal->addLayout(chargerLayout);
  NomBDD = new QLineEdit("sukhotin") ;  
  chargerLayout->addRow("BDD --> input data for Sukhotin", NomBDD);
  
  ParamsBox = new QGroupBox("Sukhotin's parameters");
  layoutPrincipal->addWidget(ParamsBox);
  ParamsBox->setEnabled(true);
  
  QFormLayout * paramslayout = new QFormLayout;
  ParamsBox->setLayout(paramslayout);
  
  Max_Ngram = new QSpinBox;
  paramslayout->addRow(tr("Maximal size of ngrams"), Max_Ngram) ;
  Max_Ngram->setRange(1,5);
  Max_Ngram->setValue(2);
  Max_Ngram->setMaximumWidth(50);
  
  Ngram_fixe = new QSpinBox;
  paramslayout->addRow(tr("Same size for all ngrams? (1/0)"), Ngram_fixe) ;
  Ngram_fixe->setRange(0,1);
  Ngram_fixe->setValue(0);
  Ngram_fixe->setMaximumWidth(50);
  
  Thresholded = new QSpinBox;
  paramslayout->addRow(tr("Thresholded ngrams? (1/0)"), Thresholded);
  Thresholded->setRange(0,1);
  Thresholded->setValue(1);
  Thresholded->setMaximumWidth(50);

  QHBoxLayout * layoutBouton = new QHBoxLayout;
  layoutPrincipal->addLayout(layoutBouton);
  layoutBouton->addStretch();
  
  quitterBouton = new QPushButton(tr("Quit")) ;
  layoutBouton->addWidget(quitterBouton);
  connect(quitterBouton, SIGNAL(clicked()), this, SLOT(close()));
  
  classifierBouton = new QPushButton(tr("Sort vowels")) ;  
  layoutBouton->addWidget(classifierBouton);
  connect(classifierBouton,SIGNAL(clicked()),this, SLOT(classer()));
}

//====================================================================== SLOTS ==================

// bouton Sort vowels

void Sukhotin::classer()
{ 
  Bdd.setConnexion("localhost", "Charprops", "charprops", NomBDD->text()) ;
  if (not Bdd.getConnexion()) qDebug() << " Pas de connexion à la base de données?!" ;
  
  Stdout = new QTextStream(stdout, QIODevice::WriteOnly) ;
  
  sqlIdsTable() ;
  suppDiagonale() ;
  Consonnes = Table ;
  
  QElapsedTimer time ;
  time.start() ;
  qDebug() << "Chronomètre lancé : " << time.elapsed() / (qint64) 60000 << " minutes" ;
  
  while (putVowel()) ;
  
  QFile file("Sukhotin.out") ;
  file.open(QIODevice::WriteOnly | QIODevice::Text) ;
  QTextStream flux(& file) ;
  printResults(flux) ;
  file.close() ;
  QMessageBox::information(this, "Classification terminée", "consultez le fichier \"Sukhotin.out\"!");
}


// construit une qmap des doublets name occs
void Sukhotin::sqlIdsTable()
{ QSqlQuery req ;
  req.exec(reqNgramme()) ;
//  qDebug() << "reqNgramme " << reqNgramme() ;
  while (req.next())
  { Table.insert
    ( (id) req.value(0).toInt(),
      qMakePair(req.value(1).toString(), req.value(2).toInt()) ) ;
    qDebug() << "key : " << req.value(0).toInt() << " string : " << req.value(1).toString() << " occs : " << req.value(2).toInt() ;
  }
  qDebug() << endl ;
}

QString Sukhotin::reqNgramme()
{ if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 0))
  return QString("select id_ngram, ngram, frequence from ngramme") ;
  if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 1))
    return QString("select id_ngram, ngram, frequence from ngramme where size_ngram = %1").arg(Max_Ngram->value()) ;
  if (Ngram_fixe->value() == 0)
    return QString("select id_ngram, ngram, frequence from ngramme where stoplist = false and size_ngram <= %1").arg(Max_Ngram->value()) ;
  return QString("select id_ngram, ngram, frequence from ngramme where stoplist = false and size_ngram = %1").arg(Max_Ngram->value()) ; 
}

// met à zéro la diagonale de la table
void Sukhotin::suppDiagonale()
{ if (Table.isEmpty()) return ;
  line diag = sqlDiagCells() ;
  foreach (id i, Table.keys())
    if (Table.contains(i) and diag.contains(i))
    { Table[i].second = Table[i].second - diag[i] ;
      (* Stdout) << "." ; } 
  (* Stdout) << endl ; 
}

// ramène une diagonale de cellules : toutes les val où i = j
line Sukhotin::sqlDiagCells()
{ QSqlQuery req ;
  line diag ;
  req.exec(reqMatriceDiag()) ;
  qDebug() << "reqDiag " << reqMatriceDiag() ;
  while (req.next())
  { diag.insert((id) req.value(0).toInt(), (val) req.value(1).toInt()) ;
    (* Stdout) << "." ; }
  (* Stdout) << endl ;   
  return diag ;
}


QString Sukhotin::reqMatriceDiag()
{ if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 0))
    return QString("select fk_i, val from matrice where fk_i = fk_j") ;
  if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 1))
    return QString("select fk_i, val from matrice join ngramme as n on n.id_ngram = fk_i where fk_i = fk_j and n.size_ngram = %1").arg(Max_Ngram->value()) ;
  if (Ngram_fixe->value() == 0) 
    return QString("select fk_i, val from matrice where fk_i = fk_j and stoplist = false") ;
  return QString("select fk_i, val from matrice join ngramme as n on n.id_ngram = fk_i where fk_i = fk_j and n.stoplist = false and n.size_ngram = %1").arg(Max_Ngram->value()) ; 
}



void Sukhotin::on_vm_textChanged()
{
   ParamsBox->setEnabled(true);
}


//======================================================= algorithme

// On parcourt la table des consonnes
// On cherche le max
bool Sukhotin::putVowel()
{ static int max ;
  static int rang = 0 ;
  max = 0 ;
  id better ;
  bool flag = false ;
  foreach (id i, Consonnes.keys())
  { if (Consonnes[i].second > max)
    { max = Consonnes[i].second ;
      better = i ;
      flag = true ; } }
  if (not flag) return false ;
//  qDebug() << "max " << max << " gagnant : " << Consonnes[better].first ;
  newVowel(better, rang) ;
  rang++ ;
  return true ;
}


// Mise à jour après l'identification d'une voyelle

void Sukhotin::newVowel(id voyelle, int rang)
{ line col = sqlColCells(voyelle) ;
  Voyelles.insert(voyelle, qMakePair(Consonnes[voyelle].second, rang)) ;
  Consonnes.remove(voyelle) ;
  foreach (id i, Consonnes.keys())
  { if (col[i] != 0) Consonnes[i].second -= 2 * (int) col[i] ;
    
  }  
}

// ramène une colonne de cellules : toutes les vals suivant j
line Sukhotin::sqlColCells(id i)
{ QSqlQuery req ;
  line col ;
  req.exec(reqMatriceCol(i)) ;
  qDebug() << "reqMatriceCol " << reqMatriceCol(i) ;
  while (req.next())
  {  col.insert((id) req.value(0).toInt(), (val) req.value(1).toInt()) ;
    (* Stdout) << "." ; } 
  (* Stdout) << endl ; 
  return col ;
}

QString Sukhotin::reqMatriceCol(id i)
{ if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 0))
  return QString("select fk_j, val from matrice where fk_i = %1").arg(i) ;
  if ((Thresholded->value() == 0) and (Ngram_fixe->value() == 1))
    return QString("select fk_j, val from matrice join ngramme as n on n.id_ngram = fk_j where fk_i = %1 and n.size_ngram = %2").arg(i).arg(Max_Ngram->value()) ;
  if (Ngram_fixe->value() == 0)
    return QString("select fk_j, val from matrice where fk_i = %1 and stoplist = false").arg(i) ;
  return QString("select fk_j, val from matrice join ngramme as n on n.id_ngram = fk_j where fk_i = %1 and n.stoplist = false and n.size_ngram = %2").arg(i).arg(Max_Ngram->value()) ; 
}



// ==================================================================== impression


bool Sukhotin::sortByRank(voyelleid v1, voyelleid v2)
{ return v1.second.second < v2.second.second ;
}

bool Sukhotin::sortByValue(consonneid c1, consonneid c2)
{ return c1.second.second > c2.second.second ;
}

void Sukhotin::printResults(QTextStream & flux)
{ flux << QString("Paramètres") << endl << "  BDD : " << NomBDD->text() << endl ;
  flux << "  Taille maximale ngramme : " << Max_Ngram->value() << endl ;
  flux << "  Taille fixe ? " << (Ngram_fixe->value() == 0 ? "non" : "oui") << endl ;
  flux << "  Seuil pris en compte ? " << (Thresholded->value() == 0 ? "non" : "oui") << endl ;
  
  QList<QPair<id, une_voyelle> > vsorted, csorted ;
  QMapIterator<id, une_voyelle> v(Voyelles) ;
  QMapIterator<id, un_ngramme> c(Consonnes) ;
  while (v.hasNext())
  { v.next() ;
    vsorted << qMakePair(v.key(), v.value()) ; }
  std::sort(vsorted.begin(), vsorted.end(), sortByRank) ;
  while (c.hasNext())
  { c.next() ;
    csorted << qMakePair(c.key(), c.value()) ; }
  std::sort(csorted.begin(), csorted.end(), sortByValue) ;
  flux << "=============================================== Voyelles ===================" << endl ;
  foreach (voyelleid v, vsorted) flux << "\"" << translate(v.first) << "\"" << endl ;
                       flux << endl ;
  flux << "=============================================== Consonnes ==================" << endl ;
  foreach (consonneid c, csorted) flux << "\"" << translate(c.first) << "\" value : " << c.second.second << endl ;
}

QString Sukhotin::translate(id identifiant)
{ return Table[identifiant].first ;
}


