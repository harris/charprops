#ifndef VECTEUR_CREUX_H
#define VECTEUR_CREUX_H

#define QT5
#include <iostream>
#include <QtWidgets>

struct Cellule
     { int index ;
       float val ;
     } ;


typedef struct Cellule Cellule;
typedef QVector <Cellule> SVector;
typedef float(*DISTANCE)(SVector &, SVector &);


void ajouter(SVector & vect, int indx, float valeur) ; 
void inserer(SVector & vect, int indx, float valeur) ;


void afficher(SVector & vect);
void reelXvect(SVector & vect, float k) ;
void somme(SVector & vect1 , SVector & vect2); // vect1 += vect2 

float distanceEuclidienneSparceVector(SVector  & W, SVector  & X) ;
float produitScalaire(SVector & X, SVector & W) ;
float cosTita(SVector & X, SVector & W) ;
float distanceCosinus(SVector & X, SVector & W) ;

float norme(SVector & X) ;
void normalise(SVector & X) ;


// autres méthodes 

	int max(int,int);
	int min(int, int);
       
        double reel_aleatoire_a_b(double a, double b);   // réel aleatoire entre a et b
        int entier_aleatoire_a_b(int a,int b);		// entier aleatoire entre a et b

	void melanger(QVector <int>  & vect) ;

#endif

