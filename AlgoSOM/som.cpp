#include "som.h"
using namespace std;

// le constructeur installe la table
// initialise la mémoire des vecteurs et les paramètres du som
Som::Som(int nlig, int ncol, int nb_th, int nb_th_ap, float alphai, float ksigmai, float sigmaf)
  : Lig(nlig), Col(ncol), Nb_thread(nb_th), Nb_thread_ap(nb_th_ap), Alphai(alphai), Ksigmai(ksigmai), Sigmaf(sigmaf), QCI(0), QCE(0)
{ SOM = NULL;
  if ((Lig > 0) && (Col > 0))
  { // Creation du SOM
    SOM = new NeuroneSparce * [Lig];
    for (int i=0; i < Lig; i++) SOM[i] = new NeuroneSparce[Col];
  }
  else
   qDebug() << "Erreur: les parametres sont invalides!";
  // qDebug() << "Initialisation matrice : " << time.elapsed() / (qint64) 1000 << " secondes" ;
}

Som::~Som()
{ SOM_clean();
  //DataMatrix.clear(); 
}

//Initialisation de vecteurs mémoires à partir du DataMatrix
void Som::initialiseSOM(int taux)
{ if ((SOM != NULL) && (DataMatrixSparce.size() != 0))
  { qDebug() << "Initialisation des vecteurs mémoires en cours..." ;
    for (int i(0); i < Lig; i++)
     for (int j(0); j < Col; j++)
       initialiseSparceVM(SOM[i][j].w, taux);
  }
}

// On va initialiser chaque Vecteur Mémoire 
void Som::initialiseSparceVM(SVector & Mem , int taux)
{  int k;
   int i;
   int N = Nb_vect/taux ;
    for (k = 0; k < N; k++)
  	{ i = entier_aleatoire_a_b(0,N);
          somme(Mem , DataMatrixSparce[i]) ;
      	}
	
       reelXvect(Mem, 1.0/N) ; // Mem *= 1/N 
    
}

void Som::initialiseSOM_VU()
{ if ((SOM != NULL) && (DataMatrixSparce.size() != 0))
  { qDebug() << "Initialisation des vecteurs mémoires en cours..." ;
    SVector vect ;
    for (int i = 0; i < Nb_vect; i++)
      somme(vect , DataMatrixSparce[i]) ;

    reelXvect(vect, 1.0/Nb_vect) ;

    for (int i(0); i < Lig; i++)
     for (int j(0); j < Col; j++)
       somme(SOM[i][j].w, vect);
  }
}

/**************************************** version Euclidiènne ***********************************/


bool Som::onMap(int i, int j)
{  if (i<0 || i >= Lig || j<0 || j>=Col)   return 0;
   else return 1;
}


/**********************************************************************/

/*         Apprentissage                     */
/********************************************/

bool Som:: apprentissage(int Tmax, QString topo, DISTANCE distance, QElapsedTimer time)
{ if ((SOM != NULL) && (DataMatrixSparce.size() != 0))
  { int t, indx;
    int rayon0 = min(Col,Lig)/2 ;
    int rayon ;
    int xStar, yStar;
    float dStar ;
        
    // choix de la topologie de SOM
     typedef void(Som::*MAJVM)(int, int, int, id, int, int);
     MAJVM majVectMem;
     
     // majVect6MultiThread or majVect8MultiThreadSparce
     if (topo == "hexagonal") { majVectMem = &Som::majVect6MultiThreadSparce ; } 
     else  { majVectMem = &Som::majVect8MultiThreadSparce ; }
          
    Sigmai = Ksigmai * rayon0 ;

    etatChanged("Apprentissage en cours......."); 	// envoie de message à l'interface
    qDebug() <<"Apprentissage en cours.......";
    t = 0;
    while (t < Tmax)
    { rayon = rayon0 - (rayon0+1) * t / Tmax ;
      // choisir aleatoire un vecteur
      indx = entier_aleatoire_a_b(0,Nb_vect);
      // determiner le neurone gagnant
       getBmuMultiThread(indx, xStar, yStar, dStar, distance);
       
      // mettre à jour vecteurs mémoires de voisinage
       (this->*majVectMem)(xStar, yStar, rayon, indx, t, Tmax);
           
      t++;
      fprintf(stderr, "\033[0GProcessed %d / %d itérations ", t ,Tmax);
     
     // afficher dans l'interface graphique l'évolution de processus
     //etatSOM =  "Apprentissage en cours..." + QString::number(t) + "/"+  QString::number(max);
     //etatChanged(etatSOM); 	// envoie de message à l'interface
     
    }
    //outVM("vm10x10GDB.txt");
    qDebug() << "\nApprentissage est terminé avec succes";
    qDebug() << "Apprentissage : " << time.elapsed() / (qint64) 60000 << " minutes" ;
    return true;
  }
  else
  { qDebug() << "ERREUR: Impossible de faire l'Apprentissage.";
    return false; }
}



void Som::SOM_clean()
{ int i,j ;
  if (SOM != NULL)
  { for (i = 0; i < Lig; i++)
     for(j = 0; j < Col; j++) SOM[i][j].w.clear();
     for(i = 0; i < Lig; i++) delete [] SOM[i] ;
     delete [] SOM;
     SOM = NULL   ;
     qDebug() << "SOM suprime" ;
  }
}

/*****************************************************************
         Multithreading
********************************************************************/


/******************************************************************/
void  Som::classifierThreadSparce(int debut, int fin, DISTANCE distance)
{ int xStar, yStar ;
  float dStar ;
  QString str;
  qDebug() << "Running..." ;

  

  for (int i(debut); i < fin; i++)
    {  getBmuMultiThread(i, xStar, yStar, dStar, distance);
       SOM[ xStar ][ yStar ].classe.insert(IdWords[i], dStar);
       fprintf(stderr, "\033[0G... %d", i);
    }
}

bool  Som::classificationMultiThreadSparce(DISTANCE distance, QElapsedTimer time)
{
  if ((SOM != NULL) && (DataMatrixSparce.size() != 0))
  { //etatChanged("Classification en cours...");
    qDebug() <<"Classification en cours.......\n";
     
    //Créer des threads et partager le KeyDataMatrix sur les threads
    int debut, fin = 0;
    QVector <std::thread*> threads(Nb_thread) ;
    for (int i(0); i < Nb_thread-1; i++)
    { debut = fin;
      fin +=Nb_vect/Nb_thread ; 
      threads[i] = new std::thread(&Som::classifierThreadSparce,this, debut, fin, distance); // std::ref(flux)
    }
    // la dernière partie
    threads[Nb_thread-1] = new std::thread(&Som::classifierThreadSparce,this, fin, Nb_vect, distance); 
    for (int i(0); i < Nb_thread; i++) threads[i]->join(); // started thread

  // etatChanged("Classification terminée avec succée");
   QCI = getQCI() ;
   QCE = getQCE() ;
   qDebug() << "\nQualité de la classification (QC) = "<< QCI << "QCE = " << QCE ;
   qDebug() << "Note : On cherche à minimiser QC, (QC --> 0 <=> bonne classification)" ;
   qDebug() << "Classification : " << time.elapsed() / (qint64) 60000 << " minutes" ;
   return true ;
  }
   else
   { qDebug() << "ERREUR: clustering Impossible.";
     return false ;
   }
   
}

void Som::getBmuThread(int debut, int fin, int indx, float & dist_min, int& X, int& Y, DISTANCE distance) 
{ int i,j;							
  float dist;
  for ( i = debut; i < fin; i++ )
   for ( j = 0; j < Col; j++ )
      { dist = (*distance)(DataMatrixSparce[indx], SOM[i][j].w);
        if (dist < dist_min)
        { dist_min = dist;
          X = i;
          Y = j;
        }
      }
}



void Som::getBmuMultiThread(int indx, int& xStar, int& yStar, float& dStar, DISTANCE distance)
{ // nouvelle version création dynamique des threads
   int i;
   QVector <Nstar*> VGs;
   for (i = 0; i < Nb_thread_ap; i++)
   { VGs.append(new Nstar);
     VGs[i]->X = 0;
     VGs[i]->Y = 0;
     VGs[i]->dist = (*distance)(DataMatrixSparce[indx], SOM[0][0].w) ;
   }

   //Créer des threads 
    int debut, fin = 0;
    QVector <std::thread*> threads(Nb_thread_ap) ;
    for (i = 0; i < Nb_thread_ap-1; i++)
    { debut = fin;
      fin +=Lig/Nb_thread_ap ; 
      threads[i] = new std::thread(&Som::getBmuThread,this,debut,fin,indx,
					std::ref(VGs[i]->dist), std::ref(VGs[i]->X), std::ref(VGs[i]->Y), distance);
    }
    // la dernière partie
    threads[i] = new std::thread(&Som::getBmuThread,this,fin,Lig,indx,
					    std::ref(VGs[i]->dist), std::ref(VGs[i]->X), std::ref(VGs[i]->Y), distance); 
   
    // started thread
    for (i = 0; i < Nb_thread_ap; i++) threads[i]->join(); 

    xStar = VGs[0]->X ;
    yStar = VGs[0]->Y ;
    dStar = VGs[0]->dist ;
    for (i = 1; i < Nb_thread_ap; i++)
    { if (VGs[i]->dist < dStar)
       { dStar = VGs[i]->dist ;
         xStar = VGs[i]->X ;
         yStar = VGs[i]->Y ;         
       }
     }
}

// pour la maj on utilise deux threads: 1 prend une partie des voisins et l'autre il prend le reste
void Som::majVect6MultiThreadSparce(int x, int y, int r, int iid, int temps, int Tmax)
{ float alpha,sigma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  sigma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  // création des threads
  std::thread th1(&Som::majVect6ThreadSparce,this,x-r,x,alpha,sigma,x,y,r,iid) ;
  std::thread th2(&Som::majVect6ThreadSparce,this,x,x+r+1,alpha,sigma,x,y,r,iid) ;
  // started 
  th1.join() ;
  th2.join() ;

}

void Som::majVect6ThreadSparce(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid)
{ int i,j;
  float dist;
  float y6, j6;
  if (x%2 == 0) y6 = y + 0.5;
  else  y6 = y;
  for(i= debut; i< fin; i++)
   for(j= y-r; j<= y+r; j++)
    {if (onMap(i,j))
      { if (i%2 == 0) j6 = j + 0.5;
        else  j6 = j;
        dist = pow((i-x),2) + pow((j6-y6),2);
        if (dist <= (1.25)*r*r) 
         majVMSparce(x, y, i, j, sigma, alpha, iid) ; 
           
       }
     }
}

void Som::majVect8MultiThreadSparce(int x, int y, int r, int iid, int temps, int Tmax)
{ float alpha,sigma ;
  alpha	= Alphai * (1.0- (0.+temps)/(0.+Tmax));
  sigma	= Sigmai * pow((Sigmaf / Sigmai),(0.+temps) / (0. + Tmax));
  // création des threads
  std::thread th1(&Som::majVect8ThreadSparce,this,x-r,x,alpha,sigma,x,y,r,iid) ;
  std::thread th2(&Som::majVect8ThreadSparce,this,x,x+r+1,alpha,sigma,x,y,r,iid) ;
  // started 
  th1.join() ;
  th2.join() ;

}

void Som::majVect8ThreadSparce(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid)
{ int i,j;
  float dist;
   for(i= debut; i< fin; i++)
     for(j= y-r; j<= y+r; j++)
       {if (onMap(i,j))
          { dist = pow((i-x),2) + pow((j-y),2);
               if (dist <= (2*r*r))
                 majVMSparce(x, y, i, j, sigma, alpha, iid) ;
          }
        }
}

// MAJ  vecteur pour neurone(x,y) voisin de (i,j
void Som::majVMSparce(int &x, int &y, int &i, int &j, float & sigma, float  &alpha, int &iid) 
{ float fctvoisin,dmodecare;
  dmodecare = pow( max( abs(i-x) , abs(j-y) ) , 2 ); 
  fctvoisin = exp((-1.0)*dmodecare/(2*pow(sigma,2)));
 
  reelXvect(SOM[i][j].w , (1 - alpha*fctvoisin)) ;
  SVector vect ;
  somme(vect , DataMatrixSparce[iid]) ;
  reelXvect(vect, alpha*fctvoisin) ;
  somme(SOM[i][j].w , vect) ;
   		          
}
/********************************************************************************/
// calcul de QCI et QCE

double Som::getQCI()
{ int i, j ;
  int Nb_classes = 0 ;
  double inertie_intraClasse ;
  double inertie_interClasses = 0 ;
  for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { if (SOM[i][j].classe.size() != 0)
       {  inertie_intraClasse = 0 ;
	  Nb_classes ++ ;
          QMapIterator<id, float> it(SOM[i][j].classe) ;
          while (it.hasNext())
          {it.next();
           inertie_intraClasse += it.value()*it.value() ;
          }
	  inertie_intraClasse /= SOM[i][j].classe.size() ;
        }
	inertie_interClasses += inertie_intraClasse ;
      }
   return inertie_interClasses/Nb_classes ;
}
  
double Som::getQCE()
{ double distance = 0 ;
  double dist ;
  int M = Lig * Col ;
  int i, j, k, n ;
  for (i=0; i < Lig; i++)
   for (j=0; j < Col; j++)
    for (k=0; k < Lig; k++)
     for (n=0; n < Col; n++)
      { if ((k*Lig + n) > (i*Lig +j))
 	 { dist = distanceEuclidienneSparceVector(SOM[i][j].w, SOM[k][n].w);
	   distance += dist*dist ;
         }
       }
   return distance/((M*M-M)/2) ;
}   

/******************** I/O ********************************************************************************/
/**************imprimeClasses() , setSomModel() , getSomModel(), getDataMatrixSparce(), setDataMatrixSparce()*********************/

// Imprimer les classes dans un fichier texte:
// Prochainement on va inserer les classes dans la bdd dans une table classe
void Som::imprimeClasses(QString f)
{ if (SOM != NULL)
  { int i,j ;
    QFile file(f);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
    QTextStream flux(&file);
    for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { flux<<"N["<<i<<"]["<<j<<"]:"<<"  "<<SOM[i][j].classe.size()<<endl;
       if (SOM[i][j].classe.size() != 0)
        { QMapIterator<id, float> it(SOM[i][j].classe);
          while (it.hasNext())
          {it.next();
           flux<< "\""<<sqlGetNgram(it.key()) <<"\" ";
          }
         
       flux<<"\n\n";
       }
     }
    file.close();
  }
}

void Som::imprimeClassesPlus(QString f)
{ if (SOM != NULL)
  { int i,j ;
    QFile file(f);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
    QTextStream flux(&file);
    for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { flux<<"N["<<i<<"]["<<j<<"]:"<<"  "<<SOM[i][j].classe.size()<<endl;
       if (SOM[i][j].classe.size() != 0)
        { QMapIterator<id, float> it(SOM[i][j].classe);
          while (it.hasNext())
          {it.next();
           flux<< "\""<<Words.at(it.key()) <<"\" ";
          }
         
       flux<<"\n\n";
       }
     }
    file.close();
  }
}

void Som::imprimeIdIJDist(QString f, QString nameDB, int seuil,  int iterations, QString dist, bool initial)   // imprime un fichier texte: id i j distance
{ if (SOM != NULL)			
  { int i,j ;
    QFile file(f);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))  return;
    QTextStream flux(&file);
    flux.setCodec("iso-8859-1");
    // en tête du fichier
    flux << "# Base_de_données " << nameDB <<endl ;
    flux << "# Seuil " << seuil <<endl ;
    flux << "# SOM " << Lig <<"x"<< Col <<endl ;
    flux << "# Nbre_itérations " << iterations <<endl ;
    flux << "# Distance_utilisée " << dist <<endl ;
    flux << "# Nbre_de_ngrammes " << Nb_vect <<endl ;
    flux << "# Initialise_SOM_au_centre " << initial << endl;
    flux << "# QCI (HI) = " << QCI << endl ;
    flux << "# QCE (HO) = " << QCE << endl ;
        
    /*********************************************************************/
    flux << Lig <<" "<< Col <<endl ;
    for (i=0;  i < Lig; i++)
     for (j=0; j < Col; j++)
     { if (SOM[i][j].classe.size() != 0)
       {  QMapIterator<id, float> it(SOM[i][j].classe) ;
          while (it.hasNext())
          {it.next();
           flux<< it.key() <<" "<< i <<" "<< j <<" "<< it.value() <<endl ;
          }
       
       }
     }
    file.close();
    
  }
}


int Som::setSomModel(QString mon_fichier)
{
   QFile file(mon_fichier);
   if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return 0;
   QTextStream flux(&file);
   flux.setCodec("UTF-8");
    flux << Lig <<" "<< Col <<"\n" ;
    int i,j,k;
    
    for (i = 0 ; i < Lig; i++)
     for (j = 0 ; j < Col; j++)
      {  
         for (k=0; k < SOM[i][j].w.size(); k++)
          { 
            flux << SOM[i][j].w.at(k).index<<":"<< SOM[i][j].w.at(k).val<<" ";
          }
        flux<<"\n";
      }
  file.close() ;
 return 1;
}




int Som::getSomModel(QString vmFile)  // charger les vecteurs mémoire à partir d'un fichier texte.
{ QFile file(vmFile);
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text))  return 0;
  QTextStream flux;
  flux.setDevice(& file);
  qDebug() << "getVM en cours..." ;
  int i, x, y ;
  int fLig, fCol ;
  int nl = 0 ;
  QString line = flux.readLine().simplified();
  QStringList lineLst = line.split(" ");
  fLig = lineLst[0].toInt() ;
  fCol = lineLst[1].toInt() ;
  qDebug() <<fLig<<"  "<<fCol ;
  if ((fLig == Lig) && (fCol == Col))
   { while (!flux.atEnd())
    {
        QString line = flux.readLine().simplified();
        QStringList lineLst = line.split(" ");
        qDebug() << line ;
          x = nl / Col ;
          y = nl % Col ;
        qDebug() << x <<" , "<<y ;
          for (i = 0; i < lineLst.size(); i++)
            { QStringList strl = lineLst[i].split(":");
              ajouter(SOM[x][y].w, strl[0].toInt(), strl[1].toFloat()) ;
	    }

	nl++ ;
    }
   }
   else
    { qDebug() << "Erreur: le fichier pour getVM ne correspond pas au SOM" ;
      
    }
      
   file.close();
   return 1 ;
}


bool Som::getDataMatrixSparce(QString mon_fichier)
{
    QFile file(mon_fichier);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
     { qDebug() << "Impossible d'ouvrir le fichier " << mon_fichier ; 
       return false;
     }

    QTextStream flux(&file);
    qDebug() << "chargement en cours..." ;
    int indx_min = 1000000 ;
    int indx_max = 0 ;
    int nl=0;
    int i;
    while (!flux.atEnd())
    {
        QString line = flux.readLine().simplified();
        QStringList lineLst = line.split(" ");
        
        if (lineLst.size() > 1)
        {  Words.append(lineLst[0]) ;
           IdWords.append(nl /*lineLst[0].toInt()*/) ;
	   SVector vecteur ;
           for (i = 1; i < lineLst.size(); i++)
            { QStringList strl = lineLst[i].split(":");
              ajouter(vecteur, strl[0].toInt(), strl[1].toFloat()) ;
              if (indx_max < strl[0].toInt()) indx_max = strl[0].toInt() ;
              if (indx_min > strl[0].toInt()) indx_min = strl[0].toInt() ;
	    }

	   DataMatrixSparce.append(vecteur) ;
           nl++ ;
         
         }
        
    }
  
    file.close();
    qDebug() << "chargement termine" ;
    Nb_vect = nl ;
    qDebug() << Nb_vect ;
     qDebug() << "indx_min = " << indx_min ;
     qDebug() << "indx_max = " << indx_max ;
  return true;
} 

int Som::setDataMatrixSparce(QString mon_fichier)
{
   QFile file(mon_fichier);
   if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return 0;
   QTextStream flux(&file);
   flux.setCodec("UTF-8");
    int i,j, k = 0;
    qDebug() << "sauvegarde de la matrice en cours..." ;
    for (i = 0 ; i < DataMatrixSparce.size(); i++)
     {  QString str = sqlGetNgram(IdWords.at(i)) ;
        if (str.contains("\n") || str.contains(" ")) continue ;
        flux << sqlGetNgram(IdWords.at(i)) <<" " ; 
        for (j = 0 ; j < DataMatrixSparce[i].size() ; j++)
        {  
         flux << DataMatrixSparce[i][j].index<<":"<< DataMatrixSparce[i][j].val<<" ";
        }
        flux<<"\n";
        k ++ ;
      fprintf(stderr, "\033[0GProcessed %d / %d itérations ", i ,DataMatrixSparce.size());
     }
  
  file.close() ;
  qDebug() <<"\n"<< k ;

 return 1;
}


/************************************************************************************************************/
/********************************* Accée à la bdd  ***********************************************************/
/**********************************************************************************************************/

int Som::sqlGetCountNgrams()  // pour récupérer le nombre total des ngrams => taille des vecteurs
{ QSqlQuery reqn;
  int nbN ;
  if(not reqn.exec("select count (ngram) from  ngramme where stoplist = 'f' "))
  { qDebug() << reqn.lastError().text()<< endl;
    return -1; }
   while(reqn.next()) nbN = reqn.value(0).toLongLong();
  
  return nbN;
}

int Som::sqlGetCountWords()  // pour récupérer le nombre total des ngrams => taille des vecteurs
{ QSqlQuery reqn;
  int nbN ;
  if(not reqn.exec("select count (form) from  WORD_form "))
  { qDebug() << reqn.lastError().text()<< endl;
    return -1; }
   while(reqn.next()) nbN = reqn.value(0).toLongLong();

  return nbN;
}
// à partir des tables matrice, ngramme, on construit une matrice de donnée ==> SOM
// on colle dans DataMatrix les valeurs de la matrice
// il serait peut-être plus rapide de trier par ligne et d'affecter ligne par ligne ?



bool Som::sqlGetDataMatrix() 
{ QSqlQuery reqm ;
  id ligne = 0 ; 	    // ligne corresponde à id_ngram 
  id col ;
  int i = 0 ;
  Taille = sqlGetCountNgrams() ;
  double valeur; 
  int nb_val = 0; // nombre de valeurs non null
  etatChanged("Construction de la matrice en cours...");   // envoie de message d'état à l'interface
  qDebug() << "Construction de la matrice en cours" ;
  if(not reqm.exec("select fk_i,fk_j,val from matrice  order by fk_i,fk_j")) // On récupère i, j, valeur /*where stoplist = 'f'
  { qDebug() << reqm.lastError().text()<< endl;
    return false; }
  while(reqm.next())
  { if (ligne != reqm.value(0).toLongLong()) // if condition, On passe à ligne suivante dans la DataMatrix
     { ligne = reqm.value(0).toLongLong() ;
       IdWords.append(ligne);
       SVector vecteur ;
       DataMatrixSparce.append(vecteur) ;
       i++;
      }
   col = reqm.value(1).toLongLong() ;
   valeur =(double)reqm.value(2).toLongLong() ;
   
   ajouter(DataMatrixSparce[i-1], col, valeur);
   
   nb_val ++ ;
   fprintf(stderr, "\033[0GProcessed %d / %d itérations ", i , Taille);
   // afficher dans l'interface graphique l'évolution de processus
   //etatSOM =  "Construction de la matrice en cours..." + QString::number(ligne + 1) + "/"+  QString::number(Taille);
   
  }
  Nb_vect = DataMatrixSparce.size() ;
  qDebug() <<"Nb_vect = "<<Nb_vect;
  qDebug() <<"Nb de valeurs non null = "<< nb_val <<" / " << (long long)Nb_vect*Nb_vect << "===>"
					 <<(double)nb_val/((long long)Nb_vect*Nb_vect) <<"%" ;
  return true;
}

bool Som::sqlGetDataMatrix_pattern()
{ QSqlQuery reqm ;
  id ligne = 0 ; 	    // ligne corresponde à id_ngram
  id col ;
  int i = 0 ;
  Taille = sqlGetCountWords() ;
  double valeur;
  int nb_val = 0; // nombre de valeurs non null
  etatChanged("Construction de la matrice en cours...");   // envoie de message d'état à l'interface
  qDebug() << "Construction de la matrice en cours"<<endl;
  if(not reqm.exec("select fk_form,fk_pattern,val from WORD_matrix  order by fk_form,fk_pattern")) // On récupère i, j, valeur
  { qDebug() << reqm.lastError().text()<< endl;
    return false; }
  while(reqm.next())
  { if (ligne != reqm.value(0).toLongLong()) // if condition, On passe à ligne suivante dans la DataMatrix
     { ligne = reqm.value(0).toLongLong() ;
       IdWords.append(ligne);
       SVector vecteur ;
       DataMatrixSparce.append(vecteur) ;
       i++;
      }
   col = reqm.value(1).toLongLong() ;
   valeur =(double)reqm.value(2).toLongLong() ;
   ajouter(DataMatrixSparce[i-1], col, valeur); // insérer dans la ligne courante
   nb_val ++ ;

   // afficher dans l'interface graphique l'évolution de processus
   //etatSOM =  "Construction de la matrice en cours..." + QString::number(ligne + 1) + "/"+  QString::number(Taille);

  }
  Nb_vect = DataMatrixSparce.size() ;
  qDebug() <<"Nb_vect = "<<Nb_vect;
  qDebug() <<"Nb de valeurs non null = "<< nb_val <<" / " << Nb_vect*Nb_vect << "===>" <<(double)nb_val/(Nb_vect*Nb_vect) <<"%" ;
  return true;
}


QString Som::sqlGetNgram(id id_ngram)  // pour récupérer nom d'un ngram étant donné son id_ngram
{ QSqlQuery reqn;
  QString gram ="";
  reqn.prepare("select ngram from ngramme where id_ngram = ?"); 
  reqn.addBindValue(id_ngram);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return ""; }
   while(reqn.next()) gram = reqn.value(0).toString();
  
  return gram;
}

QString Som::sqlGetWord(id id_word)  // pour récupérer nom d'un ngram étant donné son id_ngram
{ QSqlQuery reqn;
  QString gram ="";
  reqn.prepare("select ngram from WORD_form where id = ?");
  reqn.addBindValue(id_word);
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return ""; }
   while(reqn.next()) gram = reqn.value(0).toString();

  return gram;
}

int Som::sqlGetFrequence(id id_ngram)  // pour récupérer la fréquence d'un ngram donné
{ QSqlQuery reqn;
  int freq = -1;
  reqn.prepare("select frequence from ngramme where id_ngram = ?"); // pour récupérer la fréquence d'un ngram donné
  reqn.addBindValue(id_ngram);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return -1; }
   while(reqn.next()) freq = reqn.value(0).toLongLong();
  
  return freq;
}



/********************************************************************/

/*************************************************************************************************/

// On va normaliser tous les vecteurs d'entrées

void Som::normaliseTousVecteurs()
{ int i;
  for (i = 0; i < DataMatrixSparce.size(); i++)
   normalise(DataMatrixSparce[i]);
}


/*********************************************************************************/


