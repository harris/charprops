
#include "classifier.h"

using namespace std;

classifier::classifier(): QWidget()
{
   setFixedSize(800, 600);

        classifierBouton = new QPushButton("Classifier");
        quitterBouton = new QPushButton("Quitter");

        QHBoxLayout *layoutBouton = new QHBoxLayout;
	
        // ajouter les boutons au layoutBouton
        layoutBouton->addStretch();
        layoutBouton->addWidget(classifierBouton);
        layoutBouton->addWidget(quitterBouton);
	

       // Extraction des vecteurs

        NameBDD = new QLineEdit("CorpusDB");
        Seuil   = new QSpinBox;
        Seuil->setRange(0,30000);
  	Seuil->setValue(0);
 	Seuil->setMaximumWidth(80);
        QFormLayout *chargerLayout = new QFormLayout;
        chargerLayout->addRow("BDD-->Donnees d'entree du SOM:",NameBDD);
	chargerLayout->addRow("Seuil:",Seuil);

	// fichiers résultats
	 classesOut = new QLineEdit("../classes1.txt") ;
	 motNeurone = new QLineEdit("../mot_neuronne1.txt") ;
         QFormLayout *resultatLayout = new QFormLayout;
	 resultatLayout->addRow("SOM---> calsses :",classesOut);
	 resultatLayout->addRow("SOM---> mot_neurone :",motNeurone);
         
   	 resultat = new QGroupBox("Fichiers Resultat:");
         resultat->setLayout(resultatLayout);
         resultat->setEnabled(true);

        //paramètres SOM
         N_som = new QSpinBox;
         N_som->setRange(1,30);
         N_som->setValue(10);
         N_som->setMaximumWidth(50);

         M_som = new QSpinBox;
         M_som->setRange(1,30);
         M_som->setValue(10);
         M_som->setMaximumWidth(50);

         T_max = new QSpinBox;
         T_max->setRange(0,2000000);
         T_max->setValue(1000);
         T_max->setMaximumWidth(100);

	 Alpha0 = new QDoubleSpinBox;
         Alpha0->setRange(0.3,0.9);
	 Alpha0->setSingleStep(0.1);
         Alpha0->setValue(0.5);
         Alpha0->setMaximumWidth(80);

	 Ksigma0 = new QDoubleSpinBox;
         Ksigma0->setRange(0.15,0.9);
	 Ksigma0->setSingleStep(0.05);
         Ksigma0->setValue(0.4);
         Ksigma0->setMaximumWidth(80);

    	 Topo = new QComboBox ;
         Topo->setMaximumWidth (140);
         QStringList topo ;
  	 topo << "hexagonal" << "regtangle"  ;
         Topo->addItems(topo) ;

	 Dist = new QComboBox ;
         Dist->setMaximumWidth (200);
	 Dist->addItem("distance Euclidienne");
	 Dist->addItem("distanceCosinus");
	 

	 Nb_th = new QSpinBox ;
	 Nb_th->setRange(1,1024);
         Nb_th->setValue(2);
         Nb_th->setMaximumWidth(50);

	 Nb_thAp = new QSpinBox ;
	 Nb_thAp->setRange(1,10);
         Nb_thAp->setValue(2);
         Nb_thAp->setMaximumWidth(50);
    	 
	 initialise = new QCheckBox("initialise SOM au centre de données") ;
         normalise = new QCheckBox("normaliser les vecteurs d'entrées") ;
         QVBoxLayout *layoutCheckBox = new QVBoxLayout;
  	 layoutCheckBox->addWidget(normalise);
	 layoutCheckBox->addWidget(initialise);

         QFormLayout *somLayout = new QFormLayout;
         somLayout->addRow("Nsom :", N_som);
         somLayout->addRow("Msom :", M_som);
         somLayout->addRow("Tmax :", T_max);
         somLayout->addRow("Alpha0 :", Alpha0);
	 somLayout->addRow("K (k*Sigmai) :", Ksigma0);
         somLayout->addRow("Topologie :", Topo);
	 somLayout->addRow("Distance :", Dist);
	 somLayout->addRow("Nb_Threads pour classification :", Nb_th);
         somLayout->addRow("Nb_Threads pour l'apprentissage :", Nb_thAp);
 	 

         groupSom = new QGroupBox("Parametres du SOM:");
         groupSom->setLayout(somLayout);
	 groupSom->setEnabled(true);

	
	// etatSOM
	QHBoxLayout *layoutLabel = new QHBoxLayout;
	etatSOM = new QLabel; 
        layoutLabel->addWidget(etatSOM);	
	
        QVBoxLayout *layoutPrincipal = new QVBoxLayout;
       

        layoutPrincipal->addLayout(chargerLayout);
        layoutPrincipal->addWidget(resultat);
        layoutPrincipal->addWidget(groupSom);
        layoutPrincipal->addLayout(layoutCheckBox);
        layoutPrincipal->addLayout(layoutBouton);
	layoutPrincipal->addLayout(layoutLabel);
       

        // ajouter layoutPrincipal a la fenetre
         setLayout(layoutPrincipal);
     
        // Connexion des signaux et des slots
        connect(quitterBouton, SIGNAL(clicked()), this, SLOT(close()));
        connect(classifierBouton,SIGNAL(clicked()),this, SLOT(classer()));
}

void classifier::onEtatChanged(QString str)
{ etatSOM->setText(str);
  etatSOM->setWordWrap(true) ;
  etatSOM->repaint() ;
}

void classifier::classer()
{ QElapsedTimer time ;
  time.start() ;
  int source ;
  bool clas = false ;
  BDD bdd;
  
  long long tmax ;

  qDebug() << "Chronomètre relancé : " << time.restart() / (qint64) 60000 << " minutes" ; 

 // créer la carte SOM
  som = new Som(N_som->value(), M_som->value(), Nb_th->value(), Nb_thAp->value(), Alpha0->value(), Ksigma0->value());

  // afficher des informations sur l'interface graphique
  connect(som, SIGNAL(etatChanged(QString)), this, SLOT(onEtatChanged(QString)));

  if ((source = NameBDD->text().indexOf('.')) != -1)  // vecteurs d'entrée fichiers plat
   som->getDataMatrixSparce(NameBDD->text()) ;

  else // on cherche les vecteurs d'entrées à partir de la BDD
   { bdd.setConnexion("localhost","Charprops","charprops",NameBDD->text()); // connexion à la bdd
     if (bdd.getConnexion())
     { som->sqlGetDataMatrix();
       //som->sqlGetDataMatrix_pattern();
       //bdd.fermerConnexion();  // on ferme la connexion à la bdd pour gagner de la place en mémoire
       source = -2 ;
     }
    }
    
    //som->setDataMatrixSparce("../matrix3.data") ;

    // normalisation des vecteurs d'entrées
    if ((normalise->isChecked()) || (Dist->currentText() == "distanceCosinus")) som->normaliseTousVecteurs();
   

    
    if (initialise->isChecked()) som->initialiseSOM_VU();
      // som->initialiseSOM(4) ;
   // som->getSomModel("../somModel_initial");
     
        tmax = ((T_max->value()== 0) ? 10*som->getnb_vect() : T_max->value());
    
    if (Dist->currentText() == "distance Euclidienne")
    { som->apprentissage(tmax, Topo->currentText(), &distanceEuclidienneSparceVector, time);
      clas = som->classificationMultiThreadSparce(&distanceEuclidienneSparceVector, time);
    }
    else 
    { som->apprentissage(tmax, Topo->currentText(), &distanceCosinus, time);
      clas = som->classificationMultiThreadSparce(&distanceCosinus, time);
    }

    som->imprimeIdIJDist(motNeurone->text(), NameBDD->text(), Seuil->value(), 
                                    T_max->value(), Dist->currentText(), initialise->isChecked() ) ;

    som->setSomModel("../somModel") ;
  
   if ((source != -1) && clas )
   { if (source == -2)
      { bdd.ouvrirConnexion(); // On ouvre la connexion à nouveau
        if (bdd.getConnexion()) som->imprimeClasses(classesOut->text());
        
      }
     else som->imprimeClassesPlus(classesOut->text());
    QMessageBox::information(this, "Classification terminee", "consultez le fichier" + classesOut->text());
   }
  else QMessageBox::warning(this ,"Attention", "Classification impossible") ;
 
  
}

void classifier::on_vm_textChanged()
{
   groupSom->setEnabled(true);
}
