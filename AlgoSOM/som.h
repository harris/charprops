#ifndef SOM_H_INCLUDED
#define SOM_H_INCLUDED

#include <iostream>
#include <thread>
#include <QtWidgets>
#include "../Common/BDD.h"

#include "vecteur_creux.h"


class Som : public QWidget
{
    int Lig;      // lig x col = nombre de neurones de la carte
    int Col;
    int Taille;   // taille du vecteur mémoire

    int Nb_thread ;       // pour la classification on divise les données sur le nombre Nb_thread
    int Nb_thread_ap ;   // sert pour trouver le BMU , on divise la carte sur le nombre Nb_thread_ap

    float Alphai ; // paramètres de la fct d'Apprentissage, une fonction Gaussiènne
    float Ksigmai ;
    float Sigmaf ;
    float Sigmai ;
    

    
    //int Nb_vect;         // nombre de vecteurs d'entrées = dataMatrix.size()
    int Nb_vect;

    QString etatSOM;		// pour afficher l'évolution de SOM dans l'interface graphique

        
    struct NeuroneSparce
    {   SVector w;		  // vecteur mémoire
        QMap<id, float> classe;	 // classe contient tous les mots associer à ce neurone 
    } ;
    NeuroneSparce **SOM ; 		// la carte topologique SOM
   
    
   // Matrice creuse DataMatrixSparce
    QVector <QString> Words ;
    QVector <id> IdWords ; // pour stocker les idt des mots
    QVector <SVector> DataMatrixSparce ; // la matrice creuse de données

   
    struct Nstar  // used by Multithreading
    { int X; 
      int Y;
      float dist;
    } ;  

    double QCI ; // qualité de classification, tous les threads en ont accés 
    double QCE ;

   // std::recursive_mutex _lock;

    

  Q_OBJECT

    
  public:

    Som(QString, int, int);
    Som(int, int, int, int, float , float , float = 0.1);
    ~Som();
        
    bool  apprentissage(int, QString, DISTANCE, QElapsedTimer);

    void  initialiseSOM(int taux);
    void  initialiseSOM_VU() ;
       

        
    
    int getnb_vect() { return DataMatrixSparce.size(); }
    bool sqlGetDataMatrix() ;
    bool sqlGetDataMatrix_pattern() ;
    
    int sqlGetFrequence(id id_ngram);  // pour récupérer la fréquence d'un ngram donné
    int sqlGetCountNgrams();  // pour récupérer le nombre total des ngrams => taille des vecteurs
    int sqlGetCountWords();
    
    
    QString sqlGetNgram(id id_ngram);  // pour récupérer nom d'un ngram étant donné son id_ngram
    QString sqlGetWord(id id_ngram);

    void normaliseTousVecteurs() ;
   

   

    //Nouvelle version pour les matrices creuses
    void initialiseSparceVM(SVector & Mem, int taux) ;
    bool  classificationMultiThreadSparce(DISTANCE distance, QElapsedTimer time) ;
    void  classifierThreadSparce(int debut, int fin, DISTANCE distance) ;

    

    bool getDataMatrixSparce(QString mon_fichier) ;
    int  setSomModel(QString mon_fichier) ;
    int  getSomModel(QString mon_fichier) ;

    int  setDataMatrixSparce(QString mon_fichier) ;
  

   // imprime un fichier texte: id i j distance 
    void imprimeIdIJDist(QString f, QString nameDB, int seuil,  int iterations, QString dist, bool) ;  
    void imprimeClasses(QString);
    void imprimeClassesPlus(QString);

    

   

    signals:
     void etatChanged(QString);

  private:
      
    bool onMap(int , int) ; // = true si (i,j) dans la carte SOM sinon false
    
    void SOM_clean();          // pour suprimer SOM du mémoire
    
    

   
    
   // calcul de QCI et QCE
    double getQCI() ;
    double getQCE() ;
    double getQCE_R() ;

   // Nouvelle version pour les matrices creuses
    void majVMSparce(int &x, int &y, int &i, int &j, float & sigma, float  &alpha, int &iid) ;
    void majVect8ThreadSparce(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid);
    void majVect8MultiThreadSparce(int x, int y, int r, int iid, int temps, int Tmax);
    void majVect6ThreadSparce(int debut, int fin, float alpha, float sigma,int x, int y, int r, int iid);
    void majVect6MultiThreadSparce(int x, int y, int r, int iid, int temps, int Tmax) ;

    void getBmuMultiThread(int indx, int& xStar, int& yStar, float& dStar, DISTANCE distance) ;
    void getBmuThread(int debut, int fin, int indx, float & dist_min, int& X, int& Y, DISTANCE distance) ;
	
};

#endif // SOM_H_INCLUDED
