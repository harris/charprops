/*******************************************************************************
 *Auteur : Nourredine Aliane nourredine@ai.univ-paris8.fr
 *Objectif :Implantation des opérations agissant sur le type Vecteur Creux
 * *********************************************************************/

#include "vecteur_creux.h"

void ajouter(SVector & vect, int indx, float valeur)
{  
    Cellule cell ;
    cell.index = indx ;
    cell.val = valeur ;
    vect.append(cell) ;
}

void inserer(SVector & vect, int indx, float valeur)
{
  int i = 0 ;
  while ((i < vect.size()) && (indx > vect[i].index))
    i++ ;
  Cellule cell ;
  cell.index = indx ;
  cell.val = valeur ;
  vect.insert(i,cell) ;
}

void afficher(SVector & vect)
{
  for (int i = 0; i < vect.size(); i++)
    qDebug() << vect.at(i).index <<":"<< vect.at(i).val ;
}

void reelXvect(SVector & vect, float k)
{
  int size = vect.size() ; 
  Cellule * pv = vect.data();
  for (int i = 0; i < size; i++)
   pv[i].val *= k ;
}

void somme(SVector & W , SVector & X) // vect1 += vect2 
{
   int i = 0, j = 0 ;
   int sizeX = X.size() ; 
   
    while ((i < W.size()) && (j < sizeX))
     {     
         Cellule * pw = W.data();
   	 Cellule * px = X.data();  
  	 if ( pw[i].index == px[j].index) 
           { pw[i].val += px[j].val ;
             i++ ;
	     j++ ;
           }
         else if (pw[i].index < px[j].index)
          i++ ;
	     else 
                 {inserer(W , X[j].index , X[j].val) ;
                  j++ ;
	         }   
       }
   
    if (i == W.size())
     while (j < sizeX)
     { inserer(W , X[j].index , X[j].val) ;
       j++ ;
      }
}
 

double reel_aleatoire_a_b(double a, double b)
{
    return ( rand()/(double)RAND_MAX ) * (b-a) + a;    // generateur des reels aleatoires entre a et b
}

int entier_aleatoire_a_b(int a,int b)
{
   return (rand() % (b - a )) + a;
}

int max(int a, int b)
{
    if ( a < b) return b ;
    else return a ;
}

int min(int a, int b)
{
     if ( a < b) return a ;
      else return b ;
}

void melanger(QVector <int>  & vect)
{
    int i=0;
    int nombre_tire=0;
    int temp=0;
    int taille = vect.size() ;
    int * tableau = vect.data() ;
    
    for(i = 0; i< taille;i++)
     {  nombre_tire = entier_aleatoire_a_b(0, taille);
        // On échange les contenus des cases i et nombre_tire
        temp = tableau[i];
        tableau[i] = tableau[nombre_tire];
        tableau[nombre_tire]=temp;
     }
}

float distanceEuclidienneSparceVector(SVector  & W, SVector & X)
{
    float some = 0.0;
      int i = 0, j = 0 ;
      int sizeW = W.size() ;
      int sizeX = X.size() ;
      Cellule * pw = W.data();
      Cellule * px = X.data();
    
     while ((i < sizeW) && (j < sizeX))
     {        
  	 if ( pw[i].index == px[j].index) 
           { some += (pw[i].val - px[j].val)*(pw[i].val - px[j].val) ;
             i++ ;
	     j++ ;
           }
         else if (pw[i].index < px[j].index)
          {some += pw[i].val * pw[i].val ;
            i++ ;
	   }
               else 
                 {some += px[j].val * px[j].val ;
                  j++ ;
	         }   
       }
   
    if (i == sizeW)
     while (j < sizeX)
     { some += px[j].val * px[j].val ;
       j++ ;
      }
    else 
      while (i < sizeW)
     { some += pw[i].val * pw[i].val ;
       i++ ;
      }
 
   return sqrt(some) ;
}

// Calul la norme d'un vecteur 

float norme(SVector & X)
{ float result = 0 ;          // résultat du calcul
  
  int i = 0 ;
  int sizeX = X.size() ;
  Cellule * px = X.data();
  while (i < sizeX)
  { result += pow(px[i].val , 2) ;
    i++ ;
  }
 return sqrt(result) ;
}

// Normalise un vecteur

void normalise(SVector & X)
{ float norm = norme(X) ;
  int i = 0 ;
  int sizeX = X.size() ;
  Cellule * px = X.data();
  if ( norm != 0)
  { while (i < sizeX)
    { px[i].val /= norm ;
      i++ ;
    }
  }
}


// produit scalaire

float produitScalaire(SVector & X, SVector & W) 
{ float result = 0 ; 	   // résultat du calcul
  int i = 0, j = 0 ;
  int sizeW = W.size() ;
  int sizeX = X.size() ;
  Cellule * pw = W.data();
  Cellule * px = X.data();
  while ((i < sizeW) && (j < sizeX))
     {        
  	 if ( pw[i].index == px[j].index) 
           { result += pw[i].val *  px[j].val ;
             i++ ;
	     j++ ;
           }
         else if (pw[i].index < px[j].index)
               i++ ;
	      else 
                j++ ;
      }

 return result ;
}

float cosTita(SVector & X, SVector & W) // X est déjà normalisé
{ float norme_vm = norme(W) ;
  if (norme_vm != 0)
    return produitScalaire(X,W)/norme_vm ;
  else 
    return 0 ; 
}

float distanceCosinus(SVector & X, SVector & W) // X vecteur d'entrée et Mem pour vecteur mémoire d'un neurone
{ float norme_vm = norme(W) ;
  if (norme_vm != 0)
    return (1 - produitScalaire(X,W)/norme_vm) ;
  else 
    return 0 ; 
}
