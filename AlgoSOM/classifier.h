#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include "som.h"

class classifier : public QWidget
{
    Q_OBJECT

    public:
        classifier();
        
    private slots:
        void classer();
        void on_vm_textChanged();
        void onEtatChanged(QString);

    private:
        QPushButton *classifierBouton;
        QPushButton *quitterBouton;

        QLineEdit *NameBDD;
	QSpinBox *Seuil;
	QLineEdit *classesOut;
	QLineEdit *motNeurone;
        QCheckBox *initialise;
	QCheckBox *normalise ;
        QSpinBox *N_som;
        QSpinBox *M_som;
        QSpinBox *T_max;
 	QDoubleSpinBox *Alpha0 ;
	QDoubleSpinBox *Ksigma0 ;
        QSpinBox *Nb_th;
	QSpinBox *Nb_thAp;
        QComboBox *Topo; 
        QComboBox *Dist;
        QGroupBox *groupSom;
 	QGroupBox *resultat;

       // Etat de SOM
       QLabel *etatSOM;

       Som * som;
};


#endif // CLASSIFIER_H
