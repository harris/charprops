#ifndef WINDOW_H
#define WINDOW_H

#include <QtWidgets>
#include <QtSql>
#include "customviewdialog.h"
typedef uint idd ;
//typedef QList < QPair<QRegExp,QColor> > ListRequete;


typedef QMap<idd, double> classe ;	 // classe contient tous les mots associer à ce neurone

class ModelSom: public QAbstractTableModel
{ Q_OBJECT

  public:
    ModelSom();
    virtual int rowCount( const QModelIndex &parent) const {return RowCarte ;} ;
    virtual int columnCount(const QModelIndex &parent) const {return ColCarte ;};
    virtual QVariant data( const QModelIndex &index, int role) const ;
    bool setData(const QModelIndex &index, const QVariant &value, int role) ;
    Qt::ItemFlags flags(const QModelIndex &index) const ;
    
    QString sqlGetNgram(idd ) const;
    int offset(int ) const ;
    void createCarte(int , int) ;
    classe * * CarteTopo  ;    // la carte topologique SOM
    
    int RowCarte ;
    int ColCarte ;
    //ListRequete Requete ;
    
private:
  QString cellVisibleContent(int, int) const;
  CustomViewDialog cvd ;
  

};

#endif
