#include "customviewdialog.h"

CustomViewDialog::CustomViewDialog(QWidget *parent)
: QDialog(parent)
{ ContenerLayout = new QVBoxLayout(this) ;
  
  QGroupBox * operationbox = new QGroupBox(tr("")) ;
  ContenerLayout->addWidget(operationbox, 0) ;
  QGridLayout * operationlayout = new QGridLayout ;
  
  
  AddButton = new QPushButton("+") ;
  AddButton->setFixedWidth(25) ;
  AddButton->setFixedHeight(25) ;
  AddButton->setToolTip("Select another pattern") ;
  AddButton->setEnabled(false) ;
  connect(AddButton, SIGNAL(clicked()), this, SLOT(addPattern())) ;
  operationlayout->addWidget(AddButton, 0, 0) ;
  
  FinishedButton = new QPushButton("Finished") ;
  FinishedButton->setFixedWidth(85) ;
  connect(FinishedButton, SIGNAL(clicked()), this, SLOT(accept())) ;
  operationlayout->addWidget(FinishedButton, 0, 1,Qt::AlignRight) ;
  operationbox->setLayout(operationlayout) ;
  
  addPattern() ;
  setLayout(ContenerLayout) ;
  setWindowTitle("Custom View") ;
}

void CustomViewDialog::addPattern()
{   PatternGroupBox= new QGroupBox(tr("")) ;
    ContenerLayout->addWidget(PatternGroupBox,1) ;

    QGridLayout * patternlayout = new QGridLayout ;
    PatternLocateLabel = new QLabel("Pattern to locate (regular expression):") ;
    PatternLocateText = new QLineEdit ;
    SelectColorLabel = new QLabel("Select the color for this pattern:") ;
   // SelectColorComboBox = new QComboBox ;
    ValidateButton = new QPushButton("Validate") ;

   /* QPixmap pixmap(QSize(100,17));
    pixmap.fill(Qt::red);
    QIcon icon (pixmap);
    SelectColorComboBox->setIconSize(pixmap.size());
    SelectColorComboBox->addItem(icon, "Red");

    QPixmap pixmap2(QSize(100,17));
    pixmap2.fill(Qt::green);
    QIcon icon2 (pixmap2);
    SelectColorComboBox->setIconSize(pixmap2.size());
    SelectColorComboBox->addItem(icon2, "Green");

    QPixmap pixmap3(QSize(100,17));
    pixmap3.fill(Qt::blue);
    QIcon icon3 (pixmap3);
    SelectColorComboBox->setIconSize(pixmap3.size());
    SelectColorComboBox->addItem(icon3, "Blue");
*/
 //+++++
    SelectColorComboBox= new QComboBox(this);
    QStringList colorNames ;
    colorNames << "red" << "green" << "blue" ;
    int index = 0;
    foreach (const QString &colorName, colorNames) {
    QColor color(colorName);
    SelectColorComboBox->addItem(colorName, color);
    const QModelIndex idx = SelectColorComboBox->model()->index(index++, 0);
    SelectColorComboBox->model()->setData(idx, color, Qt::BackgroundColorRole);
    }
//+++++
    patternlayout->addWidget(PatternLocateLabel, 0, 0) ;
    patternlayout->addWidget(PatternLocateText, 0, 1) ;
    patternlayout->addWidget(SelectColorLabel, 1, 0) ;
    patternlayout->addWidget(SelectColorComboBox, 1 ,1) ;
    ValidateButton->setFixedWidth(85) ;
    patternlayout->addWidget(ValidateButton, 2, 1) ;

    PatternGroupBox->setLayout(patternlayout) ;
    connect(ValidateButton, SIGNAL(clicked()), this, SLOT(validatePattern()));
}

void CustomViewDialog::validatePattern()
{
  //QPushButton *bouton =  (QPushButton *)(QObject::sender());
    PatternLocateText->hide();
    SelectColorComboBox->hide();
    ValidateButton->hide();
    PatternLocateLabel->setText(tr("Pattern to locate : ") +  PatternLocateText->text()) ;
    SelectColorLabel->setText(tr("Color selected : ") + SelectColorComboBox->currentText()) ;
    Requete << QPair<QRegExp, QString>(QRegExp(PatternLocateText->text()), SelectColorComboBox->currentText()) ;
    AddButton->setEnabled(true);

}


