cmake_minimum_required(VERSION 2.6)
project(visu_som)


find_package(Qt5Widgets REQUIRED)
find_package(Qt5Network REQUIRED)

set(SOURCES main.cpp windowsom.cpp modelsom.cpp customviewdialog.cpp dialogngram.cpp )
set(HEADERS windowsom.h modelsom.h  customviewdialog.h dialogngram.h)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

add_definitions(-Wall ${QT_DEFINITIONS} ${Qt5Widgets_DEFINITIONS})

include_directories(${Qt5Widgets_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR})


qt5_wrap_cpp(HEADERS_MOC ${HEADERS})

qt5_wrap_ui(FORMS_MOC ${FORMS})
qt5_add_resources(RESOURCES_RCC ${RESOURCES})

add_executable(visu_som ${SOURCES} ${HEADERS_MOC} ${FORMS_MOC} ${RESOURCES_RCC})

qt5_use_modules(visu_som Network Sql Widgets)

