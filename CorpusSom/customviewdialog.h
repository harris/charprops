#ifndef CUSTOMVIEWDIALOG_H
#define CUSTOMVIEWDIALOG_H
#include <QtWidgets>
#include <QtSql>

typedef QList < QPair<QRegExp,QString> > ListRequete;

class CustomViewDialog : public QDialog
{
    Q_OBJECT
public:
    CustomViewDialog(QWidget *parent=0) ;
    ListRequete Requete ;

public slots:
    void addPattern() ;
    void validatePattern() ;

signals:
   // void clicked;


private:
    QPushButton     *   FinishedButton ;
    QPushButton     *   ValidateButton ;
    QPushButton     *   AddButton ;
    QLabel          *   PatternLocateLabel ;
    QLabel          *   SelectColorLabel ;
    QLineEdit       *   PatternLocateText ;
    QComboBox       *   SelectColorComboBox ;
    QVBoxLayout     *   ContenerLayout ;
    QGroupBox       *   PatternGroupBox ;

 };
#endif // CUSTOMVIEWDIALOG_H
