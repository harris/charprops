#ifndef WINDOWSOM_H
#define WINDOWSOM_H

#include <QtSql>
#include <QtWidgets>
#include "modelsom.h"
#include "customviewdialog.h"
#include "dialogngram.h"

class WindowSom : public QWidget
{
    Q_OBJECT

  public:
    WindowSom(QWidget *parent = 0);
    ~WindowSom();
    bool readFileLine(const QString & filename) ;
    ModelSom * Model;
    
  public slots:
    void    browseMatrixFile() ;
    void    loadMatrixSom() ;
    void    customViewCarte() ;
    void openDialogNgram(QModelIndex );

  private:
    void createMatrixView() ;
    float ngramContentRegExp(int, int, QRegExp);
    QString readFirstLines(QTextStream &) ;

    QLineEdit   * MatrixFile ;
    QTableView  * View ;
    QPushButton * BrowseButton ;
    QPushButton * LoadToView ;
    QPushButton * CustomView ;
    QLabel      * CarteParametres ;
};


class CustomDelegate : public QStyledItemDelegate
{ Q_OBJECT

  public:
    explicit CustomDelegate(int row, int col, QString color, float percolor ,QObject *parent = 0)
        : QStyledItemDelegate(parent), Row(row), Col(col), Color(color), PerColor(percolor) {

         qDebug() << "constructor :(" + QString::number(Row) + ") (" + QString::number(Col) + ")" ;
            }

    explicit CustomDelegate(QObject *parent = 0)
        : QStyledItemDelegate(parent) {}
     void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    
  private:
    int Row;
    int Col;
    QString Color ;
    float PerColor ;

};
#endif // WINDOWSOM_H
