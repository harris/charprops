#include "windowsom.h"

WindowSom::WindowSom(QWidget *parent):
QWidget(parent)
{
    Model = new ModelSom ;
    QGridLayout * mainlayout = new QGridLayout;
    mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));

//====================================CHOIX DE FICHIER CONTENANT LA MATRICE=======================
    mainlayout->addWidget(new QLabel("Matrix file:"), 0, 0) ;
    MatrixFile = new QLineEdit ;
    mainlayout->addWidget(MatrixFile, 0, 1) ;
    BrowseButton = new QPushButton("&Browse") ;
    connect(BrowseButton, SIGNAL(clicked()), this, SLOT(browseMatrixFile())) ;
    mainlayout->addWidget(BrowseButton, 0, 2) ;
//====================================== OPERATIONS SUR LA MATRICE================================
    QGroupBox * operationbox = new QGroupBox(tr("")) ;
    mainlayout->addWidget(operationbox, 1, 2) ;
    QVBoxLayout * operationlayout = new QVBoxLayout ;

    LoadToView = new QPushButton("&Load matrix") ;
    connect(LoadToView, SIGNAL(clicked()), this, SLOT(loadMatrixSom())) ;
    operationlayout->addWidget(LoadToView, 0) ;
    CustomView = new QPushButton("&Custom view") ;
    connect(CustomView, SIGNAL(clicked()), this, SLOT(customViewCarte())) ;
    operationlayout->addWidget(CustomView, 1) ;
    CarteParametres = new QLabel;
    operationlayout->addWidget(CarteParametres,2, Qt::AlignTop) ;
    operationbox->setLayout(operationlayout) ;
    operationbox->setAlignment(Qt::AlignTop) ;

//======================================PARTIE AFFICHAGE==========================================
   createMatrixView() ;
   connect(View, SIGNAL(clicked(QModelIndex)), this, SLOT(openDialogNgram(QModelIndex))) ;
   QGroupBox * viewbox = new QGroupBox(tr("")) ;
   mainlayout->addWidget(viewbox, 1, 0, 1, 2) ;
   QGridLayout * viewLayout = new QGridLayout ;
   viewLayout->addWidget(View, 0, 0) ;
   viewbox->setLayout(viewLayout) ;
   setLayout(mainlayout) ;
   setWindowTitle("SOM Visualisation") ;
   showMaximized() ;
}
// auxiliaire du constructeur
void WindowSom::createMatrixView()
{ View = new QTableView(this) ;
  View->verticalHeader()->setDefaultSectionSize(85);
  View->horizontalHeader()->setDefaultSectionSize(52);
  View->horizontalHeader()->hide();
  View->verticalHeader()->hide();
  View->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  View->setStyleSheet("QTableView::item{border : 0.5px ;}") ;
}

WindowSom::~WindowSom()
{}

// slot du bouton pour chercher le fichier
void WindowSom::browseMatrixFile()
{ QString file = QFileDialog::getOpenFileName(this, tr("Find Matrix File"), QDir::currentPath());
  if (not file.isEmpty())
  { MatrixFile->clear() ;
    MatrixFile->setText(file) ;
  }
}

// slot du bouton pour remplir les données de la carte
void WindowSom::loadMatrixSom()
{ if(MatrixFile->text() == NULL)
  { QMessageBox::information(this, tr("Matrix File"), tr("Download failed, please check the matrix file!")) ;
    MatrixFile->setFocus() ;
    return ;
  }
  CarteParametres->setText("") ;
  readFileLine(MatrixFile->text()) ;
  View->setModel(Model) ;
  //View->setShowGrid(false) ;
  int row = 0 ;
  int col = 0 ;

  while (row < Model->rowCount(QModelIndex()))
  { col =  Model->offset(row);
    while (col < Model->columnCount(QModelIndex()))
    { View->setSpan(row, col, 1, 2) ;
      col += 2 ; }
    row++ ;
  }
}

// utilité de renvoyer un booléen non utilisé ?
bool WindowSom::readFileLine(const QString & filename)
{ int i, j ;
  idd idn ;
  double dist ;
  QFile file(filename) ;
  QString word ;
  if (not file.open(QIODevice::ReadOnly | QIODevice::Text))
  { qDebug() << "impossible to open the file " << filename ;
    return false ; }
  QTextStream in(& file) ;
  while ((word = readFirstLines(in)).isEmpty()) ;
  // insérer ici la lecture des paramètres
  Model->RowCarte = word.toInt() ;
  in >> Model->ColCarte ;
  if (true) Model->ColCarte = Model->ColCarte * 2 + 1 ;     // si hexagonal
  Model->createCarte(Model->RowCarte, Model->ColCarte) ;    // récuperation des dimensions et création de la carte
  while (not in.atEnd())
  { in >> idn >> i >> j >> dist ;
    Model->CarteTopo[i][j * 2 + Model->offset(i)].insert(idn, dist) ;
  }
  file.close() ;
  return true ;
}

QString WindowSom::readFirstLines(QTextStream & flux)
{ QString word ;

  flux >> word ;
  if (word != "#") return word ;
  CarteParametres->setText(CarteParametres->text() + "\n" + flux.readLine() );
  //qDebug() << flux.readLine() ; // ici imprimer la ligne sur le coté de la fenêtre
  return QString() ;
}

// slot du bouton customview
void WindowSom::customViewCarte()
{ CustomViewDialog CustomDial ;
  if (not CustomDial.exec()) return ;
  int row = Model->rowCount(QModelIndex());
  int col = Model->columnCount(QModelIndex());
  QBrush yellow;
  yellow.setColor(Qt::red);
  int k = 0;
  while (k < CustomDial.Requete.count())
  { for (int i = 0 ; i < row; i++)
    { for (int j = 0 ; j < col ; j++)
      { QModelIndex index = Model->index(i, j) ;
        QMapIterator<idd, double> it(Model->CarteTopo[i][j]) ;
        while (it.hasNext())
        { it.next() ;
                  if (Model->sqlGetNgram(it.key()).contains(CustomDial.Requete.at(k).first.pattern()))
                  {
                      float percolor = ngramContentRegExp(index.row(),index.column(),CustomDial.Requete.at(k).first) ;
                    // ici je filtre les ngrammes répondent à la condition ( le filtre de la requête)
                    // une fois le comportement de la view est changé (ici setStyleSheet)
                    // elle actualise automatiquement l'affichage avec un appel à data de la classe modelsom
                      qDebug() << "(" + QString::number(index.row()) + ") (" + QString::number(index.column()) + ")" ;
                     // View->setCurrentIndex(index);
                      //CustomDelegate * custdlg = new CustomDelegate(index.row(),index.column(), CustomDial.Requete.at(k).second, percolor);
                      CustomDelegate * custdlg = new CustomDelegate(index.row(),index.column(), CustomDial.Requete.at(k).second, percolor,View);

                      View->setItemDelegate(custdlg);
                      View->repaint();
                  }
        }
      }
    }
    k++;
  }
}

// renvoie la proportion (entre 0 et 1) de ngrammes répondant à la requête
float WindowSom::ngramContentRegExp(int i, int j, QRegExp regex)
{ int cmpt = 0 ;
  QMapIterator<idd, double> it(Model->CarteTopo[i][j]) ;
  while (it.hasNext())
  { it.next() ;
    cmpt += (Model->sqlGetNgram(it.key())).contains(regex) ;
  }
  return (float(cmpt)/float(Model->CarteTopo[i][j].size())) ;
}

// slot du clic sur un index de la matrice
void WindowSom::openDialogNgram(QModelIndex index)
{ if (not index.isValid()) return ;
  DialogNgram dlgr(index.row(), index.column(), Model) ;
  if (not dlgr.exec()) return;
}

void CustomDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{ QString s = index.data().toString();
  QColor cl;
  cl.setNamedColor(Color) ;
  QStyleOptionViewItemV4 opt = option;
    initStyleOption(&opt, index);
    if (s.isEmpty() && s.isNull())  QStyledItemDelegate::paint(painter, option, index);

    if(index.row() == Row && index.column() == Col)
    {

      qDebug() << "paint : (" + QString::number(Row) + ") (" + QString::number(Col) + ")" ;

      painter->save();



      painter->setRenderHint(QPainter::Antialiasing , true);
      painter->setBrush(QBrush(cl));
      painter->setPen(QColor("black"));
      painter->drawRoundedRect(option.rect.x()+5, option.rect.y()+5, option.rect.width()-5, option.rect.height()-6,10,10);
      painter->drawText(option.rect.x()+2.5,option.rect.y()+2.5,option.rect.width(), option.rect.height(), Qt::AlignCenter, index.data().toString());
      painter->restore();
    }
    else { qDebug()<< "fin paint : (" + QString::number(index.row()) + ") (" + QString::number(index.column()) + ")" ;
      QStyledItemDelegate::paint(painter, option, index);
    }

}

