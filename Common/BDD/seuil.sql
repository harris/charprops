create or replace function seuil(integer) returns integer
  as
    $$
      update ngramme set stoplist = false ;
      update ngramme set stoplist = true where frequence <= $1 ;
      update matrice set stoplist = false ;
      update matrice as m 
        set stoplist = true
          from ngramme as i
            where ((i.id_ngram = m.fk_i and i.stoplist = true) or (i.id_ngram = m.fk_j and i.stoplist = true)) ;
      select count(ngramme.id) from ngramme where stoplist = false ;
   $$ language SQL ;