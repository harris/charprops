select count(m.id) / (select count(id)^2 from ngramme where frequence > 15)
 from matrice as m, ngramme as i
   where
    ((i.id_ngram = m.fk_i and i.frequence > 15)
     and (i.id_ngram = m.fk_j and i.frequence > 15))
