-- Table: language

-- DROP TABLE language;

CREATE TABLE language
(
  id serial NOT NULL,
  name text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE language
  OWNER TO "Charprops";
insert into language(name) values('French');
insert into language(name) values('English');
insert into language(name) values('Arabic');
