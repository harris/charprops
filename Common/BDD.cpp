#include "BDD.h"

using namespace std;

BDD::BDD(QString serveur, QString user, QString pwd , QString db): connexion(false)
{ setConnexion(serveur, user, pwd, db) ; // Connexion à la base en tant qu'administrateur
}

BDD::~BDD()
{ if (connexion) 
   { DB.close() ;
     qDebug() << "Connexion fermée" << endl ;
     QSqlDatabase::removeDatabase("QPSQL");
   }
}

void BDD::fermerConnexion()
{  if (connexion) 
   { DB.close() ;
     //QSqlDatabase::removeDatabase("QPSQL");
     qDebug() << "Connexion fermée" << endl ;
     connexion = false;
   }
}

void BDD::ouvrirConnexion()
{  if (DB.open())
      { qDebug() << "Vous êtes maintenant connecté à "<<DB.databaseName()<<"-->"<< DB.hostName() << endl ;
      connexion = true ; }
     else
     { qDebug() << "La connexion a échoué, désolé" << endl ;
       connexion = false ; }
}

void BDD::setConnexion ( QString serveur, QString user, QString pwd , QString nomBase)
{ if ( not connexion)
  { DB = QSqlDatabase::addDatabase("QPSQL") ;
    DB.setHostName(serveur) ;
    DB.setUserName(user) ;
    DB.setPassword(pwd) ;
    DB.setDatabaseName(nomBase) ;
    ouvrirConnexion();  
  }  
   else qDebug() << "Vous êtes déjà connecté à une basse de données! " ;
}

QMap<id, val> sqlDiagCells() ;
QMap<id, val> sqlColCells(id) ;

// inutilisé
bool BDD::execSQL(const QString &req)
{ QSqlQuery requete;
  if(not requete.exec(req)) qDebug() << requete.lastError().text() << requete.lastQuery() ;
  return (not requete.lastError().isValid()) ;
}
// inutilisé
QSqlQuery BDD::execSQL(const QString &req,const QVariantList &args)
{ QSqlQuery requete;
  requete.prepare(req);
  Q_ASSERT(requete.boundValues().size() == args.size());
  for(int i = 0; i < args.size(); ++i) requete.bindValue(i,args[i]);
  if (not requete.exec())
    qDebug() << requete.lastError().text()<< requete.lastQuery();
  return requete;
}

int BDD::sqlGetFrequence(id id_ngram)  // pour récupérer la fréquence d'un ngram donné
{ QSqlQuery reqn;
  int freq = -1;
  reqn.prepare("select frequence from ngramme where id_ngram = ?"); // pour récupérer la fréquence d'un ngram donné
  reqn.addBindValue(id_ngram);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return -1; }
   while(reqn.next()) freq = reqn.value(0).toLongLong();
  
  return freq;
}

QString BDD::sqlGetNgram(id id_ngram)  // pour récupérer nom d'un ngram étant donné son id_ngram
{ QSqlQuery reqn;
  QString gram ="";
  reqn.prepare("select ngram from ngramme where id_ngram = ?"); 
  reqn.addBindValue(id_ngram);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return ""; }
   while(reqn.next()) gram = reqn.value(0).toString();
  
  return gram;
}

id BDD::sqlGetIdNgram(QString gram)  // pour récupérer id_ngram d'un ngram donné
{ QSqlQuery reqn;
  id id_ngram = 0;
  reqn.prepare("select id_ngram from ngramme where ngram = ?"); 
  reqn.addBindValue(gram);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return 0; }
   while(reqn.next()) id_ngram = reqn.value(0).toLongLong();
  return id_ngram;
}

int BDD::sqlGetIdCorpus(QString corpus)  // pour récupérer le id_corpus d'un corpus donné
{ QSqlQuery reqn;
  int id_cops = 0;
  reqn.prepare("select id_corpus from corpus where nom_corpus = ?");
  reqn.addBindValue(corpus);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return 0; }
   while(reqn.next()) id_cops = reqn.value(0).toLongLong();
  
  return id_cops;
}

QString BDD::sqlGetNomCorpus(int id_cops)  // pour récupérer nom d'un corpus étant donné son id_corpus
{ QSqlQuery reqn;
  QString cops = "";
  reqn.prepare("select nom_corpus from corpus where id_corpus = ?");
  reqn.addBindValue(id_cops);			   		     
  if(not reqn.exec())
  { qDebug() << reqn.lastError().text()<< endl;
    return ""; }
   while(reqn.next()) cops = reqn.value(0).toString();
  
  return cops;
}

bool BDD::sqlGetDataMatrix() 
{ QSqlQuery reqm ;
  id ligne = 0 ; 	    // ligne corresponde à id_ngram 
  id col ;
  double valeur; 
  if(not reqm.exec("select fk_i,fk_j,val from matrice where stoplist = 'f' order by fk_i,fk_j")) // On récupère i, j, valeur 
  { qDebug() << reqm.lastError().text()<< endl;
    return false; }
  qDebug() << " construction de la matrice en cours"<<endl;
  while(reqm.next())
  { if (ligne != reqm.value(0).toLongLong()) // if condition, On passe à ligne suivante dans la DataMatrix
     ligne = reqm.value(0).toLongLong() ;
   col = reqm.value(1).toLongLong() ;
   valeur =(double)reqm.value(2).toLongLong() ;
   
   DataMatrix[ligne].insert(col, valeur);
  }
  return true;
}

maLigne BDD::sqlGetLigne(id id_ngram)
{ return DataMatrix[id_ngram] ;
}

maLigne BDD::sqlGetDiagonal()
{ maLigne diago ;
  QMapIterator<id, maLigne > i(DataMatrix);
    while (i.hasNext())
    {i.next();
      QMapIterator<id, double > it(i.value());
      while ( it.hasNext())
      {it.next();
        if (i.key() == it.key())
        { diago.insert(it.key(), it.value());
          break ; }
      }
     }
   return diago ;
}

