#ifndef TYPES_H
#define TYPES_H

/* Types identifiant et nombre d'occurrences

Le passage par la base de données rend compliqué l'utilisation d'unsigned à cause des cast

  id = l'identifiant généré par programme ou par base de données
  Les identifiants peuvent aller jusqu'à 500 000 avec CorpusChar
  Ils peuvent atteindre jusqu'à 1 000 000 avec CorpusWord
  La valeur stockable en base de données : 2 147 483 647
  
  val = les valeurs prises par les nombres d'occurrences ; générées par programme
  Les valeurs peuvent aller jusqu'à 500 000 avec CorpusChar
  Elles sont probablement semblables avec CorpusWord

Le type qint32 permet d'assurer la compatibilité totale, quelle que soit la plateforme, avec postgresql integer

*/

typedef qint32 id ;
typedef qint32 val ;

#endif // TYPES_H