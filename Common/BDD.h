#ifndef BDD_H
#define BDD_H

#include <QtSql>

#include "Types.h"

typedef QMap<id, double> maLigne ;
typedef QMap<id, maLigne> maMatrice ;



class BDD
{ bool connexion ;
  QSqlDatabase DB ;

/************************ Pour la matrice de données *************************************/

  
// utiliser QMap
  maMatrice DataMatrix ; // la matrice de données

/********************************************************************************************/
public:
  BDD(QString serveur, QString user, QString pwd, QString db) ;
  BDD(): connexion(false) {}
  ~BDD() ;
  
  bool      getConnexion() { return connexion; }
  void      setConnexion(QString serveur, QString user, QString pwd, QString db);
  void      fermerConnexion();
  void      ouvrirConnexion();
  maMatrice   getDataMatrix() { return DataMatrix; }

  bool sqlGetDataMatrix();     // Pour générer la matrice de données à partir de la table matrice,
                              // return true si tout va bien.
  
  int sqlGetFrequence(id id_ngram);  		// pour récupérer la fréquence d'un ngram donné
  QString sqlGetNgram(id id_ngram); 	 	// pour récupérer le nom d'un ngram étant donné son idt
  id sqlGetIdNgram(QString gram); 	 	// pour récupérer le id d'un ngram donné
  int sqlGetIdCorpus(QString corpus); 		// pour récupérer le id d'un corpus donné
  QString sqlGetNomCorpus(int id_cops); 	 // pour récupérer le nom d'un corpus étant donné son idt
  
  
  
  QMap<id, val> sqlDiagCells() ;
  QMap<id, val> sqlColCells(id) ;

  maLigne sqlGetLigne(id);
  maLigne sqlGetDiagonal();

  
  
  // tous inutilisés :
  bool      execSQL(const QString &) ;
  QSqlQuery execSQL(const QString &, const QVariantList &) ;
    
private:
  
};


#endif // BDD_H
