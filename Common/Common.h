#ifndef COMMON_H
#define COMMON_H

#include <math.h>
 
inline const char * readabletime(qint64 time)
{ int hours, minutes, seconds ;
  hours = floor(time / (qint64) 3600000) ;
  minutes = floor(time / (qint64) 60000) - hours * 60 ;
  seconds = floor(time / (qint64) 1000) - minutes * 60 - hours * 3600 ;
  time -= (qint64) (hours * 3600000 + minutes * 60000 + seconds * 1000) ;
  return qPrintable(QString("%1h %2mn %3s %4ms").arg(hours).arg(minutes).arg(seconds).arg(time)) ;
}




#endif // COMMON_H