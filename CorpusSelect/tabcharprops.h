#ifndef TABCHARPROPS_H
#define TABCHARPROPS_H

#define QT5
#include <QtWidgets>
#include "window.h"
//#include "../ngrammes/windowngrams.h"

class TabCharprops : public QWidget
{ Q_OBJECT

public:
   TabCharprops(QWidget * parent = 0);

  // WindowNgrams *TabWindowNgrams;

   QTabWidget   *   TabWidgetCharprops;

  // QString *CorpusAnalysis;

   QPushButton  *   Quit;
   QLabel       *   MessageTab;
};

#endif // TABCHARPROPS_H
