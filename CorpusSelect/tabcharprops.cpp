#include "tabcharprops.h"


TabCharprops::TabCharprops(QWidget *parent) : QWidget(parent)
{
    //================================================= CONSTRUCTEUR DES TABs

        QGridLayout * layout = new QGridLayout;

        TabWidgetCharprops = new QTabWidget;
        QString * CorpusHandle = new QString(tr("Corpus Constitution"));
        TabWidgetCharprops->addTab(new Window(this) , * CorpusHandle);
        layout->addWidget(TabWidgetCharprops, 0, 0);

        // CorpusAnalysis = new QString(tr("Corpus Analysis"));
        //TabWindowNgrams = new WindowNgrams(this);
       // TabWidgetCharprops->addTab(TabWindowNgrams , * CorpusAnalysis);

        QGroupBox   * qGroupbox = new QGroupBox(tr(""));
        layout->addWidget(qGroupbox, 1, 0);
        QGridLayout * quitLayout = new QGridLayout;

        MessageTab = new QLabel;
        quitLayout->addWidget(MessageTab, 0, 0,1,4);


        Quit = new QPushButton("&Quit");
        //Quit->setFixedWidth(80);
        connect(Quit, SIGNAL(clicked()), this, SLOT(close()));
        quitLayout->addWidget(Quit, 0, 5);
        qGroupbox->setLayout(quitLayout);

        setLayout(layout);
}
