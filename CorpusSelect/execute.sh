
if [ ! -d build ]; then
 mkdir build
fi
cd build
if [ ! -d temp ]; then
 mkdir temp
fi

cmake .. && make

