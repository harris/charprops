#ifndef CONNECTIONDIALOG_H
#define CONNECTIONDIALOG_H

#define QT5

#include <QtWidgets>

class ConnectionDialog : public QDialog
{
    Q_OBJECT
public :
    ConnectionDialog(QWidget *parent = 0);
public slots :
    void            connectToDb();

protected :
    QLabel      *   ConnectionLabel;
    QLabel      *   DataBaseNameLabel;
    QLabel      *   UserNameLabel;
    QLabel      *   PassWordLabel;

    QComboBox   *   DataBaseComboBox;
    QLineEdit   *   UserNameText;
    QLineEdit   *   PassWordText;

    QPushButton *   ConnectButton;
    QPushButton *   CancelButton;

};

#endif // CONNECTIONDIALOG_H

// progressDialog
//no cursos
