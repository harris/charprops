#include "connectiondialog.h"
//#include "../ngrammes/Ngrams.h"


ConnectionDialog::ConnectionDialog(QWidget *parent) : QDialog(parent)
{
    QGridLayout * layout = new QGridLayout;

    ConnectionLabel = new QLabel(tr("Please ...")); // info et affichage les erreurs
    layout->addWidget(ConnectionLabel,0,0);

    DataBaseNameLabel = new QLabel(tr("DataBase:"));
    DataBaseComboBox = new QComboBox;
    layout->addWidget(DataBaseNameLabel, 1,0);
    layout->addWidget(DataBaseComboBox,1,1);

    UserNameLabel = new QLabel(tr("User name:"));
    UserNameText  = new QLineEdit;
    layout->addWidget(UserNameLabel,2,0);
    layout->addWidget(UserNameText,2,1);


    PassWordLabel = new QLabel(tr("Password:"));
    PassWordText = new QLineEdit;
    PassWordText->setEchoMode(QLineEdit::Password);


    layout->addWidget(PassWordLabel,3,0);
    layout->addWidget(PassWordText);

    ConnectButton = new QPushButton(tr("Connect"));
    connect(ConnectButton, SIGNAL(clicked()), this, SLOT(connectToDb())) ;
    layout->addWidget(ConnectButton,4,0);


    CancelButton = new QPushButton(tr("Cancel"));
    connect(CancelButton, SIGNAL(clicked()), this, SLOT(close())) ;
    layout->addWidget(CancelButton,4,1);

    setLayout(layout);
    setWindowTitle(tr("DataBase connection"));

}
void ConnectionDialog::connectToDb()
{
   /* BDD bdd("localhost", "Charprops", "charprops", "CorpusDB");
      if (bdd.getConnexion()){
        // inserer dans la table corpus
      }
      return 0 ;
    */
       //   quand je valide      connexion.pg_connexion(,,,);
}
