var searchData=
[
  ['m_5fid',['m_id',['../structnode.html#a458fc6f80b6e213e50c395a617464039',1,'node']]],
  ['matrixngramsbutton',['MatrixNgramsButton',['../class_window_ngrams.html#a72296ecf37d2fdf71727555332a90203',1,'WindowNgrams']]],
  ['matrixsparsity',['MatrixSparsity',['../class_window_ngrams.html#a92dfa71a2e368621b0b05f57e5126315',1,'WindowNgrams']]],
  ['matrixthresholdbutton',['MatrixThresholdButton',['../class_window_ngrams.html#a23d3e5b55e72744b7d726efb3b92ec4c',1,'WindowNgrams']]],
  ['maxmarkers',['MaxMarkers',['../class_window_word.html#a3229d631fbf162bf7203f003334a9dc5',1,'WindowWord']]],
  ['maxoccurrences',['MaxOccurrences',['../class_window_word.html#a752687c7d826e5734f2e7b915129f5a4',1,'WindowWord']]],
  ['messagetab',['MessageTab',['../class_tab_charprops.html#a4bbab3ffc9dbf0e72bcb4ca9cfb3cc0c',1,'TabCharprops']]],
  ['mle',['mle',['../structnode.html#a98d6b84a6fba1e1ce9ad359cd1aa1082',1,'node']]],
  ['model',['Model',['../class_dialog_ngram.html#a1051dadb9f1953e7da4fe8a57467b4c9',1,'DialogNgram::Model()'],['../class_window_som.html#a349cdc9604c0570d8a7c6fa5d6287923',1,'WindowSom::Model()']]]
];
