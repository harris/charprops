var searchData=
[
  ['next',['next',['../struct_conversion.html#a07c36e2aac014eb6f2c5a745a866d934',1,'Conversion']]],
  ['ngram',['Ngram',['../class_window_ngrams.html#a6f8fb00b0a0ba2be001af13516b96e30',1,'WindowNgrams']]],
  ['ngramentropy',['NgramEntropy',['../class_window_ngrams.html#ac03a36b3fda304a6700adbc837c7f9d5',1,'WindowNgrams']]],
  ['ngramnumber',['NgramNumber',['../class_window_ngrams.html#a6abbd77bef377872adf4b60c04028b07',1,'WindowNgrams']]],
  ['ngramstable',['NgramsTable',['../class_window_ngrams.html#a5faeaa0610eeb6f44a1e151b2eb20161',1,'WindowNgrams']]],
  ['numberfiles',['NumberFiles',['../class_window_ngrams.html#a1b69bb772eac7aa8f9e43dfeeb8d53eb',1,'WindowNgrams::NumberFiles()'],['../class_window_word.html#aeb5af1b1f8346e1e03be0e00b396aae0',1,'WindowWord::NumberFiles()']]]
];
