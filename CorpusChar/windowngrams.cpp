/* ============================ CHARPROPS ===============================
 * 
 CharProps 0.1a is a program designed for those who want to explore grapheme properties 
 on the basis of raw Unicode text data. It does some other things too.
 
 Copyright (C) 2015  Bernard (Gilles), Manad (Otman), Aliane (Nourredine)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 =========================================================================== */

#include "windowngrams.h"


// Il faudrait stocker toutes les requêtes dans un fichier à part et les sélectionner dans l'ordre
// ou alors en faire des procédures stockées
// ou les deux, le premier pour la portabilité, le deuxième pour la performance


WindowNgrams::WindowNgrams(QWidget *parent)
 : QWidget(parent), Threshold(0)
{ Bdd.setConnexion("localhost", "Charprops", "charprops", "CorpusDB") ;
  
  //==============================================================FENETRAGE
  QGridLayout * mainlayout = new QGridLayout;
  mainlayout->setSizeConstraint((QLayout::SetMinAndMaxSize));

  //================================ PARTIE CORPUS===========================

  corpusBoxes(mainlayout) ;

  //=========================é================= PARTIE NGRAMS ==============================

  // la grande boite Ngram
  QGroupBox * ngrambigbox = new QGroupBox(tr("Ngrams analysis")) ;
  mainlayout->addWidget(ngrambigbox, 0, 1) ;
  QVBoxLayout * ngrambiglayout = new QVBoxLayout ;
  ngrambigbox->setAlignment(Qt::AlignTop) ;
  ngrambigbox->setLayout(ngrambiglayout) ;
  
  // boite paramètres
  QGroupBox * ngramparambox = new QGroupBox(tr("")) ;
  ngrambiglayout->addWidget(ngramparambox, 0) ;
  QGridLayout * ngramparamlayout = new QGridLayout ;
  ngramparambox->setAlignment(Qt::AlignTop) ;
  ngramparambox->setLayout(ngramparamlayout) ;
  
  ngramparamlayout->addWidget(new QLabel(tr("Ngram size")), 0, 0);
  TailleNgrams = new QSpinBox ;
  TailleNgrams->setRange(1, 5) ;
  TailleNgrams->setValue(5) ;
  ngramparamlayout->addWidget(TailleNgrams, 0, 1);
  
  ngramparamlayout->addWidget(new QLabel(tr("Context radius")), 1, 0) ;
  ContexteNgrams = new QSpinBox ;
  ContexteNgrams->setRange(1, 1) ;
  ContexteNgrams->setValue(1) ;
  ngramparamlayout->addWidget(ContexteNgrams, 1, 1);
  
  TreeConstitutionButton = new QPushButton(tr("Ngram constitution")) ;
  TreeConstitutionButton->setEnabled(false);
  connect(TreeConstitutionButton, SIGNAL(clicked()), this, SLOT(showNgrams()));
  ngramparamlayout->addWidget(TreeConstitutionButton, 2, 0, 1, -1);

  QGroupBox * ngramresultbox = new QGroupBox(tr("Ngram results")) ;
  ngrambiglayout->addWidget(ngramresultbox, 1) ;
  QGridLayout * ngramresultlayout= new QGridLayout ;
  ngramresultbox->setAlignment(Qt::AlignTop);
  ngramresultbox->setLayout(ngramresultlayout);
  
  NgramNumber = createLabel() ;
  ngramresultlayout->addWidget(new QLabel(tr("Ngram number")), 0, 0);
  ngramresultlayout->addWidget(NgramNumber, 0, 1) ;
 
  UnigramNumber = createLabel() ;
  ngramresultlayout->addWidget(new QLabel("Character number"), 1, 0);
  ngramresultlayout->addWidget(UnigramNumber, 1, 1);

  UnigramLowestFreq = createLabel() ;
  ngramresultlayout->addWidget(new QLabel("Unigram lowest frequency"), 2, 0);
  ngramresultlayout->addWidget(UnigramLowestFreq, 2, 1);
  
  NgramEntropy = createLabel() ;
  ngramresultlayout->addWidget(new QLabel("Corpus entropy"), 3, 0);
  ngramresultlayout->addWidget(NgramEntropy, 3, 1);

  createNgramsTable() ;
  ngrambiglayout->addWidget(NgramsTable, 3) ;
   
  //=========================é================= PARTIE CONTEXTE ==============================

  QGroupBox * contextbigbox = new QGroupBox(tr("Context analysis")) ;// groupBox1
  mainlayout->addWidget(contextbigbox, 0, 2) ;
  QVBoxLayout * contextbiglayout= new QVBoxLayout ;
  contextbigbox->setLayout(contextbiglayout) ;
  
  MatrixNgramsButton = new QPushButton("Matrix constitution") ;
  MatrixNgramsButton->setEnabled(false) ;
  connect(MatrixNgramsButton, SIGNAL(clicked()), this, SLOT(constructMatrix())) ;
  contextbiglayout->addWidget(MatrixNgramsButton, 0) ;
    
  // boite seuil / nombre
  QGroupBox * thresholdbox = new QGroupBox(tr("")) ;
  contextbiglayout->addWidget(thresholdbox, 1) ;
  QGridLayout * thresholdlayout = new QGridLayout ;
  thresholdbox->setLayout(thresholdlayout) ;
 
  thresholdlayout->addWidget(new QLabel("Ngram threshold"), 0, 0) ;
  ThresholdBox = new QLineEdit ;
  ThresholdBox->setValidator(new QIntValidator()) ;
  ThresholdBox->setReadOnly(true) ;
  connect(ThresholdBox, SIGNAL(editingFinished()), this, SLOT(thresholdEntered())) ;
  thresholdlayout->addWidget(ThresholdBox, 0, 1) ;
 
  thresholdlayout->addWidget(new QLabel("Number of kept ngrams"), 1, 0);
  AcceptedBox = new QLineEdit ;
  AcceptedBox->setValidator(new QIntValidator()) ;  
  AcceptedBox->setReadOnly(true) ;
  connect(AcceptedBox, SIGNAL(editingFinished()), this, SLOT(acceptedNumberEntered())) ;
  thresholdlayout->addWidget(AcceptedBox, 1, 1) ;
  
  MatrixThresholdButton = new QPushButton("Validate this threshold for later analysis") ;
  MatrixThresholdButton->setEnabled(false) ;
  connect(MatrixThresholdButton, SIGNAL(clicked()), this, SLOT(thresholdSelected())) ;
  thresholdlayout->addWidget(MatrixThresholdButton, 2, 0, 1, -1) ;
  
  QGroupBox * contextresultsbox = new QGroupBox(tr("Context results")) ;
  contextbiglayout->addWidget(contextresultsbox, 2) ;
  QGridLayout * contextresultslayout = new QGridLayout ;
  contextresultsbox->setLayout(contextresultslayout) ;
  
  MatrixSparsity = createLabel() ;
  contextresultslayout->addWidget(new QLabel("Original matrix sparsity"), 0, 0) ;
  contextresultslayout->addWidget(MatrixSparsity, 0, 1) ;
  
  ThresholdedMatrixSparsity = createLabel() ;
  contextresultslayout->addWidget(new QLabel("Thresholded matrix sparsity"), 1, 0) ;
  contextresultslayout->addWidget(ThresholdedMatrixSparsity, 1, 1) ;
  
 //contextresultsbox->setStyleSheet ("border: 2px solid ;");

  setLayout(mainlayout);
  setWindowTitle("Corpus Analysis");
}
//==========================================================AUXILIAIRES DU CONSTRUCTEUR

QLabel * WindowNgrams::createLabel()
{ QLabel * label = new QLabel ;
  label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred) ;
  label->setWordWrap(true);
  return label ;
}

void WindowNgrams::createNgramsTable()
{ NgramsTable = new QTableWidget(0, 2) ;
  NgramsTable->setSelectionBehavior(QAbstractItemView::SelectRows) ;
  NgramsTable->setHorizontalHeaderLabels(QStringList() << tr("Ngram") << tr("Frequency")) ;
  NgramsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch) ;
  
  NgramsTable->verticalHeader()->hide() ;
  NgramsTable->setShowGrid(false) ;
  NgramsTable->setContextMenuPolicy(Qt::CustomContextMenu);
  //connect(NgramsTable, SIGNAL(), this, SLOT()) ;
}


//======================================================== SLOTs Ngrams ========================================

/*
Ici il faudra vérifier : si c'est le même corpus avec les mêmes paramètres, il est inutile de recommencer
Et toutes les requêtes select doivent différencier les ngrammes de cet essai (param_ngramme = trie) des autres, en testant la valeur id_trie
Particulièrement attention à la matrice.

*/
// Slot Ngram constitution
void WindowNgrams::showNgrams()
{ NgramsTable->setRowCount(0);
  Time.start() ;
  TreeConstitutionButton->setEnabled(false) ;
  
  Bdd.ouvrirConnexion() ;  
  int idparam = sqlWriteTrie() ;
  int taille_ngram = TailleNgrams->value() ;

  // à noter que la requête ne marche qu'une fois ; si on ne vide pas la table, on ne peut pas traiter le même corpus
  // par contre, un corpus qui n'a pas le même contenu, ça marche
  // mais pas pour un texte presque identique
  
  Ngram = new Ngrams(IdCorpus, taille_ngram, ContexteNgrams->value(), idparam,  Time) ;
  NgramNumber->setNum(Ngram->getCountNgrams()) ;
  UnigramLowestFreq->setNum(sqlGetUnigramLowestFreq()) ;
  UnigramNumber->setNum(sqlGetUnigramNumber()) ;
  
  Bdd.fermerConnexion() ;
/*  Inhibé car c'est trop long avec un corpus important (même Copiale)
 * 
 * Implémenter ça avec QTableView et QAbstractTableModel
 * Il faut redéfinir les fonctions headerData et data et d'autres ; voici une suggestion dans un autre langage :
 * 
 * This will usually work like this:
 * 
 * local data = { ... some large 2d table }
 * 
 * local TableView = QTableView.new()
 * local DataModel = QAbstractTableModel.new(TableView)
 * 
 * function DataModel:rowCount() return 42 end
 * function DataModel:columnCound() return 3 end
 * local nothing = QVariant()
 * function DataModel:data(index, role)
 *    if role == 0 then -- DisplayRole
 *        local row = index:row()
 *        local col = index:column()
 *        return data[row+1][col+1] -- get the data from your Lua table
 *    end
 * end
 * 
 * TableView:setModel(DataModel)
 * TableView:show()
 * 
  QSqlQuery req;
  req.exec(QString("select ngram, frequence from ngramme, trie where size_ngram = %1 and id_trie = %2 order by frequence DESC").arg(taille_ngram).arg(idparam)) ;
  while(req.next())
  { QTableWidgetItem * ngram = new QTableWidgetItem(req.value(0).toString()) ;
    ngram->setFlags(ngram->flags() ^ Qt::ItemIsEditable) ;

    QTableWidgetItem * frequency = new QTableWidgetItem((req.value(1)).toString()) ;
    frequency->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    frequency->setFlags(frequency->flags() ^ Qt::ItemIsEditable);

    int row = NgramsTable->rowCount();
    NgramsTable->insertRow(row);
    NgramsTable->setItem(row, 0, ngram);
    NgramsTable->setItem(row, 1, frequency);
  }
*/

  MatrixNgramsButton->setEnabled(true);
}

// appelé par showNgrams
int WindowNgrams::sqlGetUnigramLowestFreq()
{ QSqlQuery req ;
  req.exec("select frequence from ngramme where size_ngram = 1 order by frequence asc limit 1") ;
  if (req.next()) return req.value(0).toInt() ;
  return 0 ;
}

// appelé par showNgrams
int WindowNgrams::sqlGetUnigramNumber()
{ QSqlQuery req ;
  req.exec("select count(ngram) from ngramme where size_ngram = 1") ;
  if (req.next()) return req.value(0).toInt() ;
  return 0 ;
}

// appelé par showNgrams
// insertion des paramètres Trie dans la base et récupération de l'identifiant pour le passer au constructeur
int WindowNgrams::sqlWriteTrie()
{ QSqlQuery req ;
  req.exec(QString("insert into trie (taille_max, taille_contexte) values ('%1', '%2')").arg(TailleNgrams->value()).arg(ContexteNgrams->value())) ;
  return req.lastInsertId().toInt() ;
}

//======================================================== SLOTs Contexte =========================================

// slot de MatrixNgramsButton
void WindowNgrams::constructMatrix()
{ MatrixNgramsButton->setEnabled(false) ;
  Bdd.ouvrirConnexion() ;
  qDebug() << "Début de la construction de la matrice (+ remplissage table matrice) : " << readabletime(Time.elapsed()) ;
  Ngram->sqlWriteMatrix() ;
  qDebug() << "Fin de la construction de la matrice (+ remplissage table matrice) : " << readabletime(Time.elapsed()) ;
  QSqlQuery req ;
  req.exec("select count(val) from matrice") ;
  if (req.next()) MatrixSparsity->setText(QString::number((double) req.value(0).toInt() / pow((double) Ngram->getCountNgrams(), 2), 'f', 4)) ;
  Bdd.fermerConnexion() ;
  ThresholdBox->setReadOnly(false) ;
  AcceptedBox->setReadOnly(false) ;
  MatrixThresholdButton->setEnabled(true) ;
}

// Slot du ThresholdBox
void WindowNgrams::thresholdEntered()
{ Bdd.ouvrirConnexion() ;
  QSqlQuery req ;
  req.exec(QString("select count(ngram) from ngramme where frequence > %1").arg(ThresholdBox->text().toInt())) ;
  if (req.next())
  { AcceptedBox->setText(req.value(0).toString()) ;
    Threshold = ThresholdBox->text().toInt() ;
    KeepNumber = req.value(0).toInt() ; }
  Bdd.fermerConnexion() ;
}

// slot de AcceptedBox
// the complicated end is not necessary, but less puzzling for the user
void WindowNgrams::acceptedNumberEntered()
{ Bdd.ouvrirConnexion() ;
  QSqlQuery req ;
  int n ;
  req.exec(QString("select count(rang) from (select rank() over (order by frequence DESC) as rang from ngramme) as foo where rang <= %1").arg(AcceptedBox->text().toInt())) ;
  if (not req.next()) return ;
  n = req.value(0).toInt() ;
  AcceptedBox->setText(req.value(0).toString()) ;
  KeepNumber = n ;
  req.clear() ;
  req.exec(QString("select frequence from (select row_number() over (order by frequence DESC) as rownumber, frequence from ngramme) as foo where rownumber = %1 or rownumber = %2").arg(n).arg(n + 1)) ;
  if (not req.next()) return ;
  n = req.value(0).toInt() ;              // n est la fréquence minimale au dessus du seuil ; la suivante est celle d'en dessous
  if (not req.next()) return ;
  if (Threshold < n and Threshold >= req.value(0).toInt()) return ;
  Threshold = req.value(0).toInt() ;
  Bdd.fermerConnexion() ;
  ThresholdBox->setText(QString::number(Threshold)) ;
}

// slot du bouton MatrixThresholdButton
// si l'utilisateur n'est pas passé par seuil ou nombre, on met seuil à 0 et nombre à la totalité
void WindowNgrams::thresholdSelected()
{ if (not Threshold)
  { ThresholdBox->setText("0") ;
    KeepNumber = Ngram->getCountNgrams() ;
    AcceptedBox->setText(QString::number(KeepNumber)) ; }
  ThresholdBox->setReadOnly(true) ;
  AcceptedBox->setReadOnly(true) ;
  MatrixThresholdButton->setEnabled(false) ;
  Bdd.ouvrirConnexion() ;
  QSqlQuery req ;
  int param_id ;
  req.exec(QString("insert into param_matrice (seuil, nombre_retenu) values('%1', '%2')").arg(Threshold).arg(KeepNumber)) ;
  param_id = req.lastInsertId().toInt() ;
  req.clear() ;
  qDebug() << "Update ngramme : " << readabletime(Time.elapsed()) ;
  req.exec(QString("update ngramme set stoplist = true where frequence <= %1").arg(Threshold)) ;
  req.clear() ;
  qDebug() << "Début update matrice : " << readabletime(Time.elapsed()) ;
  req.exec(QString("update matrice set id_param_matrix = %1 where id_param_matrix is null").arg(param_id)) ;
  req.clear() ;
  req.exec("update matrice as m set stoplist = true from ngramme as i where ((i.id_ngram = m.fk_i and i.stoplist = true) or (i.id_ngram = m.fk_j and i.stoplist = true))") ;
  qDebug() << "Fin update matrice : " << readabletime(Time.elapsed()) ;
  sqlThresholdedSparsity() ;
  Bdd.fermerConnexion() ;
}

// attention si totalcomponents et fullcomponents n'ont pas la valeur attendue
void WindowNgrams::sqlThresholdedSparsity()
{ QSqlQuery req ;
  double fullcomponents, totalcomponents ;
  req.exec("select count(val) from matrice where stoplist = false") ;
  if (req.next()) fullcomponents = (double) req.value(0).toInt() ;
  else fullcomponents = 0 ;
  req.clear() ;
  req.exec("select count(ngram) from ngramme where stoplist = false") ;
  if (req.next()) totalcomponents = pow((double) req.value(0).toInt(), 2) ;
  else totalcomponents = 0 ;
  if (not fullcomponents or not totalcomponents) ThresholdedMatrixSparsity->setText(tr("Error")) ;
  else ThresholdedMatrixSparsity->setText(QString::number(fullcomponents / totalcomponents, 'f', 2)) ;
}
