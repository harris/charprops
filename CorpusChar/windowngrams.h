/* ============================ CHARPROPS ===============================
 * 
 CharProps 0.1a is a program designed for those who want to explore grapheme properties 
 on the basis of raw Unicode text data. It does some other things too.
 
 Copyright (C) 2015  Bernard (Gilles), Manad (Otman), Aliane (Nourredine)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 =========================================================================== */

#ifndef WINDOWNGRAMS_H
#define WINDOWNGRAMS_H
#include "Ngrams.h"

#include <QtSql>

//================================ COMPOSANTS DE LA FENETRE

class WindowNgrams : public QWidget
{  Q_OBJECT

  public:
    WindowNgrams(QWidget  * parent = 0) ;
    
    BDD                   Bdd ;
    int                   KeepNumber ;
    int                   Threshold ;
    Ngrams             *  Ngram ;
    QElapsedTimer         Time ;

  protected slots:

    void                showCorpus() ;
    void                constructMatrix() ;
    void                showNgrams() ;
    void                thresholdEntered() ;
    void                acceptedNumberEntered() ;
    void                thresholdSelected() ;
    
  protected:

    // Partie Corpus
    void                corpusBoxes(QGridLayout *) ;
    void                 createUrlTable() ;
    QComboBox          * CorpusBox ;
    QPushButton        * LoadCorpusButton ;
    QComboBox          * sqlGetCorpusBox() ;
    QStandardItemModel * CorpusModel ;
    QTableView         * CorpusView ;
    QLabel             * createLabel() ;
    int                  IdCorpus ;
    
    QLabel             * ConstDate ;
    QLabel             * NumberFiles ;
    QLabel             * CorpusSize ;
    QLabel             * CorpusEncoding ;
    QLabel             * CorpusFormats ;
    
    QString             sqlGetFormats() ;
    QString             sqlGetEncoding() ;
    
    //Partie Trie
    QSpinBox          * TailleNgrams ;
    QSpinBox          * ContexteNgrams ;
    QPushButton       * TreeConstitutionButton ;
    int                 sqlWriteTrie() ;
    
    QLabel            * NgramNumber ;
    QLabel            * UnigramNumber ;
    int                 sqlGetUnigramNumber() ;
    QLabel            * UnigramLowestFreq ;
    int                 sqlGetUnigramLowestFreq() ;
    QLabel            * NgramEntropy ;
    
    QTableWidget      * NgramsTable ;
    void                createNgramsTable() ;
    
    // Partie Matrice
    QPushButton       * MatrixNgramsButton ;
    QLineEdit          * AcceptedBox ;
    QLineEdit          * ThresholdBox ;
    QPushButton       * MatrixThresholdButton ;
   
    QLabel            * MatrixSparsity ;
    int                 sqlWriteParaMatrix() ;
    QLabel            * ThresholdedMatrixSparsity ;
    void                sqlThresholdedSparsity() ;
    

} ;

#endif // WINDOWNGRAMS_H
