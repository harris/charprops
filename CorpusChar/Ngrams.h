/* ============================ CHARPROPS ===============================
 * 
 CharProps 0.1a is a program designed for those who want to explore grapheme properties 
 on the basis of raw Unicode text data. It does some other things too.
 
 Copyright (C) 2015  Bernard (Gilles), Manad (Otman), Aliane (Nourredine)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 =========================================================================== */

#ifndef NGRAMS_H
#define NGRAMS_H

#include <QtWidgets>

#include "../Common/Types.h"
#include "../Common/BDD.h"
#include "../Common/Common.h"

// l'élément de l'arbre préfixe
struct node
{ QChar letter ;
  QVector<id> identifier ;     // un tableau d'identifiants de chaque ngramme dont ce noeud contient le dernier caractère
  bool terminal ;               // pour les ngraphes, tous les ngrammes ne seront pas des ngraphes
  val occurrences ;
  double mle ;                  // Maximum Likelihood Estimation de la probabilité conditionnelle : p(nodei|node1... nodei-1)
  double proba1 ;               // méthode 1 (dénominateur = nombre de ngrammes
  double proba2 ;               // méthode 2 (dénominateur = nombre de caractères (longueur du texte)
  /* autre information utile ici */
  node * sons ;
  node * bros ;
} ;

typedef node * arbre ;

class Ngrams
{  
  arbre Trie ;
  int MaxNgramLength ;
  int Voisinage ;             // nombre de ngrammes dans le contexte à prendre en compte
  int ContextualLength ;      // taille du contexte
  int CountNgrams ;           // nombre total de ngrammes
  
  public:

    Ngrams(int, int, int, int, QElapsedTimer) ;
    ~Ngrams();
   
    arbre getTrie() { return Trie; }
    int getMaxNgramLength() { return MaxNgramLength ; }
    int getContextualLength() { return ContextualLength ; }
    int getCountNgrams() { return CountNgrams ; }
    
    // Création
    void insert(QStringRef, arbre, int = 0, int = 0) ;

    // Recherche
    bool research(QStringRef, arbre, int = 0) ;
          
    void calculate_mle(arbre, val = 0) ;
    void calculate_proba(arbre, val = 0, short = 0) ;
    void sqlWriteData(int) ;
    void sqlWriteMatrix();
    
    // Exportation dans un fichier
    void printArbre(arbre, QTextStream &, int = 0) ;
    
    // Impression ngrammes
    void printNgrams(arbre, QTextStream &, QString = "") ;
    void printSizedNgrams(arbre, QTextStream &, ushort) ;
    
  private:
    
    void addTokens(QTextStream &) ;
    inline void addNgramme(QString) ;

    arbre create_branch(QStringRef, arbre, int, int) ;
    arbre create_node(QChar, int = 0, arbre = NULL, arbre = NULL, bool = false) ;

    arbre assoc(QChar, arbre) ;
    
    void sqlWriteCodeNgrams(arbre, QString, int, QSqlQuery, int) ;
    void sqlWriteCodeNgram(id, QString, val, QSqlQuery, int, bool = false) ;
    
    void computeAndWriteMatrix(arbre, arbre, int, QSqlQuery) ;
    void identifyAndWriteVoisins(arbre, arbre, int, id, QSqlQuery) ;
    void sqlWriteCell(id, id, val, QSqlQuery, bool = false) ;
     
    // fonctions devenues inutiles (sont dans impression.cpp, non linké)
    void getVoisins(arbre, arbre, int, QTextStream &) ;
    void identifyVoisins(arbre, arbre, int, id, QTextStream &) ;
    void printCell(id, id, val, QTextStream &) ;
    
    void printCodeNgrams(arbre, QTextStream &, QString, int) ;
    void printCodeNgram(arbre, QTextStream &, QString, int) ;
    void printNgram(QString, QTextStream &) ;
        
    void printNode(arbre, QTextStream &, QString = "") ;
    void spacesIndent(int, QTextStream &) ;
    void printString(QString, QTextStream &) ;
    
    void printLetter(QChar, QTextStream &, bool = false) ;
    bool isPrintableLetter(QChar) ;
    
    void printSizedNgram(arbre, QTextStream &, QString, ushort) ;
     
} ;

#endif // NGRAMS_H
