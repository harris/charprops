/* ============================ CHARPROPS ===============================
 * 
 C harProps 0.1 is a program designed for those who want to explore grapheme properties 
 on the basis of raw Unicode text data. It does some other things too.
 
 Copyright (C) 2015  Bernard (Gilles), Manad (Otman), Aliane (Nourredine)
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 =========================================================================== */

#include "Ngrams.h"
#include "windowngrams.h"

// Constructeur de l'arbre des ngrammes

/* Attention
 * Ce programme ne fonctionne qu'avec Unicode BMP ;
 * pour les alphabets non BMP, il faudrait tester la présence de surrogates
 * Ce devrait être fait en amont, au moment du choix du corpus
 * Dans ce cas, il faudrait utiliser QUtfString (Mathias Kunter) et QUtfChar
 * QTextStream >> c
 * if c.isLowSurrogate() prendre aussi le caractère suivant (sans incrémenter count) ; convertir en un int
 * Que faire en cas d'erreur (low sans high)
 * 
 * Et au lieu de stocker des QChar (ushort) dans l'arbre, on stocke des uint (QUtfChar)
 * 
 * étant donné que ça ralentit, avoir deux versions du code.
 * 
 * Prévu pour la version de juin
 */

Ngrams::Ngrams(int corpus, int maximum, int voisinage, int params, QElapsedTimer time)
 : MaxNgramLength(maximum), Voisinage(voisinage), CountNgrams(1)
{ ContextualLength = MaxNgramLength * (Voisinage + 1) ;
  Trie = create_node((QChar) '/', 0, NULL, NULL, true) ;  
  
  QSqlQuery req ;
  req.exec(QString("select depot_url_copy from document where fk_id_corpus ='%1'").arg(corpus)) ;
  QTextStream flux ;
  QFile infile ;
  qDebug() << "Début de la construction de l'arbre préfixe : " << readabletime(time.elapsed()) ;
  while (req.next())
  { infile.setFileName(QUrl(req.value(0).toString()).toLocalFile()) ;
    if (not infile.open(QIODevice::ReadOnly | QIODevice::Text))
    { qDebug() << "ERREUR: Impossible d'ouvrir le fichier " << QUrl(req.value(0).toString()).toLocalFile() << " en lecture." << endl ;
      return ; }
    flux.setDevice(& infile) ;
    addTokens(flux) ;
    infile.close() ;
  }
  qDebug() << "Fin de la construction de l'arbre préfixe : " << readabletime(time.elapsed()) ;
  
  // Calcul des probabilités ; inhibé pour gagner du temps, comme on ne l'utilise pas encore
  // calculate_mle(Trie) ;
  // calculate_proba(Trie, Trie->occurrences) ;
  
  qDebug() << "Début de l'insertion des ngrammes dans la base : " << readabletime(time.elapsed()) ;
  sqlWriteData(params) ;
  qDebug() << "Fin de l'insertion des ngrammes dans la base : " << readabletime(time.elapsed()) ;
  CountNgrams-- ;
}

Ngrams::~Ngrams()
{}

/********************************************************************** Ajouter ngrammes dans l'arbre **************************************/


void Ngrams::addTokens(QTextStream & flux)
{ QChar c;
  QString str("") ;
  ushort count = 0 ;
  
  while ((not flux.atEnd()) and (count < ContextualLength))
  { flux >> c ;
    str.append(c) ;
    count++ ; }
  addNgramme(str) ;
    
  while (not flux.atEnd())
  { flux >> c;
    str.append(c) ;
    str = str.right(ContextualLength) ;
    addNgramme(str) ; }
    
  while (not (str = str.mid(1)).isEmpty()) addNgramme(str) ;
}

// Used by : addTokens

inline void Ngrams::addNgramme(QString ngram)
{ insert(ngram.midRef(0), Trie) ;  // ajouter ngram dans l'arbre préfixe 
}

// ================================================================== Fabrication de l'arbre ==============================================


// Méthode de base pour créer un noeud
// Used by : Ngrams (avec root, la première fois) ; puis create_branch (sans root)
// terminal n'est pas encore utilisé
// root est pour la création de la racine (créée avant la première lecture, donc doit être initialisé à 0)
// On arrête de créer des identifiants quand level est au dessus de la taille du ngramme
// C'est la seule fonction qui touche à CountNgrams

// Si on lui transmettait le ngramme, on pourrait l'afficher ici avec son code.

arbre Ngrams::create_node (QChar c, int level, arbre son, arbre bro, bool root)
{ arbre p = new node ;
  p->letter = c ;
  p->sons = son ;
  p->bros = bro ;
  if (root) p->occurrences = 0 ;
  else p->occurrences = 1 ;
  p->identifier.fill(0, MaxNgramLength) ;
  if (level > 0 and level <= MaxNgramLength) p->identifier[level - 1] = CountNgrams++ ;
  p->mle = 1.0 ;
  p->proba1 = 1.0 ;
  p->proba2 = 1.0 ;
  return p ;
}

// Cette fonction insère dans l'arbre l'ensemble des ngrammes ayant le même début avec leur nombre d'occurrences
// level sert à déclencher ou à arrêter la création d'identifiants (quand on est dans le voisinage)
// Used by : addNgramme

void Ngrams::insert(QStringRef string, arbre tree, int position, int level)
{ arbre branch ;
  tree->occurrences++ ;
  if (position >= string.size()) return ;
  if ((branch = assoc(string.at(position), tree->sons)))
  { insert(string, branch, position + 1, level + 1) ;
    return ; }
  branch = create_branch(string, tree, position, level + 1) ;
  branch->bros = tree->sons ;
  tree->sons = branch ;
  return ;
}

// Créer une branche entière à partir du morceau de ngramme qui ne figurait pas dans l'arbre
// le retour est greffé dans l'arbre par la fonction insert

arbre Ngrams::create_branch(QStringRef string, arbre tree, int position, int level)
{ if (position >= string.size()) return NULL ;
  return create_node(string.at(position), level, create_branch(string, tree, position + 1, level + 1)) ;
}

// Ramener la branche de l'arbre commençant par une lettre
// On parcourt tous les frères d'un noeud
arbre Ngrams::assoc(QChar c, arbre tree)
{ if (not tree) return NULL ;
  if (tree->letter == c) return tree ;
  return assoc(c, tree->bros) ;
}

// ============================================================== Calculs internes aux ngrammes ===============================================

// Calculer les probabilités conditionnelles de la dernière lettre de chaque ngramme par rapport à son contexte
// unigramme : probabilité tout court
// Maximum likelihood estimation

void Ngrams::calculate_mle(arbre tree, val preceeding_occs)
{ if (not tree) return ;
  if (preceeding_occs) tree->mle = ((double) tree->occurrences) / ((double) preceeding_occs) ;
  calculate_mle(tree->bros, preceeding_occs) ;
  calculate_mle(tree->sons, tree->occurrences) ;
}

// Calculer les probabilités des ngrammes
// 2 méthodes pour calculer les proba
// p(bonjo) = N(bonjo) / N(5grammes)
// p(bonjo) = 1/3
// p(bonjo) = p(o|bonj) * p(j|bon) * p(n|bo) * p(o|b) * p(b)
//          = N(bonjo) / N(bonj) * N(bonj) / N(bon) * N(bon) / N(bo) * N(bo) / N(b) * N(b) / N
// p(bonjo) = N(bonjo) / N(caractères)
// p(bonjo) = 1/7
// tendent vers la même chose si le nombre de caractères est très grand
// N - n + 1 (N - 4)  tend vers N ; 

void Ngrams::calculate_proba(arbre tree, val text_length, short size)
{ if (not tree) return ;
  if (size)
  { tree->proba1 = ((double) tree->occurrences) / ((double) (text_length - size + 1)) ;
    tree->proba2 = ((double) tree->occurrences) / ((double) text_length) ; }
    calculate_proba(tree->bros, text_length, size) ;
    calculate_proba(tree->sons, text_length, size + 1) ;
}

/* Entropie
L'entropie d'un corpus est calculée par :

ordre 1 :
log2(nombre total de caractères) moins (Somme pour tous les caractères i de (occs[i]*log2(occs[i])) divisée par le nombre total de caractères)





*/
// ================================================================== écriture dans la base =================================================

// écrire les données dans la base

void Ngrams::sqlWriteData(int params)
{ QSqlQuery req ;
  req.prepare("insert into ngramme (id_ngram, ngram, size_ngram, frequence, id_trie) values (:id, :ngram, :size, :occs, :trie)") ;
  sqlWriteCodeNgrams(Trie->sons, "", 0, req, params) ;
  sqlWriteCodeNgram(0, "", 0, req, params, true) ;
}

// écrire la table identifiant / ngram / occurrences dans la base
// argument bdd et table
void Ngrams::sqlWriteCodeNgrams(arbre tree, QString ngram, int size, QSqlQuery req, int params)
{ if (not tree) return ;
  if (size == MaxNgramLength) return ;
  ngram.append(tree->letter) ;
  sqlWriteCodeNgram(tree->identifier[size], ngram, tree->occurrences, req, params) ;
  sqlWriteCodeNgrams(tree->sons, ngram, size + 1, req, params) ;
  sqlWriteCodeNgrams(tree->bros, ngram.left(ngram.size() - 1), size, req, params) ;
}

// écrire un batch multi row dans la base
// Le 1000 devrait être un paramètre modifiable (dans quelle classe ?)
// Il y a un bug de Qt qui rend impossible l'exécution de multiples batchs avec "?"

void Ngrams::sqlWriteCodeNgram(id identifier, QString ngram, val occs, QSqlQuery req, int params, bool flag)
{ static QVariantList identifiers ;
  static QVariantList ngrams ;
  static QVariantList sizes ;
  static QVariantList alloccs ;
  static QVariantList allparams ;
  static int compteur = 0 ;

  if (not flag)
  { identifiers << identifier ;
    ngrams << ngram ;
    sizes << ngram.size() ;
    alloccs << occs ;
    allparams << params ;
    compteur++ ; }
  
  if (compteur == 1000 or flag)
  { req.bindValue(":id", identifiers) ;
    req.bindValue(":ngram", ngrams) ;
    req.bindValue(":size", sizes) ;
    req.bindValue(":occs", alloccs) ;
    req.bindValue(":trie", allparams) ;
    req.execBatch() ;
    
    identifiers.clear() ;
    ngrams.clear() ;
    sizes.clear() ;
    alloccs.clear() ;
    allparams.clear() ;
    compteur = 0 ;
    return ; }
    
}


// ================================================================== connexion avec l'interface =================================================

// calculer la matrice ; 
// cette fonction est le slot d'un bouton "Calculer matrice des contextes"
// les économies sont sur l'envoi à la base, non sur le calcul
// car on est obligé d'envoyer.
// mettre dans le noeud l'information qu'il n'est pas sélectionnable

void Ngrams::sqlWriteMatrix()
{ QSqlQuery req ;
  req.prepare("insert into matrice (fk_i, fk_j, val) values (:pred, :succ, :val)") ;
  computeAndWriteMatrix(Trie->sons, Trie, MaxNgramLength, req) ;
  sqlWriteCell(0, 0, 0, req, true) ;
}

// on met les voisins dans la base en même temps qu'on les identifie

void Ngrams::computeAndWriteMatrix(arbre tree, arbre root, int size, QSqlQuery req)
{ if (not tree) return ;
  if (not size) return ;
  identifyAndWriteVoisins(tree->sons, root, 0, tree->identifier[MaxNgramLength - size], req) ;
  computeAndWriteMatrix(tree->bros, root, size, req) ;
  computeAndWriteMatrix(tree->sons, root, size - 1, req) ;
}

// identifier des voisins
// predecessor = identifiant du prédécesseur

void Ngrams::identifyAndWriteVoisins(arbre tree, arbre root, int level, id predecessor, QSqlQuery req)
{ if (not tree) return ;
  if (level == MaxNgramLength) return ;
  arbre branch = assoc(tree->letter, root->sons) ;  // le résultat ne peut être NULL
  tree->identifier[level] = branch->identifier[level] ;
  sqlWriteCell(predecessor, tree->identifier[level], tree->occurrences, req) ;
  identifyAndWriteVoisins(tree->bros, root, level, predecessor, req) ;
  identifyAndWriteVoisins(tree->sons, branch, level + 1, predecessor, req) ;
}

// écrire une cellule de la matrice (X Y value)
// la matrice est imprimée cellule par cellule, ordre aléatoire
// utiliser reserve() pour améliorer le temps ; vérifier ce que clear() fait
// il y a une fuite de mémoire, mais je ne sais où.

void Ngrams::sqlWriteCell(id predecessor, id successor, val value, QSqlQuery req, bool flag)
{ static QVariantList predecessors ;
  static QVariantList successors ;
  static QVariantList values ;
  static short compteur = 0 ;
  
  if (not flag)
  { predecessors << predecessor ;
    successors << successor ;
    values << value ;
    compteur++ ; }
  
  if (compteur == 1000 or flag)
  { req.bindValue(":pred", predecessors) ;
    req.bindValue(":succ", successors) ;
    req.bindValue(":val", values) ;
    req.execBatch() ;
    
    predecessors.clear() ;
    successors.clear() ;
    values.clear() ;
    compteur = 0 ;
    return ; }
}


