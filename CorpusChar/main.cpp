#include "windowngrams.h"

int main(int argc, char  * args[])
{ 

  QApplication app(argc,args);
  WindowNgrams windowngrams;
  windowngrams.show();
  return app.exec(); 
}

/* Résultat des tests

Sur un corpus de 5,5 Go
a traité 374.108 ngrammes
en 13 minutes (incluant l'arbre complet des contextes)
pendant le traitement, utilisait 1.5 Go de mémoire et 2% de CPU

a dégagé Firefox

Mais a craqué après.; il utilisait à ce moment là près de 2 Go de mémoire.

On va donc pour l'instant rester à des valeurs plus raisonnables

*/
