 
 
 // ======================================= TOUT CE QUI EST CI-DESSOUS EST DEVENU INUTILE ===================================================
 
 // ================================================================== Impression de la matrice ==============================================
 
 // Mettre les identifiants non créés
 // Et travailler sur le voisinage
 // Parcourir l'arbre ; prendre le premier fils de la racine, et chercher ses fils
 // traiter chaque fils comme un ngramme initial ;
 // lui affecter son identifiant en fonction de la longueur du ngramme ;
 // On récupère chaque identifiant en épluchant parallèlement la branche initiale et la branche où on est
 // et on le recopie
 // puis passer à ses fils / à ses frères 
 
 // prendre tous les ngrammes de taille <= MaxNgramLength et identifier leurs voisins à droite
 
 void Ngrams::getVoisins(arbre tree, arbre root, int size, QTextStream & flux)
 { if (not tree) return ;
   if (not size) return ;
   identifyVoisins(tree->sons, root, 0, tree->identifier[MaxNgramLength - size], flux) ;
   getVoisins(tree->bros, root, size, flux) ;
   getVoisins(tree->sons, root, size - 1, flux) ;
 }
 
 // identifier des voisins
 // predecessor = identifiant du prédécesseur
 
 void Ngrams::identifyVoisins(arbre tree, arbre root, int level, id predecessor, QTextStream & flux)
 { if (not tree) return ;
   if (level == MaxNgramLength) return ;
   arbre branch = assoc(tree->letter, root->sons) ;  // le résultat ne peut être NULL
   tree->identifier[level] = branch->identifier[level] ;
   printCell(predecessor, tree->identifier[level], tree->occurrences, flux) ;
   identifyVoisins(tree->bros, root, level, predecessor, flux) ;
   identifyVoisins(tree->sons, branch, level + 1, predecessor, flux) ;
 }
 
 // imprime une cellule de la matrice (X Y value)
 // la matrice est imprimée cellule par cellule, ordre aléatoire
 void Ngrams::printCell(id predecessor, id successor, val value, QTextStream & flux)
 { flux << predecessor << ", " << successor << ", " << value << endl ; }
 
 
 // ================================================================== Impression de la table  ==============================================
 
 // imprimer les ngrammes avec leur code
 
 void Ngrams::printCodeNgrams(arbre tree, QTextStream & flux, QString ngram, int size)
 { if (not tree) return ;
   if (size == MaxNgramLength) return ;
   ngram.append(tree->letter) ;
   printCodeNgram(tree, flux, ngram, size) ;
   printCodeNgrams(tree->sons, flux, ngram, size + 1) ;
   printCodeNgrams(tree->bros, flux, ngram.left(ngram.size() - 1), size) ;
 }
 
 // Le ngramme est codé en numérique (décimal pour le moment)
 
 void Ngrams::printCodeNgram(arbre N, QTextStream & flux, QString ngram, int size)
 { flux <<  N->identifier[size] << ", " ;
   printNgram(ngram, flux) ;
   flux << ", " << N->occurrences << endl ;
 }
 
 void Ngrams::printNgram(QString ngram, QTextStream & flux)
 { int position = 0 ;
   while (position < ngram.size() - 1) flux << ngram.at(position++).unicode() << " " ;
   flux << ngram.at(position).unicode() ;
 }
 
 // ================================================================== Trace de l'arbre ==============================================
 
 // imprimer les ngrammes
 // sans tenir compte de la taille
 void Ngrams::printNgrams(arbre tree, QTextStream & flux, QString ngram)
 { if (not tree) return ;
   ngram.append(tree->letter) ;
   printNode(tree, flux, ngram) ;
   printNgrams(tree->sons, flux, ngram) ;
   printNgrams(tree->bros, flux, ngram.left(ngram.size() - 1)) ;
 }
 
 // imprimer les ngrammes ordonnés par taille
 void Ngrams::printSizedNgrams(arbre tree, QTextStream & flux, ushort max)
 { ushort size = 1 ;
   while (size <= max) printSizedNgram(tree, flux, "", size++) ;
 }
 
 // écrire les ngrammes d'une taille donnée
 void Ngrams::printSizedNgram(arbre tree, QTextStream & flux, QString ngram, ushort size)
 { if (not tree) return ;
   ngram.append(tree->letter) ;
   if (not size)
   { printNode(tree, flux, ngram) ;
     printSizedNgram(tree->bros, flux, ngram.left(ngram.size() - 1), size) ;
     return ; }
     printSizedNgram(tree->bros, flux, ngram.left(ngram.size() - 1), size) ;
     printSizedNgram(tree->sons, flux, ngram, size - 1) ; 
 }
 
 // écrire l'arbre dans un fichier pour sauvegarde et vérification
 // Adaptée, pourrait permettre d'écrire un fichier CSV intégrable dans une base de données
 // indentation pour respecter le niveau (fils / frère)
 void Ngrams::printArbre(arbre tree, QTextStream & flux, int indentation)
 { if (not tree) return ;
   spacesIndent(indentation, flux) ;
   printNode(tree, flux) ;
   if (tree->sons) printArbre(tree->sons, flux, indentation + 1) ;
   if (tree->bros) printArbre(tree->bros, flux, indentation) ;
   return ;
 }
 
 void Ngrams::spacesIndent(int number, QTextStream & flux)
 { while (number--) flux << "  " ; }
 
 // écrire un noeud dans un fichier = une ligne
 // seule fonction qui imprime les renseignements
 void Ngrams::printNode(arbre N, QTextStream & flux, QString ngram)
 { if (ngram.isEmpty()) printLetter(N->letter, flux) ;
   else printString(ngram, flux) ;
   flux << " occs : " << N->occurrences << " ; mle : " << N->mle << " ; proba1 : " << N->proba1 << " ; proba2 : " << N->proba2 << " ; identifiants : " << N->identifier[0] << " " \
   << N->identifier[1] << " " << N->identifier[2] << " " << N->identifier[3] << " " << N->identifier[4] << endl ;
 }
 
 // écrire une chaîne incluant des caractères non imprimables dans un fichier
 void Ngrams::printString(QString ngram, QTextStream & flux)
 { flux << "\"" ;
   int position = 1 ;
   while (position < ngram.size())
     printLetter(ngram.at(position++), flux, true) ;
   flux << "\"" ;
 }
 
 // écrire une lettre dans un fichier
 void Ngrams::printLetter(QChar c, QTextStream & flux, bool string)
 { if (isPrintableLetter(c))
   { flux << c ;
     return ; }
     if (string) flux << " *SP*" << c.unicode() << "*SP* " ;
     else flux << c.unicode() ;
 }
 
 // un caractère est-il imprimable ?
 // l'espace est considéré comme imprimable car il ne brise pas les lignes
 // et il y en a trop pour la lisibilité
 
 bool Ngrams::isPrintableLetter(QChar c)
 { if (c.isLetterOrNumber()) return true ;
   if (c.isPunct()) return true ;
   if (c.isSymbol()) return true ;
   if (c.category() == QChar::Separator_Space) return true ;
   return false ;
 }
 